FROM node:lts-alpine3.12
ARG NPM_TOKEN
WORKDIR /tuturno
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
COPY src ./src
COPY ormconfig.js ./ormconfig.js
COPY tsconfig.json ./tsconfig.json
RUN sed -i 's/<exclude-tests-on-build>/**\/*.test.ts/g' ./tsconfig.json
COPY .npmrc ./.npmrc
RUN echo "" >> .npmrc
RUN echo "//npm.pkg.github.com/:_authToken=$NPM_TOKEN" >> .npmrc
RUN cat .npmrc
RUN npm i
RUN npm run build
COPY src/views/*.pug ./build/views/
CMD ["node", "build/index.js"]