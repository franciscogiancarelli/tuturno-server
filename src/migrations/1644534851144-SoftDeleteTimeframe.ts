import {MigrationInterface, QueryRunner} from "typeorm";

export class SoftDeleteTimeframe1644534851144 implements MigrationInterface {
    name = 'SoftDeleteTimeframe1644534851144'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appointment" DROP CONSTRAINT "FK_6a01dfa8da8170cf617a502e18c"`);
        await queryRunner.query(`ALTER TABLE "timeframe" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`);
        await queryRunner.query(`ALTER TABLE "appointment" ADD CONSTRAINT "FK_6a01dfa8da8170cf617a502e18c" FOREIGN KEY ("timeframeId") REFERENCES "timeframe"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appointment" DROP CONSTRAINT "FK_6a01dfa8da8170cf617a502e18c"`);
        await queryRunner.query(`ALTER TABLE "timeframe" DROP COLUMN "deletedAt"`);
        await queryRunner.query(`ALTER TABLE "appointment" ADD CONSTRAINT "FK_6a01dfa8da8170cf617a502e18c" FOREIGN KEY ("timeframeId") REFERENCES "timeframe"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
