import { MigrationInterface, QueryRunner } from "typeorm";

export class MinimumTimeInAdvanceAndOptionalEmailConfirmed1643888703479
    implements MigrationInterface
{
    name = "MinimumTimeInAdvanceAndOptionalEmailConfirmed1643888703479";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "minimumTimeInAdvance" integer DEFAULT '15'`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "minimumTimeInAdvance"`
        );
    }
}
