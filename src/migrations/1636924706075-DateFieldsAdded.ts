import { MigrationInterface, QueryRunner } from "typeorm";

export class DateFieldsAdded1636924706075 implements MigrationInterface {
    name = "DateFieldsAdded1636924706075";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "user" ADD "createdAt" TIMESTAMP NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `ALTER TABLE "user" ADD "updatedAt" TIMESTAMP NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ADD "deletedAt" TIMESTAMP`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ADD "createdAt" TIMESTAMP NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ADD "updatedAt" TIMESTAMP NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "absent" SET NOT NULL`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "payed" SET NOT NULL`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "payed" DROP NOT NULL`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "absent" DROP NOT NULL`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" DROP COLUMN "updatedAt"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" DROP COLUMN "createdAt"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" DROP COLUMN "deletedAt"`
        );
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "updatedAt"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "createdAt"`);
    }
}
