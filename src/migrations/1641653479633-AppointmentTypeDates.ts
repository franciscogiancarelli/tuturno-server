import { MigrationInterface, QueryRunner } from "typeorm";

export class AppointmentTypeDates1641653479633 implements MigrationInterface {
    name = "AppointmentTypeDates1641653479633";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "deletedAt" TIMESTAMP`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "createdAt" TIMESTAMP NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "updatedAt" TIMESTAMP NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "duration" DROP DEFAULT`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "duration" SET DEFAULT '60'`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "updatedAt"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "createdAt"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "deletedAt"`
        );
    }
}
