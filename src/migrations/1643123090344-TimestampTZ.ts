import { MigrationInterface, QueryRunner } from "typeorm";

export class TimestampTZ1643123090344 implements MigrationInterface {
    name = "TimestampTZ1643123090344";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "review" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `ALTER TABLE "user" ALTER COLUMN "deletedAt" TYPE timestamptz USING "deletedAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "user" ALTER COLUMN "createdAt" TYPE timestamptz USING "createdAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "user" ALTER COLUMN "updatedAt" TYPE timestamptz USING "updatedAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ALTER COLUMN "from" TYPE timestamptz USING "from"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ALTER COLUMN "to" TYPE timestamptz USING "to"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ALTER COLUMN "deletedAt" TYPE timestamptz USING "deletedAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ALTER COLUMN "createdAt" TYPE timestamptz USING "createdAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ALTER COLUMN "updatedAt" TYPE timestamptz USING "updatedAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "createdAt" TYPE timestamptz USING "createdAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "updatedAt" TYPE timestamptz USING "updatedAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER COLUMN "deletedAt" TYPE timestamptz USING "deletedAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER COLUMN "createdAt" TYPE timestamptz USING "createdAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER COLUMN "updatedAt" TYPE timestamptz USING "updatedAt"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "from" TYPE timestamptz USING "from"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "to" TYPE timestamptz USING "to"::timestamptz`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "createdAt" TYPE timestamptz USING "createdAt"::timestamptz`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "createdAt" TYPE timestamp USING "createdAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "to" TYPE timestamp USING "to"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ALTER COLUMN "from" TYPE timestamp USING "from"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER COLUMN "updatedAt" TYPE timestamp USING "updatedAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER COLUMN "createdAt" TYPE timestamp USING "createdAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER COLUMN "deletedAt" TYPE timestamp USING "deletedAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "updatedAt" TYPE timestamp USING "updatedAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "createdAt" TYPE timestamp USING "createdAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ALTER COLUMN "updatedAt" TYPE timestamp USING "updatedAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ALTER COLUMN "createdAt" TYPE timestamp USING "createdAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ALTER COLUMN "deletedAt" TYPE timestamp USING "deletedAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ALTER COLUMN "to" TYPE timestamp USING "to"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ALTER COLUMN "from" TYPE timestamp USING "from"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "user" ALTER COLUMN "updatedAt" TYPE timestamp USING "updatedAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "user" ALTER COLUMN "createdAt" TYPE timestamp USING "createdAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "user" ALTER COLUMN "deletedAt" TYPE timestamp USING "deletedAt"::timestamp`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP COLUMN "updatedAt"`
        );
        await queryRunner.query(`ALTER TABLE "review" DROP COLUMN "createdAt"`);
    }
}
