import { MigrationInterface, QueryRunner } from "typeorm";

export class LocationRenaming1638130734373 implements MigrationInterface {
    name = "LocationRenaming1638130734373";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "review" DROP CONSTRAINT "FK_f54881daf6362511a79f3b323cd"`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" DROP CONSTRAINT "FK_f60f16bbd3527d4883ecc2434e3"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" DROP CONSTRAINT "FK_3ed920fb8d655521e25c70570b0"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP CONSTRAINT "FK_9f832bf47834a098ae94cf62509"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP CONSTRAINT "FK_70d5b11b39223725cc36e11280a"`
        );
        await queryRunner.query(
            `ALTER TABLE "review" RENAME COLUMN "locationId" TO "businessId"`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" RENAME COLUMN "locationId" TO "businessId"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" RENAME COLUMN "locationId" TO "businessId"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" RENAME COLUMN "locationId" TO "businessId"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" RENAME COLUMN "locationId" TO "businessId"`
        );
        await queryRunner.query(
            `ALTER TABLE "review" ADD CONSTRAINT "FK_2185579dd3d78702b90d4e5a2c0" FOREIGN KEY ("businessId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ADD CONSTRAINT "FK_3ff187f8b5d0de68f22e8841468" FOREIGN KEY ("businessId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ADD CONSTRAINT "FK_0c397aff9d95fff673807c9fbf9" FOREIGN KEY ("businessId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD CONSTRAINT "FK_03e3670fac9369e2310aa576251" FOREIGN KEY ("businessId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD CONSTRAINT "FK_3e12f9ea4df1c9a44482567d997" FOREIGN KEY ("businessId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER INDEX "locationSearchVectorIndex" RENAME TO  "businessSearchVectorIndex";`
        );
        await queryRunner.query(`ALTER TABLE location RENAME TO business;`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE business RENAME TO location;`);
        await queryRunner.query(
            `ALTER INDEX "businessSearchVectorIndex" RENAME TO "locationSearchVectorIndex";`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP CONSTRAINT "FK_3e12f9ea4df1c9a44482567d997"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP CONSTRAINT "FK_03e3670fac9369e2310aa576251"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" DROP CONSTRAINT "FK_0c397aff9d95fff673807c9fbf9"`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" DROP CONSTRAINT "FK_3ff187f8b5d0de68f22e8841468"`
        );
        await queryRunner.query(
            `ALTER TABLE "review" DROP CONSTRAINT "FK_2185579dd3d78702b90d4e5a2c0"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" RENAME COLUMN "businessId" TO "locationId"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" RENAME COLUMN "businessId" TO "locationId"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" RENAME COLUMN "businessId" TO "locationId"`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" RENAME COLUMN "businessId" TO "locationId"`
        );
        await queryRunner.query(
            `ALTER TABLE "review" RENAME COLUMN "businessId" TO "locationId"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD CONSTRAINT "FK_70d5b11b39223725cc36e11280a" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD CONSTRAINT "FK_9f832bf47834a098ae94cf62509" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ADD CONSTRAINT "FK_3ed920fb8d655521e25c70570b0" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ADD CONSTRAINT "FK_f60f16bbd3527d4883ecc2434e3" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "review" ADD CONSTRAINT "FK_f54881daf6362511a79f3b323cd" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
    }
}
