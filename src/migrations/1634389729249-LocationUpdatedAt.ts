import { MigrationInterface, QueryRunner } from "typeorm";

export class LocationUpdatedAt1634389729249 implements MigrationInterface {
    name = "LocationUpdatedAt1634389729249";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_ad08b498dcdesd4tergfsdf323e"`);
        await queryRunner.query(`DROP INDEX "IDX_ad08b498dcdesdfdsf926239d0"`);
        await queryRunner.query(`DROP INDEX "IDX_ad08b4982rgwefwerf9235432"`);
        await queryRunner.query(
            `ALTER TABLE "location" ADD "updatedAt" TIMESTAMP NOT NULL DEFAULT now()`
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_f0336eb8ccdf8306e270d400cf" ON "location" ("name") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_5cfc15e9800d246af3414189be" ON "location" ("description") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_d08b498dcdedea6d926239d06e" ON "location" ("ownerUid") `
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_d08b498dcdedea6d926239d06e"`);
        await queryRunner.query(`DROP INDEX "IDX_5cfc15e9800d246af3414189be"`);
        await queryRunner.query(`DROP INDEX "IDX_f0336eb8ccdf8306e270d400cf"`);
        await queryRunner.query(
            `ALTER TABLE "location" DROP COLUMN "updatedAt"`
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_ad08b4982rgwefwerf9235432" ON "location" ("name") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_ad08b498dcdesdfdsf926239d0" ON "location" ("ownerUid") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_ad08b498dcdesd4tergfsdf323e" ON "location" ("description") `
        );
    }
}
