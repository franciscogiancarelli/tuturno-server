import { MigrationInterface, QueryRunner } from "typeorm";

export class AddToBusinessCountryAndPostalCodeFields1638184574849
    implements MigrationInterface
{
    name = "AddToBusinessCountryAndPostalCodeFields1638184574849";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "business" ADD "phone" character varying  DEFAULT ''`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ADD "locality" character varying  DEFAULT ''`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ADD "postalCode" character varying  DEFAULT ''`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ADD "country" character varying  DEFAULT ''`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER column "phone" DROP DEFAULT`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER column "locality" DROP DEFAULT`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER column "postalCode" DROP DEFAULT`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER column "country" DROP DEFAULT`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "business" DROP COLUMN "country"`);
        await queryRunner.query(
            `ALTER TABLE "business" DROP COLUMN "postalCode"`
        );
        await queryRunner.query(
            `ALTER TABLE "business" DROP COLUMN "locality"`
        );
        await queryRunner.query(`ALTER TABLE "business" DROP COLUMN "phone"`);
    }
}
