import { MigrationInterface, QueryRunner } from "typeorm";

export class AppointmentAbsentAndPayed1636292058925
    implements MigrationInterface
{
    name = "AppointmentAbsentAndPayed1636292058925";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD "absent" boolean DEFAULT false`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD "payed" boolean DEFAULT false`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP COLUMN "payed"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP COLUMN "absent"`
        );
    }
}
