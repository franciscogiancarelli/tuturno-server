import { MigrationInterface, QueryRunner } from "typeorm";

export class AppointmentTypeMaxUserRequest1643029621308
    implements MigrationInterface
{
    name = "AppointmentTypeMaxUserRequest1643029621308";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "maxDailyUserRequest" integer`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "maxWeeklyUserRequest" integer`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "maxMonthlyUserRequest" integer`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "maxMonthlyUserRequest"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "maxWeeklyUserRequest"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "maxDailyUserRequest"`
        );
    }
}
