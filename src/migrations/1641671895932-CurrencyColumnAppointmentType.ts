import { MigrationInterface, QueryRunner } from "typeorm";

export class CurrencyColumnAppointmentType1641671895932
    implements MigrationInterface
{
    name = "CurrencyColumnAppointmentType1641671895932";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TYPE "public"."appointment_type_currency_enum" AS ENUM('ARS', 'USD', 'EUR') `
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "currency" "public"."appointment_type_currency_enum" NOT NULL DEFAULT 'ARS'`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "price" TYPE money USING price::money`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "price" TYPE integer USING price::numeric::integer`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "currency"`
        );
        await queryRunner.query(
            `DROP TYPE "public"."appointment_type_currency_enum"`
        );
    }
}
