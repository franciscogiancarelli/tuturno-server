import { MigrationInterface, QueryRunner } from "typeorm";

export class ResourceRelationFix1641089769558 implements MigrationInterface {
    name = "ResourceRelationFix1641089769558";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "business" DROP CONSTRAINT "FK_d08b498dcdedea6d926239d06e4"`
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_f0336eb8ccdf8306e270d400cf"`
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_5cfc15e9800d246af3414189be"`
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_d08b498dcdedea6d926239d06e"`
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_f75de36d0d30611d95d6247fd1"`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER COLUMN "phone" SET NOT NULL`
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_c6894e962b80bc10a694c0271e" ON "business" ("name") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_1bc7d8707077a2a849b183ce0a" ON "business" ("description") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_74b13cac1172e24d30b34fa106" ON "business" ("ownerUid") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_6b1dc30433e4dc67f2e1239b9a" ON "business" ("type") `
        );
        await queryRunner.query(
            `ALTER TABLE "business" ADD CONSTRAINT "FK_74b13cac1172e24d30b34fa106d" FOREIGN KEY ("ownerUid") REFERENCES "user"("uid") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "business" DROP CONSTRAINT "FK_74b13cac1172e24d30b34fa106d"`
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_6b1dc30433e4dc67f2e1239b9a"`
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_74b13cac1172e24d30b34fa106"`
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_1bc7d8707077a2a849b183ce0a"`
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_c6894e962b80bc10a694c0271e"`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ALTER COLUMN "phone" DROP NOT NULL`
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_f75de36d0d30611d95d6247fd1" ON "business" ("type") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_d08b498dcdedea6d926239d06e" ON "business" ("ownerUid") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_5cfc15e9800d246af3414189be" ON "business" ("description") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_f0336eb8ccdf8306e270d400cf" ON "business" ("name") `
        );
        await queryRunner.query(
            `ALTER TABLE "business" ADD CONSTRAINT "FK_d08b498dcdedea6d926239d06e4" FOREIGN KEY ("ownerUid") REFERENCES "user"("uid") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
    }
}
