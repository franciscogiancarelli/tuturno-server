import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class EmailVerification1632872227098 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            "user",
            new TableColumn({
                type: "boolean",
                name: "emailConfirmed",
                default: false,
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("user", "emailConfirmed");
    }
}
