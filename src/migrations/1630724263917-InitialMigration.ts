import { MigrationInterface, QueryRunner } from "typeorm";

export class InitialMigration1630724263917 implements MigrationInterface {
    name = "InitialMigration1630724263917";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "user" ("uid" uuid NOT NULL DEFAULT uuid_generate_v4(), "address" character varying, "email" character varying NOT NULL, "password" character varying NOT NULL, "name" character varying NOT NULL, "phone" character varying NOT NULL, "deletedAt" TIMESTAMP, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_df955cae05f17b2bcf5045cc021" PRIMARY KEY ("uid"))`
        );
        await queryRunner.query(
            `CREATE TABLE "timeframe" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "from" TIMESTAMP NOT NULL, "to" TIMESTAMP NOT NULL, "width" integer NOT NULL, "locationId" uuid NOT NULL, "ownerUid" uuid NOT NULL, "resourceId" uuid NOT NULL, CONSTRAINT "PK_bd148f3369428286caef8edee9b" PRIMARY KEY ("id"))`
        );
        await queryRunner.query(
            `CREATE TABLE "resource" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL DEFAULT 'General', "description" character varying, "interval" integer NOT NULL DEFAULT '15', "locationId" uuid NOT NULL, "ownerUid" uuid NOT NULL, CONSTRAINT "PK_e2894a5867e06ae2e8889f1173f" PRIMARY KEY ("id"))`
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_c8ed18ff47475e2c4a7bf59daa" ON "resource" ("name") `
        );
        await queryRunner.query(
            `CREATE TABLE "appointment_type" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL DEFAULT 'General', "price" integer NOT NULL, "description" character varying NOT NULL, "duration" integer NOT NULL DEFAULT '60', "locationId" uuid, CONSTRAINT "PK_c2c6d743b2dc3aa4b46f4cb99f8" PRIMARY KEY ("id"))`
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_33bdac66e158b9c8a7e8847897" ON "appointment_type" ("name") `
        );
        await queryRunner.query(
            `CREATE TABLE "location" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "description" character varying, "images" character varying array, "cover" character varying, "deletedAt" TIMESTAMP, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "address" character varying NOT NULL, "latitude" double precision NOT NULL, "longitude" double precision NOT NULL, "type" character varying NOT NULL DEFAULT 'General', "ownerUid" uuid, CONSTRAINT "PK_876d7bdba03c72251ec4c2dc827" PRIMARY KEY ("id"))`
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_ad08b498dcdesdfdsf926239d0" ON "location" ("ownerUid") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_ad08b4982rgwefwerf9235432" ON "location" ("name") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_ad08b498dcdesd4tergfsdf323e" ON "location" ("description") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_f75de36d0d30611d95d6247fd1" ON "location" ("type") `
        );
        await queryRunner.query(
            `CREATE TABLE "appointment" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "from" TIMESTAMP NOT NULL, "to" TIMESTAMP NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "locationId" uuid NOT NULL, "ownerUid" uuid, "resourceId" uuid NOT NULL, "timeframeId" uuid NOT NULL, "typeId" uuid NOT NULL, CONSTRAINT "PK_e8be1a53027415e709ce8a2db74" PRIMARY KEY ("id"))`
        );
        await queryRunner.query(
            `CREATE TABLE "resource_appointment_types_appointment_type" ("resourceId" uuid NOT NULL, "appointmentTypeId" uuid NOT NULL, CONSTRAINT "PK_625aa9ea8a7c9e9d1f7eb0420d8" PRIMARY KEY ("resourceId", "appointmentTypeId"))`
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_03e91e5bcf8b7b770de4e9656c" ON "resource_appointment_types_appointment_type" ("resourceId") `
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_e5d764ec3506c32fc93b659a8c" ON "resource_appointment_types_appointment_type" ("appointmentTypeId") `
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ADD CONSTRAINT "FK_f60f16bbd3527d4883ecc2434e3" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ADD CONSTRAINT "FK_3b576a5d8f0a0a2d71df1237ec8" FOREIGN KEY ("ownerUid") REFERENCES "user"("uid") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" ADD CONSTRAINT "FK_561e27abdef015684b6ff97147a" FOREIGN KEY ("resourceId") REFERENCES "resource"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ADD CONSTRAINT "FK_3ed920fb8d655521e25c70570b0" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" ADD CONSTRAINT "FK_f725d5d643b5727b766c684e694" FOREIGN KEY ("ownerUid") REFERENCES "user"("uid") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD CONSTRAINT "FK_9f832bf47834a098ae94cf62509" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "location" ADD CONSTRAINT "FK_d08b498dcdedea6d926239d06e4" FOREIGN KEY ("ownerUid") REFERENCES "user"("uid") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD CONSTRAINT "FK_70d5b11b39223725cc36e11280a" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD CONSTRAINT "FK_0cac8b5903ac2fd1b6e6e8acaaa" FOREIGN KEY ("ownerUid") REFERENCES "user"("uid") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD CONSTRAINT "FK_59854e09b99cf15c747fa86cd0a" FOREIGN KEY ("resourceId") REFERENCES "resource"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD CONSTRAINT "FK_6a01dfa8da8170cf617a502e18c" FOREIGN KEY ("timeframeId") REFERENCES "timeframe"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD CONSTRAINT "FK_4b35e2976b4ea578b05b9de5a64" FOREIGN KEY ("typeId") REFERENCES "appointment_type"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "resource_appointment_types_appointment_type" ADD CONSTRAINT "FK_03e91e5bcf8b7b770de4e9656cd" FOREIGN KEY ("resourceId") REFERENCES "resource"("id") ON DELETE CASCADE ON UPDATE CASCADE`
        );
        await queryRunner.query(
            `ALTER TABLE "resource_appointment_types_appointment_type" ADD CONSTRAINT "FK_e5d764ec3506c32fc93b659a8c3" FOREIGN KEY ("appointmentTypeId") REFERENCES "appointment_type"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "resource_appointment_types_appointment_type" DROP CONSTRAINT "FK_e5d764ec3506c32fc93b659a8c3"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource_appointment_types_appointment_type" DROP CONSTRAINT "FK_03e91e5bcf8b7b770de4e9656cd"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP CONSTRAINT "FK_4b35e2976b4ea578b05b9de5a64"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP CONSTRAINT "FK_6a01dfa8da8170cf617a502e18c"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP CONSTRAINT "FK_59854e09b99cf15c747fa86cd0a"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP CONSTRAINT "FK_0cac8b5903ac2fd1b6e6e8acaaa"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP CONSTRAINT "FK_70d5b11b39223725cc36e11280a"`
        );
        await queryRunner.query(
            `ALTER TABLE "location" DROP CONSTRAINT "FK_d08b498dcdedea6d926239d06e4"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP CONSTRAINT "FK_9f832bf47834a098ae94cf62509"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" DROP CONSTRAINT "FK_f725d5d643b5727b766c684e694"`
        );
        await queryRunner.query(
            `ALTER TABLE "resource" DROP CONSTRAINT "FK_3ed920fb8d655521e25c70570b0"`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" DROP CONSTRAINT "FK_561e27abdef015684b6ff97147a"`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" DROP CONSTRAINT "FK_3b576a5d8f0a0a2d71df1237ec8"`
        );
        await queryRunner.query(
            `ALTER TABLE "timeframe" DROP CONSTRAINT "FK_f60f16bbd3527d4883ecc2434e3"`
        );
        await queryRunner.query(`DROP INDEX "IDX_e5d764ec3506c32fc93b659a8c"`);
        await queryRunner.query(`DROP INDEX "IDX_03e91e5bcf8b7b770de4e9656c"`);
        await queryRunner.query(
            `DROP TABLE "resource_appointment_types_appointment_type"`
        );
        await queryRunner.query(`DROP TABLE "appointment"`);
        await queryRunner.query(`DROP INDEX "IDX_f75de36d0d30611d95d6247fd1"`);
        await queryRunner.query(`DROP INDEX "IDX_d08b498dcdedea6d926239d06e"`);
        await queryRunner.query(`DROP INDEX "IDX_ad08b498dcdesdfdsf926239d0"`);
        await queryRunner.query(`DROP INDEX "IDX_ad08b4982rgwefwerf9235432"`);
        await queryRunner.query(`DROP INDEX "IDX_ad08b498dcdesd4tergfsdf323e"`);
        await queryRunner.query(`DROP TABLE "location"`);
        await queryRunner.query(`DROP INDEX "IDX_33bdac66e158b9c8a7e8847897"`);
        await queryRunner.query(`DROP TABLE "appointment_type"`);
        await queryRunner.query(`DROP INDEX "IDX_c8ed18ff47475e2c4a7bf59daa"`);
        await queryRunner.query(`DROP TABLE "resource"`);
        await queryRunner.query(`DROP TABLE "timeframe"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }
}
