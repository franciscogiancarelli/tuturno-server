import { MigrationInterface, QueryRunner } from "typeorm";

export class Reviews1634404717246 implements MigrationInterface {
    name = "Reviews1634404717246";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "review" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "score" integer NOT NULL, "description" character varying, "locationId" uuid NOT NULL, "ownerUid" uuid NOT NULL, CONSTRAINT "PK_2e4299a343a81574217255c00ca" PRIMARY KEY ("id"))`
        );

        await queryRunner.query(
            `ALTER TABLE "review" ADD CONSTRAINT "FK_f54881daf6362511a79f3b323cd" FOREIGN KEY ("locationId") REFERENCES "location"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
        await queryRunner.query(
            `ALTER TABLE "review" ADD CONSTRAINT "FK_4213f3dda4903aee99e66f72ba6" FOREIGN KEY ("ownerUid") REFERENCES "user"("uid") ON DELETE NO ACTION ON UPDATE NO ACTION`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "review" DROP CONSTRAINT "FK_4213f3dda4903aee99e66f72ba6"`
        );
        await queryRunner.query(
            `ALTER TABLE "review" DROP CONSTRAINT "FK_f54881daf6362511a79f3b323cd"`
        );
        await queryRunner.query(`DROP TABLE "review"`);
    }
}
