import {MigrationInterface, QueryRunner} from "typeorm";

export class AppointmentNotes1647778546518 implements MigrationInterface {
    name = 'AppointmentNotes1647778546518'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appointment" ADD "notes" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "appointment" DROP COLUMN "notes"`);
    }

}
