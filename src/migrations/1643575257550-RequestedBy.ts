import { MigrationInterface, QueryRunner } from "typeorm";

export class RequestedBy1643575257550 implements MigrationInterface {
    name = "RequestedBy1643575257550";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD "requestedBy" character varying NOT NULL DEFAULT 'customer'`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP COLUMN "requestedBy"`
        );
    }
}
