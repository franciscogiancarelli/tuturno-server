import {MigrationInterface, QueryRunner} from "typeorm";

export class RecurrentIdColumnTimeframe1644836417217 implements MigrationInterface {
    name = 'RecurrentIdColumnTimeframe1644836417217'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "timeframe" ADD "recurrentId" uuid`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "timeframe" DROP COLUMN "recurrentId"`);
    }

}
