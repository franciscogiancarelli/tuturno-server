import { MigrationInterface, QueryRunner } from "typeorm";

export class Unaccent1633553271355 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE location DROP COLUMN "searchVector";`
        );
        await queryRunner.query(`
		ALTER TABLE location ADD "searchVector" tsvector
		GENERATED ALWAYS AS (to_tsvector('spanish', coalesce(name, '') || ' ' || coalesce(description, '') || ' ' || coalesce(type,''))) STORED;
		`);
        await queryRunner.query(`
		CREATE INDEX "locationSearchVectorIndex" ON location
		USING GIN ("searchVector");
		`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE location DROP COLUMN "searchVector";`
        );
        await queryRunner.query(`
		ALTER TABLE location ADD "searchVector" tsvector
		GENERATED ALWAYS AS (to_tsvector('english', coalesce(name, '') || ' ' || coalesce(description, '') || ' ' || coalesce(type,''))) STORED;
		`);
        await queryRunner.query(`
		CREATE INDEX "locationSearchVectorIndex" ON location
		USING GIN ("searchVector");
		`);
    }
}
