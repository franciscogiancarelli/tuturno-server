import { MigrationInterface, QueryRunner } from "typeorm";

export class NullablePassword1643291320978 implements MigrationInterface {
    name = "NullablePassword1643291320978";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD "accepted" boolean DEFAULT true`
        );
        await queryRunner.query(
            `ALTER TABLE "user" ALTER COLUMN "password" DROP NOT NULL`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "user" ALTER COLUMN "password" SET NOT NULL`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP COLUMN "accepted"`
        );
    }
}
