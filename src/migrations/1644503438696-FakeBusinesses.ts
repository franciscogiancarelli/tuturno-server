import { MigrationInterface, QueryRunner } from "typeorm";

export class FakeBusinesses1644503438696 implements MigrationInterface {
    name = "FakeBusinesses1644503438696";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "user" ADD "isTesting" boolean NOT NULL DEFAULT false`
        );
        await queryRunner.query(
            `ALTER TABLE "business" ADD "isFake" boolean NOT NULL DEFAULT false`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "business" DROP COLUMN "isFake"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "isTesting"`);
    }
}
