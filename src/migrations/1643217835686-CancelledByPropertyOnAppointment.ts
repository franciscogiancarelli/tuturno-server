import { MigrationInterface, QueryRunner } from "typeorm";

export class CancelledByPropertyOnAppointment1643217835686
    implements MigrationInterface
{
    name = "CancelledByPropertyOnAppointment1643217835686";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD "cancelledBy" character varying NOT NULL DEFAULT ''`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP COLUMN "cancelledBy"`
        );
    }
}
