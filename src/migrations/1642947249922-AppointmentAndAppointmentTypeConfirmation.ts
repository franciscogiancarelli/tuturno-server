import { MigrationInterface, QueryRunner } from "typeorm";

export class AppointmentAndAppointmentTypeConfirmation1642947249922
    implements MigrationInterface
{
    name = "AppointmentAndAppointmentTypeConfirmation1642947249922";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ADD "needConfirmation" boolean NOT NULL DEFAULT false`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" ADD "confirmed" boolean NOT NULL DEFAULT true`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "price" DROP DEFAULT`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "appointment_type" ALTER COLUMN "price" SET DEFAULT '$100.00'`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment" DROP COLUMN "confirmed"`
        );
        await queryRunner.query(
            `ALTER TABLE "appointment_type" DROP COLUMN "needConfirmation"`
        );
    }
}
