import swaggerJsdoc from "swagger-jsdoc";
import User from "entities/User";
import Business from "entities/Business";
import Appointment from "entities/Appointment";
import AppointmentType from "entities/AppointmentType";
import Timeframe from "entities/Timeframe";
import Resource from "entities/Resource";

const options: swaggerJsdoc.Options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Tuturno API",
            description: `
## Bienvenido a la documentación de la API de Tuturno!

Acá vas a poder ver todos los endpoints que disponibles para que te
comuniques con nuestros servidores.
`,
            version: "1.0.0",
        },
        components: {
            schemas: {
                User: User.getSchema(),
                Business: Business.getSchema(),
                Appointment: Appointment.getSchema(),
                AppointmentType: AppointmentType.getSchema(),
                Timeframe: Timeframe.getSchema(),
                Resource: Resource.getSchema(),
            },
            securitySchemes: {
                BearerAuth: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT",
                },
            },
        },
        servers: [
            {
                url: process.env.HOST,
                description: "Local server",
            },
            {
                url: process.env.PROD_HOST,
                description: "Production server",
            },
        ],
        tags: [
            {
                name: "Users",
                description: "Endpoints to manage Users",
            },
            {
                name: "Businesses",
                description: "Endpoints to manage Businesses",
            },
            {
                name: "Me",
                description: "Endpoints to manage your profile",
            },
            {
                name: "Appointments",
                description: "Endpoints to manage your appointments",
            },
            {
                name: "AppointmentTypes",
                description: "Endpoints to manager your appointment types",
            },
            {
                name: "Timeframe",
                description: "Endpoints to manager your timeframes",
            },
            {
                name: "Review",
                description: "Endpoints to manage reviews",
            },
            {
                name: "Resources",
                description: "Endpoints to manage resources",
            },
        ],
    },
    apis: ["./src/routers/*/index.ts", "./src/routers/**/types.ts"],
};

export default options;
