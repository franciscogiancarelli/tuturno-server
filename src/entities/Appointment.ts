import Schema from "typedef/Schema";
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import Business from "./Business";
import User from "./User";
import Resource from "./Resource";
import Timeframe from "./Timeframe";
import AppointmentType from "./AppointmentType";

/**Posible values of cancelledBy column*/
export enum RequestedBy {
    customer = "customer",
    business = "business",
}

export enum CancelledBy {
    customer = "customer",
    business = "business",
    notCancelled = "",
}

@Entity()
export default class Appointment {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column({ nullable: false, type: "timestamptz" })
    from!: Date;

    @Column({ nullable: false, type: "timestamptz" })
    to!: Date;

    @Column({ default: false })
    absent?: boolean;

    @Column({ default: true, nullable: true, type: "boolean" })
    accepted?: boolean | null;

    @Column({ default: false })
    payed?: boolean;

    @Column({ default: true })
    confirmed?: boolean;

    @Column({ default: RequestedBy.customer })
    requestedBy?: string;

    @Column({ default: CancelledBy.notCancelled })
    cancelledBy?: string;

    @Column({ nullable: true })
    notes?: string;

    @ManyToOne(() => Business, (business) => business.appointments, {
        nullable: false,
    })
    business!: Business;

    @ManyToOne(() => User, (user) => user.appointments)
    owner!: User;

    @ManyToOne(() => Resource, (resource) => resource.appointments, {
        nullable: false,
        onDelete: "CASCADE",
    })
    resource!: Resource;

    @ManyToOne(() => Timeframe, (timeframe) => timeframe.appointments, {
        nullable: false,
    })
    timeframe!: Timeframe;

    @CreateDateColumn({ type: "timestamptz" })
    createdAt!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    updatedAt!: Date;

    @ManyToOne(() => AppointmentType, { nullable: false, onDelete: "CASCADE" })
    type!: AppointmentType;

    public static getSchema(): Schema {
        return {
            properties: {
                id: {
                    type: "string",
                    example: "7d59cb4a-9151-4c63-82b1-a65d0817fa2b",
                },
                from: {
                    type: "string",
                    nullable: true,
                    example: "9 de Julio 4325",
                },
                to: {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
                business: {
                    $ref: "#/components/schemas/Business",
                },
                createdAt: {
                    type: "string",
                    example: "2021-09-10T18:00:17.794Z",
                },
            },
        };
    }
}
