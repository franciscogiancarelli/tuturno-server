import Schema from "typedef/Schema";
import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    Index,
    ManyToMany,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from "typeorm";
import Business from "./Business";
import Resource from "./Resource";

export enum Currency {
    ARS = "ARS",
    USD = "USD",
    EUR = "EUR",
}

@Entity()
export default class AppointmentType {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @DeleteDateColumn()
    deletedAt?: Date;

    @Index()
    @Column({ nullable: false, default: "General" })
    name!: string;

    @Column({ type: "money", nullable: false })
    price!: string;

    @Column({
        type: "enum",
        enum: Currency,
        default: Currency.ARS,
        nullable: false,
    })
    currency!: Currency;

    @Column()
    description!: string;

    @Column({ nullable: false, default: false })
    needConfirmation!: boolean;

    @Column({ nullable: true })
    maxDailyUserRequest?: number;

    @Column({ nullable: true })
    maxWeeklyUserRequest?: number;

    @Column({ nullable: true })
    maxMonthlyUserRequest?: number;

    @Column({ nullable: true, default: 15 })
    minimumTimeInAdvance?: number;

    @ManyToOne(() => Business, (business) => business.appointmentTypes)
    business!: Business;

    @ManyToMany(() => Resource, (resource) => resource.appointmentTypes, {
        onDelete: "CASCADE",
    })
    resources!: Array<Resource>;

    @Column({ nullable: false })
    duration!: number;

    @CreateDateColumn({ type: "timestamptz" })
    createdAt!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    updatedAt!: Date;

    public static getSchema(): Schema {
        return {
            properties: {
                id: {
                    type: "string",
                    example: "7d59cb4a-9151-4c63-82b1-a65d0817fa2b",
                },
                name: {
                    type: "string",
                    nullable: false,
                    example: "Turno corte simple",
                },
                description: {
                    type: "string",
                    nullable: false,
                    example: "Turno para cortarte el pelo",
                },
                business: {
                    nullable: true,
                    $ref: "#/components/schemas/Business",
                },
                price: {
                    type: "number",
                    nullable: false,
                    example: 80,
                },
                currency: {
                    type: "enum",
                    nullable: false,
                    example: "ARS",
                },
                duration: {
                    type: "number",
                    nullable: false,
                    example: 60,
                },
                deletedAt: {
                    type: "string",
                    nullable: true,
                },
                createdAt: {
                    type: "string",
                    example: "2021-09-10T18:00:17.794Z",
                },
                updatedAt: {
                    type: "string",
                    example: "2021-09-10T18:00:17.794Z",
                },
            },
        };
    }
}
