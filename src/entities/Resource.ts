import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
    Index,
    DeleteDateColumn,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import Business from "./Business";
import AppointmentType from "./AppointmentType";
import Timeframe from "./Timeframe";
import Appointment from "./Appointment";
import User from "./User";
import Schema from "typedef/Schema";

@Entity()
export default class Resource {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Index()
    @Column({ nullable: false, default: "General" })
    name!: string;

    @Column({ nullable: true })
    description?: string;

    @ManyToOne(() => Business, (business) => business.resources, {
        nullable: false,
    })
    business!: Business;

    @ManyToOne(() => User, (user) => user.resources, {
        nullable: false,
    })
    owner!: User;

    @OneToMany(() => Timeframe, (timeframe) => timeframe.resource)
    timeframes!: Timeframe[];

    @OneToMany(() => Appointment, (appointment) => appointment.resource)
    appointments!: Appointment[];

    @ManyToMany(
        () => AppointmentType,
        (appointmentType) => appointmentType.resources
    )
    @JoinTable()
    appointmentTypes!: AppointmentType[];

    @Column({ nullable: false, default: 15 })
    interval!: number;

    @DeleteDateColumn({ type: "timestamptz" })
    deletedAt?: Date;

    @CreateDateColumn({ type: "timestamptz" })
    createdAt!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    updatedAt!: Date;

    public static getSchema(): Schema {
        return {
            properties: {
                id: {
                    type: "string",
                    example: "7d59cb4a-9151-4c61-82b7-a65d0817fa2b",
                },
                name: {
                    type: "string",
                    nullable: false,
                    example: "Peluquero 1",
                },
                description: {
                    type: "string",
                    example: "Peluquero corte hombre",
                },
                interval: {
                    type: "number",
                    example: 15,
                },
                createdAt: {
                    type: "string",
                    example: "2021-09-10T18:00:17.794Z",
                },
                deletedAt: {
                    type: "string",
                    nullable: true,
                },
                updatedAt: {
                    type: "string",
                    nullable: true,
                },
            },
        };
    }
}
