import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    CreateDateColumn,
} from "typeorm";
import Business from "./Business";
import User from "./User";

@Entity()
export default class Review {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column({ nullable: false })
    score!: number;

    @Column({ nullable: true })
    description?: string;

    @CreateDateColumn({ type: "timestamptz" })
    createdAt!: Date;

    @ManyToOne(() => Business, (business) => business.reviews, {
        nullable: false,
    })
    business!: Business;

    @ManyToOne(() => User, (user) => user.reviews, {
        nullable: false,
    })
    owner!: User;
}
