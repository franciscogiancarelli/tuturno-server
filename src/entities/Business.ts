import Schema from "typedef/Schema";
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    DeleteDateColumn,
    CreateDateColumn,
    ManyToOne,
    Index,
    UpdateDateColumn,
} from "typeorm";
import Appointment from "./Appointment";
import AppointmentType from "./AppointmentType";
import Resource from "./Resource";
import Timeframe from "./Timeframe";
import User from "./User";
import Review from "./Review";

@Entity()
export default class Business {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Index()
    @Column({ nullable: false })
    name!: string;

    @Index()
    @Column({ nullable: true })
    description!: string;

    @Column({ nullable: false })
    phone!: string;

    @Column({ nullable: false, default: false })
    isFake!: boolean;

    @Column("varchar", { nullable: true, array: true })
    images?: string[];

    @Column({ nullable: true })
    cover?: string;

    @OneToMany(() => Appointment, (appointment) => appointment.business, {
        cascade: true,
    })
    appointments!: Array<Appointment>;

    @OneToMany(() => Timeframe, (timeframe) => timeframe.business, {
        cascade: true,
    })
    timeframes!: Array<Timeframe>;

    @Index()
    @ManyToOne(() => User, (user) => user.businesses)
    owner!: User;

    @DeleteDateColumn({ type: "timestamptz" })
    deletedAt?: Date;

    @CreateDateColumn({ type: "timestamptz" })
    createdAt!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    updatedAt!: Date;

    @Column({ nullable: false })
    address!: string;

    @Column({ nullable: false, type: "double precision" })
    latitude!: number;

    @Column({ nullable: false, type: "double precision" })
    longitude!: number;

    @Column({ nullable: true })
    locality?: string;

    @Column({ nullable: true })
    postalCode?: string;

    @Column({ nullable: true })
    country?: string;

    @OneToMany(
        () => AppointmentType,
        (appointmentType) => appointmentType.business,
        { cascade: true }
    )
    appointmentTypes!: Array<AppointmentType>;

    @OneToMany(() => Resource, (resource) => resource.business, {
        cascade: true,
    })
    resources!: Array<Resource>;

    @OneToMany(() => Review, (review) => review.business, { cascade: true })
    reviews!: Array<Review>;

    @Index()
    @Column({ nullable: false, default: "General" })
    type!: string;

    @Index("businessSearchVectorIndex")
    @Column({
        type: "tsvector",
        select: false,
        nullable: true,
    })
    searchVector?: unknown;

    public static getSchema(): Schema {
        return {
            properties: {
                id: {
                    type: "string",
                    example: "7d59cb4a-9151-4c63-82b1-a65d0817fa2b",
                },
                address: {
                    type: "string",
                    nullable: true,
                    example: "9 de Julio 4325",
                },
                phone: {
                    type: "string",
                    example: "543425149452",
                },
                images: {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
                cover: {
                    type: "string",
                    nullable: true,
                },
                type: {
                    type: "string",
                    example: "Gimnasio",
                },
                name: {
                    type: "string",
                    example: "Gimnasio Grun",
                },
                description: {
                    type: "string",
                    example:
                        "El mejor gimnasio de Santa Fe. Musculación, Aerobics, Step y muchas actividades más!",
                },
                locality: {
                    type: "string",
                    example: "Rosario",
                },
                postalCode: {
                    type: "string",
                    example: "s2000",
                },
                country: {
                    type: "string",
                    example: "Argentina",
                },
                deletedAt: {
                    type: "string",
                    nullable: true,
                },
                createdAt: {
                    type: "string",
                    example: "2021-09-10T18:00:17.794Z",
                },
                updatedAt: {
                    type: "string",
                    example: "2021-09-10T18:00:17.794Z",
                },
            },
        };
    }
}
