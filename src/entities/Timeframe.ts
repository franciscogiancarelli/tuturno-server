import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    OneToMany,
    DeleteDateColumn,
} from "typeorm";
import Business from "./Business";
import Appointment from "./Appointment";
import Resource from "./Resource";
import User from "./User";
import Schema from "typedef/Schema";

@Entity()
export default class Timeframe {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column({ nullable: false, type: "timestamptz" })
    from!: Date;

    @Column({ nullable: false, type: "timestamptz" })
    to!: Date;

    @Column({ nullable: false })
    width!: number;

    @Column({ nullable: true, type: "uuid" })
    recurrentId?: string;

    @DeleteDateColumn({ type: "timestamptz" })
    deletedAt?: Date;

    @ManyToOne(() => Business, (business) => business.timeframes, {
        nullable: false,
    })
    business!: Business;

    @ManyToOne(() => User, (user) => user.timeframes, {
        nullable: false,
    })
    owner!: User;

    @OneToMany(() => Appointment, (appointment) => appointment.timeframe)
    appointments!: Appointment[];

    @ManyToOne(() => Resource, (resource) => resource.timeframes, {
        nullable: false,
        onDelete: "CASCADE",
    })
    resource!: Resource;

    public static getSchema(): Schema {
        return {
            properties: {
                id: {
                    type: "string",
                    example: "7d59cb4a-9151-4c63-82b1-a65d0817fa2b",
                },
                from: {
                    type: "string",
                    nullable: false,
                    example: "2020-10-10 09:00:00",
                },
                to: {
                    type: "string",
                    nullable: false,
                    example: "2020-10-10 08:00:00",
                },
                width: {
                    type: "number",
                    nullable: false,
                    example: 1,
                },
            },
        };
    }
}
