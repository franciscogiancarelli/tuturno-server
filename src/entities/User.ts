import {
    Entity,
    Column,
    DeleteDateColumn,
    OneToMany,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import Business from "./Business";
import Appointment from "./Appointment";
import Resource from "./Resource";
import Timeframe from "./Timeframe";
import Schema from "typedef/Schema";
import Review from "./Review";

@Entity()
export default class User {
    @PrimaryGeneratedColumn("uuid")
    uid!: string;

    @Column({ nullable: true })
    address?: string;

    @Column({ nullable: false, unique: true })
    email!: string;

    @Column({ nullable: true, select: false })
    password?: string;

    @Column({ nullable: false })
    name!: string;

    @Column({ nullable: false })
    phone!: string;

    @OneToMany(() => Appointment, (appointment) => appointment.owner)
    appointments!: Array<Appointment>;

    @Column({ nullable: false, default: false })
    isTesting!: boolean;

    @OneToMany(() => Timeframe, (timeframe) => timeframe.owner)
    timeframes!: Array<Timeframe>;

    @OneToMany(() => Business, (business) => business.owner)
    businesses!: Array<Business>;

    @OneToMany(() => Resource, (resource) => resource.owner)
    resources!: Array<Resource>;

    @OneToMany(() => Review, (review) => review.owner)
    reviews!: Array<Review>;

    @Column({ default: false })
    emailConfirmed?: boolean;

    @DeleteDateColumn({ type: "timestamptz" })
    deletedAt?: Date;

    @CreateDateColumn({ type: "timestamptz" })
    createdAt!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    updatedAt!: Date;

    public static getSchema(): Schema {
        return {
            properties: {
                uid: {
                    type: "string",
                    example: "7d59cb4a-9151-4c63-82b1-a65d0817fa2b",
                },
                address: {
                    type: "string",
                    example: "9 de Julio 4325",
                },
                email: {
                    type: "string",
                    example: "franciscogiancarelli@tuturno.com",
                },
                name: {
                    type: "string",
                    example: "Francisco Giancarelli",
                },
                phone: {
                    type: "string",
                    example: "+54 9 342382491",
                },
            },
        };
    }
}
