import { SwaggerUiOptions } from "swagger-ui-express";

const swaggerUiOptions: SwaggerUiOptions = {
    customCss: ".swagger-ui .topbar { display: none }",
    customSiteTitle: "Tuturno API Docs",
    customfavIcon: "./public/favicon.ico",
    swaggerOptions: {
        tryItOutEnabled: true,
        persistAuthorization: true,
        filter: true,
        docExpansion: "none",
    },
};

export default swaggerUiOptions;
