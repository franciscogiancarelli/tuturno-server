// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config();
import i18next from "i18next";
import "reflect-metadata";
import { createConnection } from "typeorm";
import locales from "./locales";

createConnection()
    .then(() =>
        i18next.init({
            compatibilityJSON: "v3",
            lng: process.env.LOCALE,
            debug: false,
            resources: locales,
        })
    )
    .then(() => require("app"));
