import i18next from "i18next";
import locales from "locales";
import "iconv-lite/encodings";

export const mockedSend = jest.fn();

jest.mock("@sendgrid/mail", () => {
    const sgMail = jest.requireActual("@sendgrid/mail");
    mockedSend.mockResolvedValue([{ statusCode: 202 }]);
    sgMail.send = mockedSend;
    sgMail.setApiKey = jest.fn();
    return sgMail;
});

jest.mock("minio", () => {
    const Minio = jest.requireActual("minio");
    class MockedClient {
        async fPutObject() {
            return null;
        }

        async removeObject() {
            return null;
        }
    }
    Minio.Client = MockedClient;
    return Minio;
});

global.console.info = jest.fn();
i18next.init({
    compatibilityJSON: "v3",
    lng: process.env.LOCALE,
    debug: false,
    resources: locales,
});
