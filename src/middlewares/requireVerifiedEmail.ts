import User from "entities/User";
import { RequestHandler } from "express";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import { getRepository } from "typeorm";
const UsersRepository = getRepository(User);

const requireVerifiedEmail: RequestHandler = async (req, res, next) => {
    /** Search if user already exists */
    const auth: Auth = JSON.parse(req.headers.authorization as string);

    /** Get the owner */
    const user = await UsersRepository.createQueryBuilder("user")
        .where("uid = :uid", { uid: auth.uid })
        .getOneOrFail();

    if (user.emailConfirmed) next();
    else {
        res.status(403).send(
            new TTError({
                code: 403,
                message: __("general.emailNotConfirmed"),
            })
        );
    }
};

export default requireVerifiedEmail;
