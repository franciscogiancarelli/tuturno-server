import { RequestHandler } from "express";
import jwt from "jsonwebtoken";
import { __ } from "locales";
import TTError from "typedef/TTError";

const authenticate: RequestHandler = async (req, res, next) => {
    try {
        if (req.headers.authorization) {
            const token = req.headers.authorization.replace("Bearer ", "");
            const decodedToken = jwt.verify(
                token,
                process.env.SECRET as string
            );
            req.headers.authorization = JSON.stringify(decodedToken);
            next();
        } else {
            res.status(403).send(
                new TTError(__("general.unauthorized"), "403")
            );
        }
    } catch (err) {
        res.status(401).send(new TTError(__("general.invalidToken"), "401"));
    }
};

export default authenticate;
