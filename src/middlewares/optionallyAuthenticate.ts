import { RequestHandler } from "express";
import jwt from "jsonwebtoken";
import TTError from "typedef/TTError";

const optionallyAuthenticate: RequestHandler = async (req, res, next) => {
    try {
        if (req.headers.authorization) {
            const token = req.headers.authorization.replace("Bearer ", "");
            const decodedToken = jwt.verify(
                token,
                process.env.SECRET as string
            );
            req.headers.authorization = JSON.stringify(decodedToken);
            next();
        } else next();
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default optionallyAuthenticate;
