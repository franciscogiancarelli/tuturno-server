import { RequestHandler } from "express";
import { validationResult } from "express-validator";
import { __ } from "locales";
import TTError from "typedef/TTError";

const validationHandler: RequestHandler = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) next();
    else {
        res.status(400).send(
            new TTError({
                code: 400,
                message: __("general.validationError"),
                errors: errors.array(),
            })
        );
    }
};

export default validationHandler;
