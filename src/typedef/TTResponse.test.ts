import TTResponse from "./TTResponse";

describe("TTResponse works as expected", () => {
    test("can be constructed from string", async () => {
        const ttresponse = new TTResponse("string");
        expect(ttresponse.message).toEqual("string");
        expect(ttresponse.code).toEqual("200");
    });

    test("can be constructed from response like with data", async () => {
        const ttresponse = new TTResponse({ data: "response" });
        expect(ttresponse.data).toEqual("response");
        expect(ttresponse.code).toEqual("200");
    });

    test("can be constructed from response like with message", async () => {
        const ttresponse = new TTResponse({ message: "response" });
        expect(ttresponse.message).toEqual("response");
        expect(ttresponse.code).toEqual("200");
    });

    test("can be constructed from response like with message and data", async () => {
        const ttresponse = new TTResponse({
            message: "response",
            data: "response",
        });
        expect(ttresponse.message).toEqual("response");
        expect(ttresponse.data).toEqual("response");
        expect(ttresponse.code).toEqual("200");
    });

    test("can be constructed from something else", async () => {
        const ttresponse = new TTResponse<{ else: string }>({
            else: "response",
        });
        expect(ttresponse.data?.else).toEqual("response");
        expect(ttresponse.code).toEqual("200");
    });
});
