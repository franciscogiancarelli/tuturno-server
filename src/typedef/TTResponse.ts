type ResponseLike<T> =
    | {
          message?: string;
          data?: T;
          status?: number;
      }
    | string
    | T;

class TTResponse<T = unknown> {
    public code = "200";
    public message?: string;
    public data?: T;
    public status = 200;

    constructor(responseLike: ResponseLike<T>) {
        if (typeof responseLike === "string") this.message = responseLike;
        else if ("data" in responseLike || "message" in responseLike) {
            this.data = responseLike.data;
            this.message = responseLike.message ?? "Request successful";
        } else {
            this.data = responseLike as T;
        }
    }
}

export default TTResponse;
