import Appointment from "entities/Appointment";

type AppointmentDTO = Omit<
    Appointment,
    | "from"
    | "to"
    | "resource"
    | "business"
    | "owner"
    | "timeframe"
    | "type"
    | "absent"
    | "payed"
    | "confirmed"
    | "createdAt"
    | "updatedAt"
    | "deletedAt"
> & {
    from: string;
};

export default AppointmentDTO;
