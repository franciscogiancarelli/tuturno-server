import { __ } from "locales";
import TTError from "./TTError";

describe("TTError works as expected", () => {
    test("can be constructed from string", async () => {
        const tterror = new TTError("string");
        expect(tterror.message).toEqual("string");
        expect(tterror.code).toEqual("500");
    });

    test("can be constructed from response like with code", async () => {
        const tterror = new TTError({ code: "123" });
        expect(tterror.code).toEqual("123");
    });

    test("can be constructed from response like with message", async () => {
        const tterror = new TTError({ message: "error" });
        expect(tterror.message).toEqual("error");
        expect(tterror.code).toEqual("500");
    });

    test("can be constructed from response like with error", async () => {
        const tterror = new TTError({ error: "error" });
        expect(tterror.error).toEqual("error");
        expect(tterror.code).toEqual("500");
    });

    test("can be constructed from response like with status", async () => {
        const tterror = new TTError({ status: 403 });
        expect(tterror.status).toEqual(403);
    });

    test("can be constructed from response like with axios response", async () => {
        const tterror = new TTError({
            response: {
                status: 123,
                data: { message: "error", error: "error" },
            },
        });
        expect(tterror.message).toEqual("error");
        expect(tterror.error).toEqual("error");
        expect(tterror.code).toEqual("123");
    });

    test("can be constructed from something else", async () => {
        const tterror = new TTError({
            else: "response",
        });
        expect(tterror.message).toEqual(JSON.stringify({ else: "response" }));
        expect(tterror.code).toEqual("500");
    });

    test("can be constructed from something wierd", async () => {
        const tterror = new TTError(300);
        expect(tterror.message).toEqual(__("general.unknownError"));
        expect(tterror.code).toEqual("500");
    });
});
