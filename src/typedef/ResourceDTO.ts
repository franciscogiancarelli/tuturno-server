import Resource from "entities/Resource";

type ResourceDTO = Omit<
    Resource,
    | "business"
    | "owner"
    | "timeframes"
    | "appointments"
    | "appointmentTypes"
    | "createdAt"
    | "updatedAt"
    | "deletedAt"
>;

export default ResourceDTO;
