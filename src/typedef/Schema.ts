type Property = {
    type?: string;
    nullable?: boolean;
    example?: string | number;
    items?: Property;
    $ref?: string;
};

interface Schema {
    properties: {
        [key: string]: Property;
    };
}

export default Schema;
