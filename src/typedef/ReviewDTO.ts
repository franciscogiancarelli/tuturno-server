import Review from "entities/Review";

type BusinessDTO = Omit<Review, "business" | "owner" | "createdAt">;

export default BusinessDTO;
