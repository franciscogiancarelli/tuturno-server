import Business from "entities/Business";

type BusinessDTO = Omit<
    Business,
    | "appointments"
    | "timeframes"
    | "resources"
    | "owner"
    | "images"
    | "appointmentTypes"
    | "reviews"
    | "createdAt"
    | "updatedAt"
    | "deletedAt"
>;

export default BusinessDTO;
