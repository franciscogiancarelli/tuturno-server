import { AxiosResponse } from "axios";
import { ValidationError } from "express-validator";
import { __ } from "locales";

export interface ErrorLike {
    message?: string;
    code?: string;
    status?: number;
    error?: string;
    errors?: ValidationError[];
}

class TTError {
    message: string;
    code: string;
    status: number;
    error?: string;
    errors?: ValidationError[];

    constructor(error: ErrorLike | string | unknown, code?: string | number) {
        if (typeof error === "object" && error) {
            /** Check if its an axios response */
            if ("response" in error) {
                const axiosResponse = (error as { response: AxiosResponse })
                    .response;
                this.code = String(axiosResponse.status);
                this.status = axiosResponse.status;
                this.message =
                    axiosResponse.data.message ||
                    axiosResponse.data.error ||
                    axiosResponse.statusText;
                this.error = axiosResponse.data.error;
            } else if (
                "code" in error ||
                "message" in error ||
                "error" in error ||
                "data" in error ||
                "status" in error
            ) {
                const errorLike = error as ErrorLike;
                if (errorLike.code) this.code = String(errorLike.code);
                else this.code = "500";
                if (errorLike.status) this.status = Number(errorLike.status);
                else if (this.code && Number.isInteger(this.code))
                    this.status = Number(this.code);
                else this.status = 500;
                if (errorLike.message) this.message = errorLike.message;
                else this.message = __("general.unknownError");
                if (errorLike.error) this.error = errorLike.error;
                if (errorLike.errors) this.errors = errorLike.errors;
            } else {
                this.code = "500";
                this.status = 500;
                this.message = JSON.stringify(error);
            }
        } else if (typeof error === "string") {
            this.message = error;
            this.code = String(code ?? "500");
            this.status = Number.isInteger(code) ? Number(this.code) : 500;
        } else {
            this.message = __("general.unknownError");
            this.code = "500";
            this.status = 500;
        }
    }
}

export default TTError;
