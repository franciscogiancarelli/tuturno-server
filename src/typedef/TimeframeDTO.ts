import Timeframe from "entities/Timeframe";

type TimeframeDTO = Omit<
    Timeframe,
    | "appointments"
    | "from"
    | "to"
    | "recurrentId"
    | "resource"
    | "business"
    | "owner"
    | "appointmentTypes"
> & {
    from: string;
    to: string;
};

export default TimeframeDTO;
