import UserDTO from "./UserDTO";
import BusinessDTO from "./BusinessDTO";
import AppointmentTypeDTO from "./AppointmentTypeDTO";
import ResourceDTO from "./ResourceDTO";
import TimeframeDTO from "./TimeframeDTO";
import AppointmentDTO from "./AppointmentDTO";
import ReviewDTO from "./ReviewDTO";

interface TestEntities {
    business: BusinessDTO;
    fakeBusiness: BusinessDTO;
    otherBusiness: BusinessDTO;
    deletedBusiness: BusinessDTO;
    appointment: AppointmentDTO;
    otherAppointment: AppointmentDTO;
    notConfirmedAppointment: AppointmentDTO;
    reviewMakerAppointment: AppointmentDTO;
    appointmentType: AppointmentTypeDTO;
    otherAppointmentType: AppointmentTypeDTO;
    maxUserRequestAppointmentType: AppointmentTypeDTO;
    resource: ResourceDTO;
    businessRegisteredUser: UserDTO;
    timeframe: TimeframeDTO;
    todayTimeframe: TimeframeDTO;
    user: UserDTO;
    testingUser: UserDTO;
    otherUser: UserDTO;
    unverifiedUser: UserDTO;
    review: ReviewDTO;
}

export default TestEntities;
