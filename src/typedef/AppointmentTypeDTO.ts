import AppointmentType from "entities/AppointmentType";

type AppointmentTypeDTO = Omit<
    AppointmentType,
    "resources" | "business" | "createdAt" | "updatedAt" | "deletedAt"
>;

export default AppointmentTypeDTO;
