import User from "entities/User";

type UserDTO = Omit<
    User,
    | "businesses"
    | "appointments"
    | "timeframes"
    | "resources"
    | "reviews"
    | "createdAt"
    | "updatedAt"
    | "deletedAt"
    | "emailConfirmed"
>;

export default UserDTO;
