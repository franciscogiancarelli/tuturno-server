import CenteredContainer from "@tuturno/tuturno-ui/CenteredContainer";
import { renderToString } from "react-dom/server";

const UnsuccessfulAcceptedAppointment: React.FC = () => {
    return (
        <html lang="en">
            <head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <link
                    rel="stylesheet"
                    href="https://tuturno.com.ar/ui/assets/tuturno.css"
                />
                <title>UnsuccessfulAcceptedAppointment</title>
            </head>
            <body className="bg-light vw-100 vh-100 d-flex flex-column container-fluid">
                <CenteredContainer>
                    <div className="p-3 bg-white mt-3 rounded shadow-sm">
                        <h4>
                            Lo sentimos, parece que tuvimos un problema al
                            aceptar el turno.
                        </h4>
                        <p>
                            Por favor, intenta nuevamente más tarde. Si sigue
                            fallando comunicate con nosotros a{" "}
                            <a href="mailto:soporte@tuturno.com.ar">
                                soporte@tuturno.com.ar
                            </a>
                        </p>
                    </div>
                </CenteredContainer>
            </body>
        </html>
    );
};

export default UnsuccessfulAcceptedAppointment;

export const stringedUnsuccessfulAcceptedAppointment = renderToString(
    <UnsuccessfulAcceptedAppointment />
);
