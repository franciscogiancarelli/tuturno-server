import Button from "@tuturno/tuturno-ui/Button";
import { renderToString } from "react-dom/server";

const EmailVerificationError: React.FC = () => {
    return (
        <html lang="es">
            <head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <link
                    rel="stylesheet"
                    href="https://tuturno.com.ar/ui/assets/tuturno.css"
                />
                <title>Tuturno - Email Verification Error</title>
            </head>
            <body className="bg-light vw-100 vh-100 d-flex flex-column container-fluid">
                <div className="p-3 flex-fill row align-items-center justify-content-center">
                    <div className="col-xs-12 col-sm-8 col-md-6 col-lg-4 col-xl-3">
                        <div className="bg-white p-4 rounded shadow">
                            <h2>😕 Lo sentimos</h2>
                            <p>
                                Parece que tuvimos un problema al verificar tu
                                correo
                            </p>
                            <Button
                                text="Volver al inicio"
                                onClick={() =>
                                    (location.href = process.env.HOST as string)
                                }
                            />
                        </div>
                    </div>
                </div>
                <div className="bg-white p-3 row">
                    <div className="col-xs-12">
                        <h6>¿Necesitas ayuda?</h6>
                        <p>
                            Comunicate con nostros a{" "}
                            <a href="mailto:soporte@tuturno.com.ar">
                                soporte@tuturno.com.ar
                            </a>
                        </p>
                    </div>
                </div>
            </body>
        </html>
    );
};

export default EmailVerificationError;

const stringedEmailVerificationError = renderToString(
    <EmailVerificationError />
);

export { stringedEmailVerificationError };
