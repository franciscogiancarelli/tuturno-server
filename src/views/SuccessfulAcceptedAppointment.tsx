import Button from "@tuturno/tuturno-ui/Button";
import CenteredContainer from "@tuturno/tuturno-ui/CenteredContainer";
import { renderToString } from "react-dom/server";

const SuccessfulAcceptedAppointment: React.FC = () => {
    return (
        <html lang="en">
            <head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <link
                    rel="stylesheet"
                    href="https://tuturno.com.ar/ui/assets/tuturno.css"
                />
                <title>SuccessfulAcceptedAppointment</title>
            </head>
            <body className="bg-light vw-100 vh-100 d-flex flex-column container-fluid">
                <CenteredContainer>
                    <div className="p-3 bg-white mt-3 rounded shadow-sm">
                        <h4>¡Gracias por aceptar el turno!</h4>
                        <p>
                            Ya le confirmamos tu asistencia al dueño del lugar,
                            esperamos que lo disfrutes!
                        </p>
                        <a href={`${process.env.HOST}/me/appointments`}>
                            <Button text="Ver mis turnos" />
                        </a>
                    </div>
                </CenteredContainer>
            </body>
        </html>
    );
};

export default SuccessfulAcceptedAppointment;

export const stringedSuccessfulAcceptedAppointment = renderToString(
    <SuccessfulAcceptedAppointment />
);
