import Button from "@tuturno/tuturno-ui/Button";
import CenteredContainer from "@tuturno/tuturno-ui/CenteredContainer";
import { renderToString } from "react-dom/server";

const UnsuccessfulPasswordReset: React.FC = () => {
    return (
        <html lang="en">
            <head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <link
                    rel="stylesheet"
                    href="https://tuturno.com.ar/ui/assets/tuturno.css"
                />
                <title>UnsuccessfulPasswordReset</title>
            </head>
            <body className="bg-light vw-100 vh-100 d-flex flex-column container-fluid">
                <CenteredContainer>
                    <div className="p-3 bg-white mt-3 rounded shadow-sm">
                        <h4>
                            Lo sentimos, no pudimos reestablecer tu contraseña.
                        </h4>
                        <a href={process.env.HOST}>
                            <Button text="Volver a la pantalla principal" />
                        </a>
                    </div>
                </CenteredContainer>
            </body>
        </html>
    );
};

export default UnsuccessfulPasswordReset;

export const stringedUnsuccessfulPasswordReset = renderToString(
    <UnsuccessfulPasswordReset />
);
