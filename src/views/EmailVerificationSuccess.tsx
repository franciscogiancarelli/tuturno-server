import Button from "@tuturno/tuturno-ui/Button";
import { __ } from "locales";
import { renderToString } from "react-dom/server";

const EmailVerificationSuccess: React.FC = () => {
    return (
        <html lang="es">
            <head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <link
                    rel="stylesheet"
                    href="https://tuturno.com.ar/ui/assets/tuturno.css"
                />
                <title>Email Verified</title>
            </head>
            <body className="bg-light vw-100 vh-100 d-flex flex-column container-fluid">
                <div className="p-3 flex-fill row align-items-center justify-content-center">
                    <div className="col-xs-12 col-sm-8 col-md-6 col-lg-4 col-xl-3">
                        <div className="bg-white p-4 rounded shadow">
                            <h2>{__("me.emailVerificationSuccessTitle")}</h2>
                            <p>
                                {__("me.emailVerificationSuccessDescription")}
                            </p>
                            <a href={`${process.env.HOST}`}>
                                <Button
                                    text={__(
                                        "me.emailVerificationSuccessButton"
                                    )}
                                />
                            </a>
                        </div>
                    </div>
                </div>
                <div className="bg-white p-3 row">
                    <div className="col-xs-12">
                        <h6>{__("me.emailVerificationSuccessHelpTitle")}</h6>
                        <p>
                            {__("me.emailVerificationSuccessHelpDescription")}{" "}
                            <a href="mailto:soporte@tuturno.com.ar">
                                soporte@tuturno.com.ar
                            </a>
                        </p>
                    </div>
                </div>
            </body>
        </html>
    );
};

export default EmailVerificationSuccess;

const stringedEmailVerificationSuccess = renderToString(
    <EmailVerificationSuccess />
);

export { stringedEmailVerificationSuccess };
