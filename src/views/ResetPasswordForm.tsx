import Button from "@tuturno/tuturno-ui/Button";
import CenteredContainer from "@tuturno/tuturno-ui/CenteredContainer";
import FormInput from "@tuturno/tuturno-ui/FormInput";
import { useForm } from "react-hook-form";

export interface PasswordResetFormProps {
    token: string;
}

const PasswordResetForm: React.FC<PasswordResetFormProps> = ({ token }) => {
    const form = useForm<{ password: string }>();

    return (
        <html lang="en">
            <head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <link
                    rel="stylesheet"
                    href="https://tuturno.com.ar/ui/assets/tuturno.css"
                />
                <title>PasswordResetForm</title>
            </head>
            <body className="bg-light vw-100 vh-100 d-flex flex-column container-fluid">
                <CenteredContainer>
                    <form
                        className="bg-white rounded sahdow-sm p-3 mt-3"
                        action={`/api/me/reset-password/${token}`}
                        method="POST"
                    >
                        <h4>Ingresá una nueva contraseña</h4>
                        <FormInput
                            required={{
                                value: true,
                                message: "Necesitamos una nueva contraseña",
                            }}
                            type="password"
                            autoComplete="new-password"
                            name="password"
                            label="Nueva contraseña"
                            register={form.register}
                            formState={form.formState}
                        />
                        <Button
                            text="Cambiar contraseña"
                            type="submit"
                            className="mt-3"
                        />
                    </form>
                </CenteredContainer>
            </body>
        </html>
    );
};

export default PasswordResetForm;
