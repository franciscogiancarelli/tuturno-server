import Appointment, { CancelledBy } from "entities/Appointment";
import Business from "entities/Business";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import sendEmailCancelByBusiness from "./sendEmailCancelByBusiness";

const cancelAppointmentsByBusiness = async (
    ids: string[],
    auth: Auth,
    message?: string
): Promise<TTResponse<string> | TTError> => {
    const AppointmentRepository = getRepository(Appointment);
    const BusinessRepository = getRepository(Business);

    const business = await BusinessRepository.createQueryBuilder("business")
        .where('business."ownerUid" = :uid', auth)
        .leftJoinAndSelect("business.appointments", "appointment")
        .andWhere("appointment.id = :id", { id: ids[0] })
        .getOne();
    if (!business) throw __("appointments.cantCancel");

    const { affected } = await AppointmentRepository.createQueryBuilder(
        "appointment"
    )
        .update({ cancelledBy: CancelledBy.business })
        .where("appointment.id IN (:...ids)", { ids })
        .andWhere('appointment."businessId" = :businessId', {
            businessId: business.id,
        })
        .execute();

    if (affected !== ids.length) {
        throw __("appointments.cantCancel");
    } else {
        for (const id of ids) {
            const appointment = await AppointmentRepository.createQueryBuilder(
                "appointment"
            )
                .where("appointment.id = :id", { id: id })
                .leftJoinAndSelect("appointment.owner", "owner")
                .leftJoinAndSelect("appointment.type", "appointmentType")
                .leftJoinAndSelect("appointment.resource", "resource")
                .getOneOrFail();

            await sendEmailCancelByBusiness(business, appointment, message);
        }
        return new TTResponse(__("appointments.appointmentCancelled"));
    }
};

export default cancelAppointmentsByBusiness;
