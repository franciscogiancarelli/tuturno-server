// eslint-disable-next-line @typescript-eslint/no-var-requires
const Minio = require("minio");

const minioClient = new Minio.Client({
    endPoint: process.env.SPACES_ENDPOINT as string,
    useSSL: true,
    accessKey: process.env.SPACES_ACCESS_KEY as string,
    secretKey: process.env.SPACES_SECRET_KEY as string,
});
async function removeImage(imageUrl: string): Promise<void> {
    return minioClient.removeObject(
        "tuturno-space",
        imageUrl.split("digitaloceanspaces.com/")[1]
    );
}

export default removeImage;
