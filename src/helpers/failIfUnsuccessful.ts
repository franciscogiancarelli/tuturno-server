import { Response } from "supertest";

function failIfUnsuccessful(response: Response): Response {
    if (response.statusCode >= 300 || response.statusCode < 200) {
        // eslint-disable-next-line no-console
        console.error("Error here", response);
        throw response.body;
    } else return response;
}

export default failIfUnsuccessful;
