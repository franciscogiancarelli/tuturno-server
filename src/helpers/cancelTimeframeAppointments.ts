import Timeframe from "entities/Timeframe";
import Auth from "typedef/Auth";
import cancelAppointmentsByBusiness from "./cancelAppointmentsByBusiness";

async function cancelTimeframesAppointments(
    timeframes: Timeframe[],
    auth: Auth,
    message?: string
): Promise<void> {
    /** Cancel appointments and send cancelation emails to appointments owners*/
    const appointmentsIds = timeframes
        .map(({ appointments }) => appointments.map(({ id }) => id))
        .flat();
    await cancelAppointmentsByBusiness(appointmentsIds, auth, message);
}

export default cancelTimeframesAppointments;
