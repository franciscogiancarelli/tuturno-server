import jwt from "jsonwebtoken";
import sgMail from "@sendgrid/mail";
import User from "entities/User";

/**
 * Use this method to send any user an email with instructions
 * to verify their account.
 * @param user User you want to send the registration email to
 */
async function sendAccountVerificationEmail(
    user: Pick<User, "email" | "name">
): Promise<void> {
    const token = jwt.sign({ email: user.email }, process.env.SECRET as string);
    const verificationLink = `${process.env.HOST}/api/me/verify-email/${token}`;

    await sgMail.send({
        to: user.email,
        from: {
            name: "Tuturno",
            email: "noreply@tuturno.com.ar",
        },
        templateId: "d-689e6f2cc2ec44bca95b8d68ea57804b",
        dynamicTemplateData: {
            title: `🎉 🎊  Bienvenid@ a Tuturno  🎊 🥳`,
            subject: "Verificá tu correo",
            subtitle: `¡Gracias ${user.name} por sumarte! 🙌`,
            description: `Nos alegra que hayas decidido forma parte de nuestra comunidad. Ya estás a un paso de pedir tu primer turno, solo necesitamos verificar tu correo.`,
            language: "es-AR",
            action: {
                name: "Verifica tu correo",
                href: `${verificationLink}`,
            },
        },
        mailSettings: {
            sandboxMode: {
                enable: process.env.JEST_WORKER_ID !== undefined,
            },
        },
    });
}

export default sendAccountVerificationEmail;
