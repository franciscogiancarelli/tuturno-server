import Appointment from "entities/Appointment";
import AppointmentType from "entities/AppointmentType";
import Timeframe from "entities/Timeframe";
import User from "entities/User";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ics = require("ics");

async function createAppointmentEvent(
    appointment: Pick<Appointment, "id" | "from" | "to">,
    timeframe: Timeframe,
    appointmentType: AppointmentType,
    owner: Pick<User, "email" | "name">
): Promise<string> {
    /** Create ics file */
    return new Promise<string>((res, rej) =>
        ics.createEvent(
            {
                start: [
                    new Date(appointment.from).getFullYear(),
                    new Date(appointment.from).getMonth() + 1,
                    new Date(appointment.from).getDate(),
                    new Date(appointment.from).getHours(),
                    new Date(appointment.from).getMinutes(),
                ],
                duration: {
                    minutes:
                        (new Date(appointment.to).getTime() -
                            new Date(appointment.from).getTime()) /
                        60000,
                },
                title: `Turno en ${timeframe.business.name}`,
                description: `${appointmentType.name}`,
                location: timeframe.business.address,
                url: `${process.env.HOST}/appointment/${appointment.id}`,
                geo: {
                    lat: timeframe.business.latitude,
                    lon: timeframe.business.longitude,
                },
                categories: [appointmentType.name, timeframe.business.type],
                status: "CONFIRMED",
                busyStatus: "BUSY",
                organizer: {
                    name: timeframe.business.name,
                    email: owner.email,
                },
                attendees: [
                    {
                        name: owner.name,
                        email: owner.email,
                        rsvp: true,
                        partstat: "NEED-ACTIONS",
                        role: "REQ-PARTICIPANT",
                    },
                ],
            },
            (err: string, value: string) => (err ? rej(err) : res(value))
        )
    );
}

export default createAppointmentEvent;
