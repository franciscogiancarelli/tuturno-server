import jwt from "jsonwebtoken";
import sgMail from "@sendgrid/mail";
import User from "entities/User";

/**
 * Use this method to send any user an email with instructions
 * to register a new password for their tuturno account.
 * @param user User you want to send the registration email to
 */
async function sendPasswordRegistrationEmail(user: User): Promise<void> {
    const token = jwt.sign({ email: user.email }, process.env.SECRET as string);
    const passwordResetLink = `${process.env.HOST}/api/me/show-reset-form/${token}`;
    await sgMail.send({
        to: user.email,
        from: {
            name: "Tuturno",
            email: "noreply@tuturno.com.ar",
        },
        templateId: "d-689e6f2cc2ec44bca95b8d68ea57804b",
        dynamicTemplateData: {
            subject: "Registrá una contraseña",
            title: `🎉 🎊  Bienvenido a Tuturno  🎊 🥳`,
            subtitle: `¡Gracias ${user.name} por sumarte! 🙌`,
            description: `Parece que estas queriendo ingresar en tu cuenta, para eso necesitamos que registres una contraseña. Para hacerlo podes entrar en registrar contraseña. Si no intentaste entrar en tu cuenta, no hace falta que hagas nada.`,
            language: "es-AR",
            action: {
                name: "Registrar contraseña",
                href: `${passwordResetLink}`,
            },
        },
        mailSettings: {
            sandboxMode: {
                enable: process.env.JEST_WORKER_ID !== undefined,
            },
        },
    });
}

export default sendPasswordRegistrationEmail;
