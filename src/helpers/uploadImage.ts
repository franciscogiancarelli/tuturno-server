// eslint-disable-next-line @typescript-eslint/no-var-requires
const Minio = require("minio");
import { writeFile } from "fs/promises";
import TTError from "typedef/TTError";
import { __ } from "locales";

const minioClient = new Minio.Client({
    endPoint: process.env.SPACES_ENDPOINT as string,
    useSSL: true,
    accessKey: process.env.SPACES_ACCESS_KEY as string,
    secretKey: process.env.SPACES_SECRET_KEY as string,
});

async function uploadImage(
    base64Image: string,
    imageId: string
): Promise<string> {
    /** Upload the cover to tuturno-space */
    const base64Meta = base64Image.split(",")[0];
    const base64Data = base64Image.split(",")[1];
    let type: string;
    if (base64Meta.includes("image/png")) type = "png";
    else if (base64Meta.includes("image/jpg")) type = "jpg";
    else if (base64Meta.includes("image/jpeg")) type = "jpeg";
    else if (base64Meta.includes("image/webp")) type = "webp";
    else throw new TTError(__("general.unsuportedImageType"));
    const buffer = Buffer.from(base64Data, "base64");
    const filePath = `temporalFile`;
    await writeFile(filePath, buffer);
    await minioClient.fPutObject(
        "tuturno-space",
        `images/${imageId}.${type}`,
        filePath,
        {
            "x-amz-acl": "public-read",
        }
    );

    return `https://tuturno-space.fra1.digitaloceanspaces.com/images/${imageId}.${type}`;
}

export default uploadImage;
