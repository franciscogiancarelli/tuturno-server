import sgMail from "@sendgrid/mail";
import Appointment from "entities/Appointment";
import Business from "entities/Business";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

async function sendEmailCancelByBusiness(
    business: Business,
    appointment: Appointment,
    message?: string
): Promise<void> {
    /** Send email invite */
    const printMessage = message ?? "";
    const appointmentTime = new Date(appointment.from).toLocaleTimeString();
    const appointmentDate = new Date(appointment.from).toLocaleDateString();
    await sgMail.send({
        to: appointment.owner.email,
        from: {
            name: "Tuturno",
            email: "noreply@tuturno.com.ar",
        },
        templateId: "d-689e6f2cc2ec44bca95b8d68ea57804b",
        dynamicTemplateData: {
            title: `${business.name} ha cancelado tu turno`,
            subtitle: appointment.type.name,
            subject: "Turno cancelado",
            description: `El negocio ${business.name} ha cancelado su turno del  ${appointmentDate} a las ${appointmentTime} para el turno ${appointment.type.name} con ${appointment.resource.name}.
            Mensaje del negocio: ${printMessage}`,
            language: "es-AR",
            action: {
                name: "Pedir otro turno",
                href: `${process.env.HOST}/business/${business.id}/checkout/${appointment.type.id}`,
            },
        },
        mailSettings: {
            sandboxMode: {
                enable: process.env.JEST_WORKER_ID !== undefined,
            },
        },
    });
}

export default sendEmailCancelByBusiness;
