import initTestEntities, { testServer } from "initTestEntities";
import { getConnection } from "typeorm";

describe("Businesses Router works well", () => {
    test("work as expected", async () => {
        await initTestEntities();
        await getConnection().close();
        testServer.close();
    });
});
