import { Server } from "http";
import request from "supertest";
import { createConnection } from "typeorm";
import faker from "faker";
import jwt from "jsonwebtoken";
import TestEntities from "typedef/TestEntities";
import { Currency } from "entities/AppointmentType";
import failIfUnsuccessful from "helpers/failIfUnsuccessful";
export let testApp: unknown;
export let testServer: Server;
export let testToken: string;
export let testTestingToken: string;
export let testOtherToken: string;
const testTimeframeFrom = new Date("2033-10-10 08:00:00").toISOString();
const testTimeframeTo = new Date("2033-10-10 19:00:00").toISOString();

const testTimeframeWidth = 2;
const testAppointmentFrom = new Date("2033-10-10 09:00:00").toISOString();
const testTodayTimeframeFrom = new Date(
    new Date().setMinutes(0, 0)
).toISOString();
const testTodayTimeframeTo = new Date(
    new Date().setHours(new Date(testTodayTimeframeFrom).getHours() + 8)
).toISOString();
const testTodayAppointmentFrom = testTodayTimeframeFrom;
const testSentAppointmentFrom = new Date("2033-10-10 14:00:00").toISOString();
const testOtherAppointmentFrom = new Date("2033-10-10 11:00:00").toISOString();
const appointmentTypeId = faker.datatype.uuid();
const otherAppointmentTypeId = faker.datatype.uuid();
const maxUserRequestAppointmentTypeId = faker.datatype.uuid();
const reviewMakerAppointmentId = faker.datatype.uuid();
// Formatting of values
export const fakerPhoneNumberFormat = "549342#######";
const passwordFormatted = faker.internet.password() + "1";
const appointmentTypePriceFormatted = faker.datatype.number().toString();

export const testEntities: TestEntities = {
    user: {
        uid: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: passwordFormatted,
        address: faker.address.direction(),
        isTesting: false,
    },
    testingUser: {
        uid: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: passwordFormatted,
        address: faker.address.direction(),
        isTesting: true,
    },
    otherUser: {
        uid: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: passwordFormatted,
        address: faker.address.direction(),
        isTesting: false,
    },
    unverifiedUser: {
        uid: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: passwordFormatted,
        address: faker.address.direction(),
        isTesting: false,
    },
    businessRegisteredUser: {
        uid: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: passwordFormatted,
        address: faker.address.direction(),
        isTesting: false,
    },
    business: {
        id: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        description: faker.company.catchPhraseDescriptor(),
        type: faker.commerce.productMaterial(),
        name: faker.company.companyName(),
        address: faker.address.direction(),
        latitude: faker.datatype.float(),
        longitude: faker.datatype.float(),
        isFake: false,
    },
    fakeBusiness: {
        id: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        description: faker.company.catchPhraseDescriptor(),
        type: faker.commerce.productMaterial(),
        name: faker.company.companyName(),
        address: faker.address.direction(),
        latitude: faker.datatype.float(),
        longitude: faker.datatype.float(),
        isFake: true,
    },
    otherBusiness: {
        id: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        description: faker.company.catchPhraseDescriptor(),
        type: faker.commerce.productMaterial(),
        name: faker.company.companyName(),
        address: faker.address.direction(),
        latitude: faker.datatype.float(),
        longitude: faker.datatype.float(),
        isFake: false,
    },
    deletedBusiness: {
        id: faker.datatype.uuid(),
        phone: faker.phone.phoneNumber(fakerPhoneNumberFormat),
        description: faker.company.catchPhraseDescriptor(),
        type: faker.commerce.productMaterial(),
        name: faker.company.companyName(),
        address: faker.address.direction(),
        latitude: faker.datatype.float(),
        longitude: faker.datatype.float(),
        isFake: false,
    },
    /** This appointment type doesn't need confirmation of the appointment*/
    appointmentType: {
        id: appointmentTypeId,
        name: faker.commerce.productName(),
        description: faker.commerce.productDescription(),
        price: appointmentTypePriceFormatted,
        currency: Currency.ARS,
        duration: 60,
        needConfirmation: false,
    },
    /** This appointment type needs confirmation of the appointment and doesn't have minimumTimeInAdvance*/
    otherAppointmentType: {
        id: otherAppointmentTypeId,
        name: faker.commerce.productName(),
        description: faker.commerce.productDescription(),
        price: appointmentTypePriceFormatted,
        currency: Currency.ARS,
        duration: 60,
        needConfirmation: true,
        minimumTimeInAdvance: 0,
    },
    /** This appointment type has maxUserRequest properties*/
    maxUserRequestAppointmentType: {
        id: maxUserRequestAppointmentTypeId,
        name: faker.commerce.productName(),
        description: faker.commerce.productDescription(),
        price: appointmentTypePriceFormatted,
        currency: Currency.ARS,
        duration: 10,
        needConfirmation: true,
        maxDailyUserRequest: 1,
        maxWeeklyUserRequest: 1,
        maxMonthlyUserRequest: 1,
    },
    resource: {
        id: faker.datatype.uuid(),
        description: faker.commerce.productDescription(),
        name: faker.commerce.productName(),
        interval: 15,
    },
    timeframe: {
        id: faker.datatype.uuid(),
        from: testTimeframeFrom,
        to: testTimeframeTo,
        width: testTimeframeWidth,
    },
    /** This timeframe is for today*/
    todayTimeframe: {
        id: faker.datatype.uuid(),
        from: testTodayTimeframeFrom,
        to: testTodayTimeframeTo,
        width: testTimeframeWidth,
    },
    /** Related with first appointment type*/
    appointment: {
        id: faker.datatype.uuid(),
        from: testAppointmentFrom,
    },
    /** Related with first appointment type*/
    otherAppointment: {
        id: faker.datatype.uuid(),
        from: testOtherAppointmentFrom,
    },
    /** Related with second appointment type*/
    notConfirmedAppointment: {
        id: faker.datatype.uuid(),
        from: testOtherAppointmentFrom,
    },
    /** Related with second appointment type*/
    reviewMakerAppointment: {
        id: reviewMakerAppointmentId,
        from: testTodayAppointmentFrom,
    },
    review: {
        id: faker.datatype.uuid(),
        score: 3,
        description: "Test review",
    },
};

async function initTestEntities(): Promise<void> {
    try {
        await createConnection();
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const expressAplication = require("app");
        testApp = expressAplication.app;
        testServer = expressAplication.server;
        /** Create user */
        failIfUnsuccessful(
            await request(testApp)
                .post("/api/me/register")
                .set("Content-Type", "application/json")
                .send(testEntities.user)
        );
        /** Create testing user */
        failIfUnsuccessful(
            await request(testApp)
                .post("/api/me/register")
                .set("Content-Type", "application/json")
                .send(testEntities.testingUser)
        );
        /** Create other user */
        failIfUnsuccessful(
            await request(testApp)
                .post("/api/me/register")
                .set("Content-Type", "application/json")
                .send(testEntities.otherUser)
        );
        /** Create unverified user */
        failIfUnsuccessful(
            await request(testApp)
                .post("/api/me/register")
                .set("Content-Type", "application/json")
                .send(testEntities.unverifiedUser)
        );

        /** Confirm user account*/
        const userVerifyToken = jwt.sign(
            { email: testEntities.user.email },
            process.env.SECRET as string
        );
        failIfUnsuccessful(
            await request(testApp)
                .get(`/api/me/verify-email/${userVerifyToken}`)
                .send()
        );

        /** Confirm testing user account*/
        const testingUserVerifyToken = jwt.sign(
            { email: testEntities.testingUser.email },
            process.env.SECRET as string
        );
        failIfUnsuccessful(
            await request(testApp)
                .get(`/api/me/verify-email/${testingUserVerifyToken}`)
                .send()
        );

        /** Confirm other user's account */
        const otherUserVerifyToken = jwt.sign(
            { email: testEntities.otherUser.email },
            process.env.SECRET as string
        );
        failIfUnsuccessful(
            await request(testApp)
                .get(`/api/me/verify-email/${otherUserVerifyToken}`)
                .send()
        );

        /** Get user's token */
        const userLoginResponse = failIfUnsuccessful(
            await request(testApp)
                .post("/api/me/login")
                .set("Content-Type", "application/json")
                .send(testEntities.user)
        );
        const testingUserLoginResponse = failIfUnsuccessful(
            await request(testApp)
                .post("/api/me/login")
                .set("Content-Type", "application/json")
                .send(testEntities.testingUser)
        );
        const otherUserLoginResponse = failIfUnsuccessful(
            await request(testApp)
                .post("/api/me/login")
                .set("Content-Type", "application/json")
                .send(testEntities.otherUser)
        );
        testToken = userLoginResponse.body.data.token;
        testTestingToken = testingUserLoginResponse.body.data.token;
        testOtherToken = otherUserLoginResponse.body.data.token;

        /** Create a business */
        failIfUnsuccessful(
            await request(testApp)
                .post("/api/businesses")
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.business)
        );

        /** Create a fake business */
        failIfUnsuccessful(
            await request(testApp)
                .post("/api/businesses")
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testTestingToken}`)
                .send(testEntities.fakeBusiness)
        );

        /** Add image to business */
        failIfUnsuccessful(
            await request(testApp)
                .post(`/api/businesses/addImage?id=${testEntities.business.id}`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send({
                    image: "data:image/webp;base64,asdfwefkjndfweDQWFWsdagasrtasdgare",
                })
        );

        /** Create other business */
        failIfUnsuccessful(
            await request(testApp)
                .post("/api/businesses")
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.otherBusiness)
        );

        /** Create deleted business */
        failIfUnsuccessful(
            await request(testApp)
                .post("/api/businesses")
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.deletedBusiness)
        );

        failIfUnsuccessful(
            await request(testApp)
                .delete(`/api/businesses?id=${testEntities.deletedBusiness.id}`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send()
        );

        /** Create an appointment type */
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/appointmentTypes?businessId=${testEntities.business.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.appointmentType)
        );

        /** Create another appointment type that needs confirmation */
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/appointmentTypes?businessId=${testEntities.business.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.otherAppointmentType)
        );

        /** Create an appointment type with property maxUserRequest */
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/appointmentTypes?businessId=${testEntities.business.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.maxUserRequestAppointmentType)
        );

        /** Create a resource */
        failIfUnsuccessful(
            await request(testApp)
                .post(`/api/resources?businessId=${testEntities.business.id}`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.resource)
        );

        /** Create future timeframe */
        failIfUnsuccessful(
            await request(testApp)
                .post(`/api/timeframes?resourceId=${testEntities.resource.id}`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send({
                    timeframes: [testEntities.timeframe],
                })
        );

        /** Create today timeframe */
        failIfUnsuccessful(
            await request(testApp)
                .post(`/api/timeframes?resourceId=${testEntities.resource.id}`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send({
                    timeframes: [testEntities.todayTimeframe],
                })
        );

        /** Relate the first appointment type to the resource */
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/resources/relateAppointmentType?id=${testEntities.resource.id}&appointmentTypeId=${testEntities.appointmentType.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
        );

        /** Relate the other appointment type to the resource */
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/resources/relateAppointmentType?id=${testEntities.resource.id}&appointmentTypeId=${testEntities.otherAppointmentType.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
        );

        /** Relate the  appointment type with maxUserRequest properties to the resource */
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/resources/relateAppointmentType?id=${testEntities.resource.id}&appointmentTypeId=${testEntities.maxUserRequestAppointmentType.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
        );

        /** Create an appointment*/
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testEntities.appointmentType.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.appointment)
        );

        /** Send an appointment*/
        failIfUnsuccessful(
            await request(testApp)
                .post(`/api/appointments/send`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send({
                    appointmentFrom: testSentAppointmentFrom,
                    appointmentTypeId: testEntities.appointmentType.id,
                    timeframeId: testEntities.timeframe.id,
                    customerName: testEntities.businessRegisteredUser.name,
                    customerEmail: testEntities.businessRegisteredUser.email,
                    customerPhone: testEntities.businessRegisteredUser.phone,
                })
        );

        /** Create another appointment */
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testEntities.appointmentType.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testOtherToken}`)
                .send(testEntities.otherAppointment)
        );

        /** Create third appointment that needs confirmation */
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testEntities.otherAppointmentType.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.notConfirmedAppointment)
        );

        /** Create an appointment to make a review*/
        failIfUnsuccessful(
            await request(testApp)
                .post(
                    `/api/appointments?timeframeId=${testEntities.todayTimeframe.id}&appointmentTypeId=${testEntities.otherAppointmentType.id}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.reviewMakerAppointment)
        );

        /** Confirm the appointment to make a review*/
        failIfUnsuccessful(
            await request(testApp)
                .put(
                    `/api/appointments/confirmAppointment?id=${reviewMakerAppointmentId}`
                )
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
        );

        /** Create a review */
        failIfUnsuccessful(
            await request(testApp)
                .post(`/api/reviews?businessId=${testEntities.business.id}`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${testToken}`)
                .send(testEntities.review)
        );
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(
            "Error while creating test entities in initTestEntities.ts"
        );
        // eslint-disable-next-line no-console
        console.error(error);
        process.exit(-1);
    }
}

export default initTestEntities;
