// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config();
import cors from "cors";
import "reflect-metadata";
import express from "express";
import swaggerUi from "swagger-ui-express";
import swaggerJsdoc from "swagger-jsdoc";
import swaggerOptions from "swaggerOptions";
import MeRouter from "routers/MeRouter";
import BusinessesRouter from "routers/BusinessesRouter";
import AppointmentsRouter from "routers/AppointmentsRouter";
import AppointmentTypesRouter from "routers/AppointmentTypesRouter";
import TimeframesRouter from "routers/TimeframesRouter";
import ResourcesRouter from "routers/ResourcesRouter";
import ReviewsRouter from "routers/ReviewsRouter";
import UsersRouter from "routers/UsersRouter";
import swaggerUiOptions from "swaggerUiOptions";

const app = express();
app.disable("x-powered-by");

/** Set up middlewares */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.set("view engine", "pug");
app.set("views", `${__dirname}/views`);

/** Define routes */
app.use("/api/users", UsersRouter);
app.use("/api/me", MeRouter);
app.use("/api/businesses", BusinessesRouter);
app.use("/api/appointments", AppointmentsRouter);
app.use("/api/appointmentTypes", AppointmentTypesRouter);
app.use("/api/resources", ResourcesRouter);
app.use("/api/timeframes", TimeframesRouter);
app.use("/api/reviews", ReviewsRouter);

app.use("/healthz", (_, res) => {
    res.status(200).send("OK");
});

/** Docs */
app.use(
    "/api/docs",
    swaggerUi.serve,
    swaggerUi.setup(swaggerJsdoc(swaggerOptions), swaggerUiOptions)
);

const server = app.listen(process.env.PORT, () => {
    // eslint-disable-next-line no-console
    console.info(`Server open on port ${process.env.PORT}`);
});

export { app, server };
