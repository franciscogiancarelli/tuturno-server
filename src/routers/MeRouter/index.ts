import express from "express";
import Login from "./handlers/Login/index";
import GetMyData from "./handlers/GetMyData/index";
import UpdateMyData from "./handlers/UpdateMyData/index";
import Register from "./handlers/Register/index";
import VerifyEmail from "./handlers/VerifyEmail/index";
import RequestPasswordReset from "./handlers/RequestPasswordReset/index";
import ResetPassword from "./handlers/ResetPassword/index";
import { validateGetMyDataRequest } from "./handlers/GetMyData/types";
import { validateUpdateMyDataRequest } from "./handlers/UpdateMyData/types";
import { validateResetPasswordRequest } from "./handlers/ResetPassword/types";
import { validateRequestPasswordResetRequest } from "./handlers/RequestPasswordReset/types";
import { validateRegisterRequest } from "./handlers/Register/types";
import { validateLoginRequest } from "./handlers/Login/types";
import ShowResetPasswordForm from "./handlers/ShowResetPasswordForm";
import { validateShowResetPasswordFormRequest } from "./handlers/ShowResetPasswordForm/types";
import RequestVerificationEmail from "./handlers/RequestVerificationEmail";
import { validateRequestVerificationEmailRequest } from "./handlers/RequestVerificationEmail/types";

const MeRouter = express.Router();

MeRouter.post("/login", validateLoginRequest, Login);

MeRouter.post("/register", validateRegisterRequest, Register);

MeRouter.get("/", validateGetMyDataRequest, GetMyData);

MeRouter.put("/", validateUpdateMyDataRequest, UpdateMyData);

MeRouter.get("/verify-email/:token", VerifyEmail);

MeRouter.get(
    "/request-password-reset/:email",
    validateRequestPasswordResetRequest,
    RequestPasswordReset
);

MeRouter.get(
    "/show-reset-form/:token",
    validateShowResetPasswordFormRequest,
    ShowResetPasswordForm
);

MeRouter.post(
    "/reset-password/:token",
    validateResetPasswordRequest,
    ResetPassword
);

MeRouter.get(
    "/request-verification-email",
    validateRequestVerificationEmailRequest,
    RequestVerificationEmail
);

export default MeRouter;
