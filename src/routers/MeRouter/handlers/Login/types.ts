//INCLUDE LOGIN  TYPES DECLARATION BELOW THIS COMMENT//

import { RequestHandler } from "express";
import { body } from "express-validator";
import { __ } from "locales";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateLoginRequest = [
    body("email").notEmpty().withMessage(__("me.missingEmail")),
    body("email").optional().isEmail().withMessage(__("me.wrongEmailType")),
    body("password").notEmpty().withMessage(__("me.missingPassword")),
    validationHandler,
];

export type LoginBody = {
    email: string;
    password: string;
};

export type LoginHandler = RequestHandler<
    unknown,
    TTResponse<{ token: string }> | TTError,
    LoginBody,
    unknown
>;

/**
 * @openapi
 *  /api/me/login:
 *   post:
 *    summary: Login
 *    tags:
 *     - Me
 *    description: >
 *     This endpoint will allow you to login on the platform.
 *     It will return a token for you to use on future communication.
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         password:
 *          type: string
 *          nullable: false
 *          example: test1234
 *         email:
 *          type: string
 *          nullable: false
 *          example: test@email.com
 *    responses:
 *     200:
 *      description: You were logged in.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          data:
 *           type: object
 *           properties:
 *            token:
 *             type: string
 *             example: "23fknwefdkjh2j1q3wkrjouhwrofguhabenihlbwfasjernwljaenjtrn342q3erwefdsf"
 */
