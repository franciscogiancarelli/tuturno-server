import initTestEntities, {
    testApp,
    testEntities,
    testServer,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Me Router works well", () => {
    test("can't login user without email or password", async () => {
        return request(testApp)
            .post("/api/me/login")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("me.missingEmail"));
                expect(res.body.errors[1].msg).toMatch(
                    __("me.missingPassword")
                );
            });
    });

    test("can't login user if email has not email format", async () => {
        return request(testApp)
            .post("/api/me/login")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                email: "notEmailFormat",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("me.wrongEmailType"));
            });
    });

    test("can't login user if email not found", async () => {
        return request(testApp)
            .post("/api/me/login")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                email: "emailnoregistrado@gmail.com",
                password: `${testEntities.user.password}_wrong`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("me.incorrectCredentials"));
            });
    });

    test("can't login user with wrong password", async () => {
        return request(testApp)
            .post("/api/me/login")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                email: testEntities.user.email,
                password: `${testEntities.user.password}_wrong`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body.message).toMatch(__("me.incorrectCredentials"));
            });
    });

    test("can login user", async () => {
        return request(testApp)
            .post("/api/me/login")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                email: testEntities.user.email,
                password: testEntities.user.password,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toHaveProperty("token");
            });
    });

    test("can't login with user without a password", async () => {
        return request(testApp)
            .post("/api/me/login")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                email: testEntities.businessRegisteredUser.email,
                password: testEntities.businessRegisteredUser.password,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(403);
                expect(res.body.message).toEqual(
                    __("me.accountNotFullyRegistered")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
