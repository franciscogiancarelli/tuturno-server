import User from "entities/User";
import bcrypt from "bcryptjs";
import { getRepository } from "typeorm";
import jwt from "jsonwebtoken";
import TTResponse from "typedef/TTResponse";
import TTError from "typedef/TTError";
import { __ } from "locales";
import { LoginHandler } from "./types";
import sendPasswordRegistrationEmail from "helpers/sendPasswordRegistrationEmail";

const UsersRepository = getRepository(User);

const Login: LoginHandler = async (req, res) => {
    try {
        const user = await UsersRepository.createQueryBuilder("user")
            .addSelect("user.password")
            .where("user.email = :email", { email: req.body.email })
            .getOne();
        if (!user) throw __("me.incorrectCredentials");
        if (!user.password) {
            sendPasswordRegistrationEmail(user);
            throw new TTError({
                code: "TTS-ME-RE-ANFR",
                message: __("me.accountNotFullyRegistered"),
                status: 403,
            });
        }

        const passwordIsCorrect = await bcrypt.compare(
            req.body.password,
            user.password
        );

        if (!passwordIsCorrect) throw __("me.incorrectCredentials");
        const token = jwt.sign({ uid: user.uid }, process.env.SECRET as string);
        res.send(new TTResponse<{ token: string }>({ token }));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default Login;
