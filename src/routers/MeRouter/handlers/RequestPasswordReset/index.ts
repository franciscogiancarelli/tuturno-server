import User from "entities/User";
import { getRepository } from "typeorm";
import jwt from "jsonwebtoken";
import TTResponse from "typedef/TTResponse";
import TTError from "typedef/TTError";
import sgMail from "@sendgrid/mail";
import path from "path";
import pug from "pug";
import { RequestPasswordResetHandler } from "./types";
import { __ } from "locales";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);
const UsersRepository = getRepository(User);

const RequestPasswordReset: RequestPasswordResetHandler = async (req, res) => {
    try {
        const user = await UsersRepository.createQueryBuilder("user")
            .where("user.email = :email", { email: req.params.email })
            .getOne();
        if (user) {
            const token = jwt.sign(
                { email: user.email },
                process.env.SECRET as string
            );
            const verificationLink = `${process.env.HOST}/api/me/show-reset-form/${token}`;
            const html = pug.renderFile(
                path.join(
                    __dirname,
                    "../../../../views/passwordResetRequest.pug"
                ),
                { link: verificationLink, name: user.name }
            );
            await sgMail.send({
                to: user.email,
                from: {
                    name: "Tuturno",
                    email: "noreply@tuturno.com.ar",
                },
                subject: "Recuperá tu contraseña",
                text: `Recibiste este correo porque nos llegó una solicitud para restaurar tu contraseña. Si fuiste vos, podés ingresar en el siguiente link para hacerlo: ${verificationLink}`,
                html: html,
                mailSettings: {
                    sandboxMode: {
                        enable: process.env.JEST_WORKER_ID !== undefined,
                    },
                },
            });
        }
        res.send(new TTResponse(__("me.recoveryEmailSent")));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default RequestPasswordReset;
