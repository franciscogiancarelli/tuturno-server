import { RequestHandler } from "express";
import { param } from "express-validator";
import { __ } from "locales";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateRequestPasswordResetRequest = [
    param("email").notEmpty().isEmail().withMessage(__("me.wrongEmailType")),
    validationHandler,
];

export type RequestPasswordResetParams = {
    email: string;
};

export type RequestPasswordResetHandler = RequestHandler<
    RequestPasswordResetParams,
    TTResponse<string> | TTError,
    unknown,
    unknown
>;

/**
 * @openapi
 *  /api/me/request-password-reset/{email}:
 *   get:
 *    summary: Request password reset.
 *    tags:
 *     - Me
 *    description: >
 *      This endpoint allows to request a user's password reset.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: email
 *       in: path
 *       required: true
 *       description: The email of the user who is requesting the password reset.
 *       schema:
 *        type: string
 *    responses:
 *     200:
 *      description: The recovery email was sent succesfully.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          message:
 *           type: string
 *           example: Recovery email sent..
 */
