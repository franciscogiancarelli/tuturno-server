import initTestEntities, {
    testApp,
    testEntities,
    testServer,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Password reset request works well", () => {
    test("can request a password reset", () => {
        return request(testApp)
            .get(`/api/me/request-password-reset/${testEntities.user.email}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("me.recoveryEmailSent"));
            });
    });

    test("won't say anything if the email is not found", () => {
        return request(testApp)
            .get(`/api/me/request-password-reset/asdfasdfasf@asdfa.com`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("me.recoveryEmailSent"));
            });
    });

    test("can't request a password if  mail has wrong type", () => {
        return request(testApp)
            .get(`/api/me/request-password-reset/asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("me.wrongEmailType"));
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
