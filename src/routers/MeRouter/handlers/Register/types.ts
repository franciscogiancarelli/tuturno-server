//INCLUDE REGISTER TYPES DECLARATION BELOW THIS COMMENT//

import { RequestHandler } from "express";
import { body } from "express-validator";
import { __ } from "locales";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateRegisterRequest = [
    body("name").notEmpty().withMessage(__("me.missingName")),
    body("email")
        .notEmpty()
        .withMessage(__("me.missingEmail"))
        .bail()
        .isEmail()
        .withMessage(__("me.wrongEmailType")),
    body("phone")
        .notEmpty()
        .withMessage(__("me.missingPhone"))
        .bail()
        .isMobilePhone("es-AR")
        .withMessage(__("general.wrongPhone")),
    body("password")
        .notEmpty()
        .withMessage(__("me.missingPassword"))
        .bail()
        .isStrongPassword({
            minLength: 8,
            minLowercase: 0,
            minUppercase: 0,
            minNumbers: 1,
            minSymbols: 0,
        })
        .withMessage(__("me.notStrongPassword")),
    validationHandler,
];

export type RegisterBody = {
    name: string;
    email: string;
    phone: string;
    password: string;
};

export type RegisterHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    RegisterBody,
    unknown
>;

/**
 * @openapi
 *  /api/me/register:
 *   post:
 *    summary: Register
 *    tags:
 *     - Me
 *    description: >
 *     This endpoint will allow you to register on the platform.
 *     It will return some of the data we registered you with. Remember phone must have country area (Argentina 549) and zone area.
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         name:
 *          type: string
 *          nullable: false
 *          example: Francisco Giancarelli
 *         password:
 *          type: string
 *          nullable: false
 *          example: test1234
 *         email:
 *          type: string
 *          nullable: false
 *          example: test@email.com
 *         phone:
 *          type: string
 *          nullable: false
 *          example: +5493424235214
 *    responses:
 *     200:
 *      description: You were registered.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          data:
 *           $ref: '#/components/schemas/User'
 */
