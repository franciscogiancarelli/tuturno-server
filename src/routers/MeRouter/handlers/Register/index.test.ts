import faker from "faker";
import initTestEntities, {
    fakerPhoneNumberFormat,
    testApp,
    testEntities,
    testServer,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { mockedSend } from "testSetup";
import { getConnection } from "typeorm";

const testUserName = faker.name.findName();
const testArgentinaUserPhone = faker.phone.phoneNumber(fakerPhoneNumberFormat);
const testArgentinaNotValidUserPhone = faker.phone.phoneNumber("123###");
const testUserEmail = faker.internet.email();
const testUserPassword = faker.internet.password() + "1";

beforeAll(() => {
    return initTestEntities();
});

describe("Me Router works well", () => {
    beforeEach(() => {
        mockedSend.mockReset();
    });
    test("can't register user without name, email, phone, or password", () => {
        return request(testApp)
            .post("/api/me/register")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({})
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("me.missingName"));
                expect(res.body.errors[1].msg).toMatch(__("me.missingEmail"));
                expect(res.body.errors[2].msg).toMatch(__("me.missingPhone"));
                expect(res.body.errors[3].msg).toMatch(
                    __("me.missingPassword")
                );
            });
    });

    test("can't register if email or phone doesn't have correct  format", () => {
        return request(testApp)
            .post("/api/me/register")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                phone: testArgentinaNotValidUserPhone,
                name: testUserName,
                email: "notEmailFormat",
                password: testUserPassword,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("me.wrongEmailType"));
                expect(res.body.errors[1].msg).toMatch(
                    __("general.wrongPhone")
                );
            });
    });

    test("can register user", () => {
        return request(testApp)
            .post("/api/me/register")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                phone: testArgentinaUserPhone,
                name: testUserName,
                email: testUserEmail,
                password: testUserPassword,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("me.userRegistered"));
            });
    });

    test("can't register two users with the same email", () => {
        return request(testApp)
            .post("/api/me/register")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                phone: testArgentinaUserPhone,
                name: testUserName,
                email: testEntities.user.email,
                password: "asdfasdfasdf23r!",
            })
            .then((res) => {
                expect(mockedSend).not.toBeCalled();
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("me.emailTaken"));
            });
    });

    test("trying to register a user with an unverified email triggers a verification email.", () => {
        return request(testApp)
            .post("/api/me/register")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                phone: testArgentinaUserPhone,
                name: testUserName,
                email: testEntities.unverifiedUser.email,
                password: "asdfasdfasdf23r!",
            })
            .then((res) => {
                expect(mockedSend).toBeCalledWith(
                    expect.objectContaining({
                        to: testEntities.unverifiedUser.email,
                    })
                );
                expect(res.statusCode).toEqual(403);
                expect(res.body.message).toMatch(__("me.emailNotVerified"));
            });
    });

    test("trying to register a user previously registered by a business triggers a verification email..", () => {
        return request(testApp)
            .post("/api/me/register")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                phone: testArgentinaUserPhone,
                name: testUserName,
                email: testEntities.businessRegisteredUser.email,
                password: "asdfasdfasdf23r!",
            })
            .then((res) => {
                expect(mockedSend).toBeCalledWith(
                    expect.objectContaining({
                        to: testEntities.businessRegisteredUser.email,
                    })
                );
                expect(res.statusCode).toEqual(403);
                expect(res.body.message).toMatch(
                    __("me.accountNotFullyRegistered")
                );
            });
    });

    test("can't register a user if password is not strong enought.", () => {
        return request(testApp)
            .post("/api/me/register")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                phone: testArgentinaUserPhone,
                name: testUserName,
                email: testUserEmail,
                password: "asd",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("me.notStrongPassword")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
