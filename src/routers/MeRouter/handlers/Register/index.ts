import User from "entities/User";
import bcrypt from "bcryptjs";
import { getRepository } from "typeorm";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { __ } from "locales";
import { RegisterHandler } from "./types";
import sendPasswordRegistrationEmail from "helpers/sendPasswordRegistrationEmail";
import sendAccountVerificationEmail from "helpers/sendAccountVerificationEmail";

const UsersRepository = getRepository(User);

const Register: RegisterHandler = async (req, res) => {
    try {
        const userData = req.body;
        userData.password = await bcrypt.hash(userData.password, 10);
        const user = await UsersRepository.createQueryBuilder("user")
            .addSelect("user.password")
            .where("user.email = :email", { email: userData.email })
            .getOne();

        if (!user) {
            const newUser = await UsersRepository.insert(userData);
            if (!newUser) throw __("me.registerFail");
            await sendAccountVerificationEmail(userData);
            return res
                .status(200)
                .send(new TTResponse<string>(__("me.userRegistered")));
        } else {
            if (user.emailConfirmed) throw new TTError(__("me.emailTaken"));
            if (user.password && !user.emailConfirmed) {
                await sendAccountVerificationEmail(user);
                throw new TTError({
                    code: "TTS-ME-RE-ENV",
                    message: __("me.emailNotVerified"),
                    status: 403,
                });
            }
            if (!user.password) {
                await sendPasswordRegistrationEmail(user);
                throw new TTError({
                    code: "TTS-ME-RE-ANFR",
                    message: __("me.accountNotFullyRegistered"),
                    status: 403,
                });
            }
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default Register;
