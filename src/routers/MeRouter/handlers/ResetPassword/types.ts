import { RequestHandler } from "express";

export const validateResetPasswordRequest = [];

export type ResetPasswordBody = {
    password: string;
};

export type ResetPasswordHandler = RequestHandler<
    { token: string },
    string,
    ResetPasswordBody,
    unknown
>;

/**
 * @openapi
 *  /api/me/reset-password/{token}:
 *   post:
 *    summary: Reset password.
 *    tags:
 *     - Me
 *    description: >
 *      This endpoint allows to reset a user's password.
 *    parameters:
 *     - name: token
 *       in: path
 *       required: true
 *       description: The token to allow the password reset
 *       schema:
 *        type: string
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         password:
 *          type: string
 *          nullable: false
 *          example: test1234
 *    responses:
 *     200:
 *      description: User password updated.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          message:
 *           type: string
 *           example: User password updated..
 */
