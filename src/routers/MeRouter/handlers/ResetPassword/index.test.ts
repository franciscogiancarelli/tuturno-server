import initTestEntities, {
    testApp,
    testEntities,
    testServer,
} from "initTestEntities";
import request from "supertest";
import { getConnection } from "typeorm";
import jwt from "jsonwebtoken";
import { stringedSuccessfulPasswordReset } from "views/SuccessfulPasswordReset";
import { stringedUnsuccessfulPasswordReset } from "views/UnsuccessfulPasswordReset";

const wrongToken =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5hY2hvbHVwb3R0aUBnbWFpbC5jb20iLCJpYXQiOjE2NDA2NDUzOTh9.dkO5hjkQvTFYyMD_grVSkjyM8SKYsMC3rPmAmuAOAIo";

beforeAll(() => {
    return initTestEntities();
});

describe("Password reset works well", () => {
    test("can reset a password", () => {
        const token = jwt.sign(
            { email: testEntities.user.email },
            process.env.SECRET as string
        );
        return request(testApp)
            .post(`/api/me/reset-password/${token}`)
            .send({
                password: "new-password",
            })
            .set("Content-Type", "application/x-www-form-urlencoded")
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.text).toEqual(stringedSuccessfulPasswordReset);
            });
    });

    test("can't reset password with a wrong token", () => {
        const token = jwt.sign(wrongToken, process.env.SECRET as string);
        return request(testApp)
            .post(`/api/me/reset-password/${token}`)
            .send({
                password: "new-password",
            })
            .set("Content-Type", "application/x-www-form-urlencoded")
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.text).toEqual(stringedUnsuccessfulPasswordReset);
            });
    });

    test("can't reset if token is not JWT type or password is missing", () => {
        return request(testApp)
            .post(`/api/me/reset-password/this_token_is_not_JTW_type`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                password: "password",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.text).toEqual(stringedUnsuccessfulPasswordReset);
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
