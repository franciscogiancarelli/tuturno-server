import User from "entities/User";
import bcrypt from "bcryptjs";
import { getRepository } from "typeorm";
import TTError from "typedef/TTError";
import jwt from "jsonwebtoken";
import { ResetPasswordHandler } from "./types";
import { __ } from "locales";
import { stringedSuccessfulPasswordReset } from "views/SuccessfulPasswordReset";
import { stringedUnsuccessfulPasswordReset } from "views/UnsuccessfulPasswordReset";

const UsersRepository = getRepository(User);

const ResetPassword: ResetPasswordHandler = async (req, res) => {
    try {
        const password = await bcrypt.hash(req.body.password, 10);
        const response = jwt.verify(
            req.params.token,
            process.env.SECRET as string
        );
        if (typeof response === "string" || !response.email)
            throw __("me.wrongToken");
        await UsersRepository.update(
            { email: response.email },
            { password, emailConfirmed: true }
        );
        res.status(200).send(stringedSuccessfulPasswordReset);
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(stringedUnsuccessfulPasswordReset);
    }
};

export default ResetPassword;
