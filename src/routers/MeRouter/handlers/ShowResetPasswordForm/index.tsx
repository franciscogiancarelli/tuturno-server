import { renderToString } from "react-dom/server";
import ResetPasswordForm from "views/ResetPasswordForm";
import { ResetPasswordFormHandler } from "./types";

const ShowResetPasswordForm: ResetPasswordFormHandler = async (req, res) => {
    res.status(200).send(
        renderToString(<ResetPasswordForm token={req.params.token} />)
    );
};

export default ShowResetPasswordForm;
