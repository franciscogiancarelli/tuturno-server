import { RequestHandler } from "express";

export const validateShowResetPasswordFormRequest = [];

export type ResetPasswordFormHandler = RequestHandler<
    { token: string },
    string,
    null,
    unknown
>;

/**
 * @openapi
 *  /api/me/reset-password-form/{token}:
 *   get:
 *    summary: Show reset password form.
 *    tags:
 *     - Me
 *    parameters:
 *     - name: token
 *       in: path
 *       required: true
 *       description: Token to allow the password reset
 *       schema:
 *        type: string
 *    description: >
 *      This endpoint allows to send a new password for your account.
 */
