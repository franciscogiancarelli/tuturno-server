import initTestEntities, { testApp, testServer } from "initTestEntities";
import { renderToString } from "react-dom/server";
import request from "supertest";
import { getConnection } from "typeorm";
import ResetPasswordForm from "views/ResetPasswordForm";

beforeAll(() => {
    return initTestEntities();
});

describe("ShowResetPasswordForm", () => {
    it("shows reset password form", () => {
        return request(testApp)
            .get(`/api/me/show-reset-form/token`)
            .then((res) => {
                expect(res.text).toEqual(
                    renderToString(<ResetPasswordForm token={"token"} />)
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
