//INCLUDE GET MY DATA TYPES DECLARATION BELOW THIS COMMENT//

import User from "entities/User";
import { RequestHandler } from "express";
import authenticate from "middlewares/authenticate";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateGetMyDataRequest = [authenticate];

export type GetMyDataHandler = RequestHandler<
    unknown,
    TTResponse<User> | TTError,
    unknown,
    unknown
>;

/**
 * @openapi
 *  /api/me/:
 *   get:
 *    summary: Get My Data
 *    tags:
 *     - Me
 *    description: >
 *     Using this endpoint you can get all the data we have of you.
 *    security:
 *     - BearerAuth: []
 *    responses:
 *     200:
 *      description: Your data was returned.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          data:
 *           $ref: '#/components/schemas/User'
 */
