import User from "entities/User";
import { getRepository } from "typeorm";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { __ } from "locales";
import { GetMyDataHandler } from "./types";

const UsersRepository = getRepository(User);

const GetMyData: GetMyDataHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const user = await UsersRepository.createQueryBuilder("user")
            .where("user.uid = :uid", auth)
            .getOne();
        if (!user) throw __("me.userNotFound");

        res.send(new TTResponse<User>(user));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default GetMyData;
