import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Me Router works well", () => {
    test("can get my data", () => {
        return request(testApp)
            .get("/api/me")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data.name).toEqual(testEntities.user.name);
                expect(res.body.data.email).toEqual(testEntities.user.email);
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
