import initTestEntities, {
    testApp,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("RequestVerificationEmail", () => {
    test("request verification email", async () => {
        return request(testApp)
            .get(`/api/me/request-verification-email`)
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.message).toMatch(
                    __("me.verificationEmailSent")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
