import User from "entities/User";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { RequestVerificationEmailHandler } from "./types";
import sendAccountVerificationEmail from "helpers/sendAccountVerificationEmail";

const UserRepository = getRepository(User);

const RequestVerificationEmail: RequestVerificationEmailHandler = async (
    req,
    res
) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const user = await UserRepository.findOneOrFail({ uid: auth.uid });
        await sendAccountVerificationEmail(user);
        return res
            .status(200)
            .send(new TTResponse<string>(__("me.verificationEmailSent")));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default RequestVerificationEmail;
