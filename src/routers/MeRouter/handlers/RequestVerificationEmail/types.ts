import { RequestHandler } from "express";
import authenticate from "middlewares/authenticate";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateRequestVerificationEmailRequest = [
    authenticate,
    validationHandler,
];

export type RequestVerificationEmailParams = {
    email: string;
};

export type RequestVerificationEmailHandler = RequestHandler<
    RequestVerificationEmailParams,
    TTResponse<string> | TTError,
    unknown,
    unknown
>;

/**
 * @openapi
 *  /api/me/request-verification-email:
 *   get:
 *    summary: Request verification email.
 *    tags:
 *     - Me
 *    description: >
 *      This endpoint allows to request a user's verification email.
 *    security:
 *     - BearerAuth: []
 *    responses:
 *     200:
 *      description: The recovery email was sent succesfully.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          message:
 *           type: string
 *           example: Verification email sent..
 */
