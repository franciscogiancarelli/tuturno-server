//INCLUDE VERIFY EMAIL TYPES DECLARATION BELOW THIS COMMENT//

/**
 * @openapi
 *  /api/me/verify-email/{token}:
 *   get:
 *    summary: Verify Email
 *    tags:
 *     - Me
 *    description: >
 *      This endpoint allows to verify a user's email.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: token
 *       in: path
 *       required: true
 *       description: The token that is given by email, after the Register request.
 *       schema:
 *        type: string
 *    responses:
 *     200:
 *      description: The email was verified.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          message:
 *           type: string
 *           example: Email verified succesfully.
 */
