import User from "entities/User";
import { RequestHandler } from "express";
import { getRepository } from "typeorm";
import jwt from "jsonwebtoken";
import { stringedEmailVerificationSuccess } from "views/EmailVerificationSuccess";
import TTError from "typedef/TTError";
import { stringedEmailVerificationError } from "views/EmailVerificationError";

const UsersRepository = getRepository(User);

const VerifyEmail: RequestHandler = async (req, res) => {
    try {
        const response = jwt.verify(
            req.params.token,
            process.env.SECRET as string
        );
        if (typeof response === "string" || !response.email)
            throw new TTError("Wrong token.");
        await UsersRepository.update(
            { email: response.email },
            { emailConfirmed: true }
        );
        res.send(stringedEmailVerificationSuccess);
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(stringedEmailVerificationError);
    }
};

export default VerifyEmail;
