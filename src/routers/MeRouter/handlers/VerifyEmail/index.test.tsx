import initTestEntities, {
    testApp,
    testEntities,
    testServer,
} from "initTestEntities";
import request from "supertest";
import { getConnection } from "typeorm";
import jwt from "jsonwebtoken";
import { renderToString } from "react-dom/server";
import EmailVerificationSuccess from "views/EmailVerificationSuccess";
import { stringedEmailVerificationError } from "views/EmailVerificationError";

beforeAll(() => {
    return initTestEntities();
});

describe("Email verification works as expected", () => {
    test("can verify an email", () => {
        const token = jwt.sign(
            { email: testEntities.user.email },
            process.env.SECRET as string
        );
        return request(testApp)
            .get(`/api/me/verify-email/${token}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.text).toEqual(
                    renderToString(<EmailVerificationSuccess />)
                );
            });
    });

    test("can't verify email with a wrong token", () => {
        const token = jwt.sign(
            "this_token_is_wrong",
            process.env.SECRET as string
        );
        return request(testApp)
            .get(`/api/me/verify-email/${token}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.text).toEqual(stringedEmailVerificationError);
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
