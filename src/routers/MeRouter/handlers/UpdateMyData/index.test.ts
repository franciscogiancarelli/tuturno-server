import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Me Router works well", () => {
    test("can update my name", () => {
        return request(testApp)
            .put("/api/me")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: `${testEntities.user}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("me.dataUpdated"));
            });
    });

    test("can update my password", () => {
        return request(testApp)
            .put("/api/me")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                password: "asdfasdfasdf23r!",
                currentPassword: testEntities.user.password,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("me.dataUpdated"));
            });
    });

    test("can't update my user id or mail", () => {
        return request(testApp)
            .put("/api/me")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                uid: "asdfdasfasdf",
                email: `$test@gmail.com`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("me.cantChangeId"));
                expect(res.body.errors[1].msg).toMatch(
                    __("me.cantChangeEmail")
                );
            });
    });

    test("can't update password if currentPassword not sent", () => {
        return request(testApp)
            .put("/api/me")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                password: "asdfasdfasdf23r!",
                currentPassword: testEntities.user.password,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("me.wrongPassword"));
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
