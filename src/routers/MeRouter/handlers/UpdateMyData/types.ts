import { RequestHandler } from "express";
import { body } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateUpdateMyDataRequest = [
    authenticate,
    requireVerifiedEmail,
    body("uid").isEmpty().withMessage(__("me.cantChangeId")),
    body("email").isEmpty().withMessage(__("me.cantChangeEmail")),
    body("password")
        .custom((password, { req }) =>
            password ? req.body.currentPassword : true
        )
        .withMessage(__("me.cantChangePasswordWithoutCurrentPassword")),
    validationHandler,
];

export type UpdateMyDataBody = {
    name?: string;
    password?: string;
    currentPassword?: string;
    phone?: string;
};

export type UpdateMyDataHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    UpdateMyDataBody,
    unknown
>;

/**
 * @openapi
 *  /api/me/:
 *   put:
 *    summary: Update My Data
 *    tags:
 *     - Me
 *    description: >
 *     This endpoint allows you to update your data. You can
 *     also use this endpoint to update your password.
 *    security:
 *     - BearerAuth: []
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         name:
 *          type: string
 *          nullable: true
 *          example: Francisco Giancarelli
 *         currentPassword:
 *          type: string
 *          nullabel: true
 *          example: test1234
 *         password:
 *          type: string
 *          nullable: true
 *          example: test1233
 *    responses:
 *     200:
 *      description: Your data was updated.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          message:
 *           type: string
 *           example: User data updated.
 */
