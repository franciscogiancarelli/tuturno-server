import User from "entities/User";
import bcrypt from "bcryptjs";
import { getRepository } from "typeorm";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { __ } from "locales";
import { UpdateMyDataHandler } from "./types";

const UsersRepository = getRepository(User);

const UpdateMyData: UpdateMyDataHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const newUserData = req.body;
        if (newUserData.password) {
            const currentUser = await UsersRepository.createQueryBuilder("user")
                .addSelect("user.password")
                .andWhere("user.uid = :uid", {
                    uid: auth.uid,
                })
                .getOne();
            if (!currentUser) throw new TTError(__("me.userNotFound"));
            const passwordIsCorrect = await bcrypt.compare(
                req.body.currentPassword as string,
                currentUser.password as string
            );
            if (!passwordIsCorrect)
                throw new TTError(__("me.wrongPassword"), 400);
            newUserData.password = await bcrypt.hash(newUserData.password, 10);
            delete newUserData.currentPassword;
        }
        await UsersRepository.update({ uid: auth.uid }, newUserData);
        res.status(200).send(new TTResponse(__("me.dataUpdated")));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default UpdateMyData;
