import express from "express";
import GetAppointmentTypes from "./handlers/GetAppointmentTypes/index";
import DeleteAppointmentType from "./handlers/DeleteAppointmentType/index";
import AddAppointmentType from "./handlers/AddAppointmentType/index";
import UpdateAppointmentType from "./handlers/UpdateAppointmentType/index";
import { validateGetAppointmentTypeRequest } from "./handlers/GetAppointmentTypes/types";
import { validateDeleteAppointmenTypeRequest } from "./handlers/DeleteAppointmentType/types";
import { validateUpdateAppointmentTypeRequest } from "./handlers/UpdateAppointmentType/types";
import { validateAddAppointmentTypeRequest } from "./handlers/AddAppointmentType/types";

const AppointmentTypesRouter = express.Router({ mergeParams: true });

AppointmentTypesRouter.get(
    "/",
    validateGetAppointmentTypeRequest,
    GetAppointmentTypes
);

AppointmentTypesRouter.post(
    "/",
    validateAddAppointmentTypeRequest,
    AddAppointmentType
);

AppointmentTypesRouter.delete(
    "/",
    validateDeleteAppointmenTypeRequest,
    DeleteAppointmentType
);

AppointmentTypesRouter.put(
    "/",
    validateUpdateAppointmentTypeRequest,
    UpdateAppointmentType
);

export default AppointmentTypesRouter;
