//INCLUDE UPDATE APPOINTMENT TYPE DECLARATION BELOW THIS COMMENT//

import { Currency } from "entities/AppointmentType";
import { RequestHandler } from "express";
import { body, oneOf, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateUpdateAppointmentTypeRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").isUUID().withMessage(__("general.invalidId")),
    query("id").notEmpty().withMessage(__("general.missingId")),
    body("businessId").isEmpty().withMessage(__("general.cantChangeId")),
    body("id").isEmpty().withMessage(__("general.cantChangeId")),
    body("name")
        .custom((name, { req }) => {
            if (
                !name &&
                !req.body.description &&
                !req.body.price &&
                !req.body.duration &&
                !req.body.currency &&
                !req.body.needConfirmation
            ) {
                return false;
            }
            return true;
        })
        .withMessage(__("appointmentTypes.mustProvideData")),
    body("price")
        .optional()
        .isCurrency({ allow_negatives: false })
        .withMessage(__("appointmentTypes.wrongPriceType")),
    body("duration")
        .optional()
        .isInt({ gt: 0 })
        .isDivisibleBy(5)
        .withMessage(__("appointmentTypes.wrongDurationType")),
    oneOf(
        [
            body("currency").optional().equals(Currency.ARS),
            body("currency").optional().equals(Currency.EUR),
            body("currency").optional().equals(Currency.USD),
        ],
        __("appointmentTypes.wrongCurrencyType")
    ),
    body("maxDailyUserRequest")
        .optional()
        .isInt({ gt: 0 })
        .withMessage(__("appointmentTypes.wrongMaxUserRequestType")),
    body("needConfirmation")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    body("maxWeeklyUserRequest")
        .optional()
        .isInt({ gt: 0 })
        .withMessage(__("appointmentTypes.wrongMaxUserRequestType")),
    body("minimumTimeInAdvance")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("appointmentTypes.wrongMinimumTimeInAdvance")),
    body("maxMonthlyUserRequest")
        .optional()
        .isInt({ gt: 0 })
        .withMessage(__("appointmentTypes.wrongMaxUserRequestType")),
    validationHandler,
];

export type UpdateAppointmentTypeQuery = {
    id: string;
};

export type UpdateAppointmentTypeBody = {
    name?: string;
    description?: string;
    duration?: number;
    price?: string;
    currency?: Currency;
    needConfirmation?: boolean;
    maxDailyUserRequest?: number;
    maxWeeklyUserRequest?: number;
    maxMonthlyUserRequest?: number;
    minimumTimeInAdvance?: number;
};

export type UpdateAppointmentTypeHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    UpdateAppointmentTypeBody,
    UpdateAppointmentTypeQuery
>;

/**
 * @openapi
 *  /api/appointmentTypes/:
 *   put:
 *    summary: Update an appointment type
 *    tags:
 *     - AppointmentTypes
 *    description: >
 *      This endpoint allows you to update an appointmentType.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *       description: The id of the appointment type you want to update.
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         name:
 *          type: string
 *          nullable: true
 *         description:
 *          type: string
 *          nullable: true
 *         duration:
 *          type: number
 *          nullable: true
 *         price:
 *          type: number
 *          nullable: true
 *         currency:
 *          type: enum
 *          nullable: true
 *          example: ARS
 *         needConfirmation:
 *          type: boolean
 *          nullable: true
 *          example: 1
 *         maxDailyUserRequest:
 *          type: integer
 *          nullable: true
 *         maxWeeklyUserRequest:
 *          type: integer
 *          nullable: true
 *         maxMonthlyUserRequest:
 *          type: integer
 *          nullable: true
 *         minimumTimeInAdvance:
 *          type: integer
 *          nullable: true
 *    responses:
 *     200:
 *      description: The appointment type was updated.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           $ref: '#/components/schemas/AppointmentType'
 */
