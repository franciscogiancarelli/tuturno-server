import initTestEntities, {
    testApp,
    testToken,
    testServer,
    testOtherToken,
    testEntities,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("AppointmentTypes Router works well", () => {
    test("can update an appointmentType", async () => {
        return request(testApp)
            .put(`/api/appointmentTypes?id=${testEntities.appointmentType.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: `${testEntities.appointmentType.name}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body.message).toMatch(
                    __("appointmentTypes.appointmentTypeUpdated")
                );
            });
    });

    test("can't update an appointmentType that is not mine", async () => {
        return request(testApp)
            .put(`/api/appointmentTypes?id=${testEntities.appointmentType.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                name: `${testEntities.appointmentType.name}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body.message).toMatch(
                    __("appointmentTypes.cantUpdateAppointmentType")
                );
            });
    });

    test("can't update an appointmentType if body is empty", async () => {
        return request(testApp)
            .put(`/api/appointmentTypes?id=${testEntities.appointmentType.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({})
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("appointmentTypes.mustProvideData")
                );
            });
    });

    test("can't update an appointmentType if no id is given", async () => {
        return request(testApp)
            .put(`/api/appointmentTypes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: `${testEntities.appointmentType.name}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({
                        msg: __("general.missingId"),
                    })
                );
            });
    });

    test("can't update an appointmentType if id is not UUID type", async () => {
        return request(testApp)
            .put(`/api/appointmentTypes?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: `${testEntities.appointmentType.name}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({ msg: __("general.invalidId") })
                );
            });
    });

    test("can't update if id parameter is passed on the body", async () => {
        return request(testApp)
            .put(`/api/appointmentTypes?id=${testEntities.appointmentType.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: `${testEntities.appointmentType.id}`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({
                        msg: __("general.cantChangeId"),
                    })
                );
            });
    });

    test("can't update if type of body parameters are wrong", async () => {
        return request(testApp)
            .put(`/api/appointmentTypes?id=${testEntities.appointmentType.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                duration: 23,
                price: -14,
                currency: "ASD",
                needConfirmation: "asd",
                maxDailyUserRequest: "asd",
                maxWeeklyUserRequest: "asd",
                maxMonthlyUserRequest: "asd",
                minimumTimeInAdvance: -1,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({
                        msg: __("appointmentTypes.wrongDurationType"),
                    })
                );
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({
                        msg: __("appointmentTypes.wrongPriceType"),
                    })
                );
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({
                        msg: __("appointmentTypes.wrongCurrencyType"),
                    })
                );
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({
                        msg: __("general.wrongState"),
                    })
                );
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({
                        msg: __("appointmentTypes.wrongMaxUserRequestType"),
                    })
                );
                expect(res.body.errors).toContainEqual(
                    expect.objectContaining({
                        msg: __("appointmentTypes.wrongMinimumTimeInAdvance"),
                    })
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
