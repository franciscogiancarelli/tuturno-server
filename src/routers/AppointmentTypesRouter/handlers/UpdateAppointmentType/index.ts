import { UpdateAppointmentTypeHandler } from "./types";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import AppointmentType from "entities/AppointmentType";
import Auth from "typedef/Auth";
import { __ } from "locales";

const AppointmentTypeRepository = getRepository(AppointmentType);

const UpdateAppointmentType: UpdateAppointmentTypeHandler = async (
    req,
    res
) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);

        /** Find the appointment type */
        const appointmentType =
            await AppointmentTypeRepository.createQueryBuilder(
                "appointment_type"
            )
                .leftJoin("appointment_type.business", "business")
                .leftJoinAndSelect("business.owner", "owner")
                .where("owner.uid = :uid", auth)
                .andWhere("appointment_type.id = :id", { id: req.query.id })
                .getOne();

        if (!appointmentType) {
            throw __("appointmentTypes.cantUpdateAppointmentType");
        } else {
            appointmentType.name = req.body.name ?? appointmentType.name;
            appointmentType.description =
                req.body.description ?? appointmentType.description;
            appointmentType.price = req.body.price ?? appointmentType.price;
            appointmentType.duration =
                req.body.duration ?? appointmentType.duration;
            appointmentType.needConfirmation =
                req.body.needConfirmation ?? appointmentType.needConfirmation;
            appointmentType.maxDailyUserRequest =
                req.body.maxDailyUserRequest ??
                appointmentType.maxDailyUserRequest;
            appointmentType.maxWeeklyUserRequest =
                req.body.maxWeeklyUserRequest ??
                appointmentType.maxWeeklyUserRequest;
            appointmentType.maxMonthlyUserRequest =
                req.body.maxMonthlyUserRequest ??
                appointmentType.maxMonthlyUserRequest;
            appointmentType.minimumTimeInAdvance =
                req.body.minimumTimeInAdvance ??
                appointmentType.minimumTimeInAdvance;
        }

        /** Update the appointment type */
        await AppointmentTypeRepository.save(appointmentType);

        res.status(200).send(
            new TTResponse(__("appointmentTypes.appointmentTypeUpdated"))
        );
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default UpdateAppointmentType;
