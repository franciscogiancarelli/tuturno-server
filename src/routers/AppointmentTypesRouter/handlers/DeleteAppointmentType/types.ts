//INCLUDE DELETE APPOINTMENT TYPE DECLARATION BELOW THIS COMMENT//
import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateDeleteAppointmenTypeRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type DeleteAppointmenTypeQuery = {
    id: string;
};

export type DeleteAppointmenTypeHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    DeleteAppointmenTypeQuery
>;

/**
 * @openapi
 *  /api/appointmentTypes/:
 *   delete:
 *    summary: Delete an appointment type
 *    tags:
 *     - AppointmentTypes
 *    description: >
 *      This endpoint allows you to delete an appointmentType.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *    - in: "query"
 *      name: "id"
 *      description: "Id of the appointment type you wish to delete"
 *      required: true
 *      type: string
 *    responses:
 *     200:
 *      description: The appointment type was deleted.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
