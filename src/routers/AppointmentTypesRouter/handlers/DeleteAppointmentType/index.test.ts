import initTestEntities, {
    testApp,
    testToken,
    testServer,
    testEntities,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("AppointmentTypes Router works well", () => {
    test("can delete an appointmentType", async () => {
        return request(testApp)
            .delete(
                `/api/appointmentTypes?id=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    "Appointment type deleted successfully."
                );
            });
    });
    test("can't delete an inexistant appointmentType", async () => {
        return request(testApp)
            .delete(
                `/api/appointmentTypes?id=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("appointmentTypes.appointmentTypeNotFound")
                );
            });
    });

    test("can't delete an appointmentType without sending an id", async () => {
        return request(testApp)
            .delete(`/api/appointmentTypes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't delete an appointmentType if id is not UUID type", async () => {
        return request(testApp)
            .delete(`/api/appointmentTypes/?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
