import TTError from "typedef/TTError";
import AppointmentType from "entities/AppointmentType";
import { getRepository } from "typeorm";
import TTResponse from "typedef/TTResponse";
import { __ } from "locales";
import { DeleteAppointmenTypeHandler } from "./types";

const AppointmentTypeRepository = getRepository(AppointmentType);

const DeleteAppointmentType: DeleteAppointmenTypeHandler = async (req, res) => {
    try {
        const id = req.query.id;
        if (!id) throw "You must specify a valid id.";
        const { affected } =
            await AppointmentTypeRepository.createQueryBuilder()
                .delete()
                .where("appointment_type.id = :id", { id })
                .execute();
        if (affected === 1)
            res.send(new TTResponse("Appointment type deleted successfully."));
        else {
            throw __("appointmentTypes.appointmentTypeNotFound");
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default DeleteAppointmentType;
