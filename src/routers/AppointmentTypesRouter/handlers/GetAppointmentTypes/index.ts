import AppointmentType from "entities/AppointmentType";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { GetAppointmentTypeHandler } from "./types";

const AppointmentTypeRepository = getRepository(AppointmentType);

const PAGE_SIZE = 20;

const GetAppointmentTypes: GetAppointmentTypeHandler = async (req, res) => {
    try {
        let query =
            AppointmentTypeRepository.createQueryBuilder("appointment_type");

        /** Filter by business */
        const businessId = req.query.businessId;
        if (businessId)
            query = query.andWhere(
                "appointment_type.businessId = :businessId",
                {
                    businessId,
                }
            );

        /** Filter by resource */
        const resourceId = req.query.resourceId;
        if (resourceId)
            query = query.leftJoinAndSelect(
                "appointment_type.resources",
                "resources"
            );

        /** Filter by id */
        const id = req.query.id;
        if (id) query = query.andWhere("appointment_type.id = :id", { id });

        /** Filter by duration */
        const duration = req.query.duration;
        if (duration)
            query = query.andWhere("appointment_type.duration = :duration", {
                duration,
            });

        /** Filter by price */
        const price = req.query.price;
        if (price)
            query = query.andWhere("appointment_type.price = :price", {
                price,
            });

        /** Filter by currency */
        const currency = req.query.currency;
        if (currency)
            query = query.andWhere("appointment_type.currency = :currency", {
                currency,
            });

        /** Filter by needConfirmation */
        const needConfirmation = req.query.needConfirmation;
        if (price)
            query = query.andWhere(
                "appointment_type.needConfirmation = :needConfirmation",
                {
                    needConfirmation,
                }
            );

        /** Paginate */
        const page = Number(req.query.page ?? 0);
        query = query.skip(page * (req.query.pageSize ?? PAGE_SIZE));
        query = query.take(req.query.pageSize ?? PAGE_SIZE);

        let appointmentTypes = await query.getMany();

        /*Convert price to float type*/
        appointmentTypes = appointmentTypes.map((type) => {
            type.price = type.price.replace("$", "");
            return type;
        });
        res.status(200).send(
            new TTResponse<AppointmentType[]>(appointmentTypes)
        );
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default GetAppointmentTypes;
