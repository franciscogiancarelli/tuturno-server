import initTestEntities, {
    testApp,
    testToken,
    testServer,
    testEntities,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("AppointmentTypes Router works well", () => {
    test("can get appointmentTypes", async () => {
        return request(testApp)
            .get(`/api/appointmentTypes?id=${testEntities.appointmentType.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(
                    testEntities.appointmentType.id
                );
            });
    });
    test("can get appointmentTypes by businessId", async () => {
        return request(testApp)
            .get(`/api/appointmentTypes?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });
    test("can get appointmentTypes by resourceId", async () => {
        return request(testApp)
            .get(`/api/appointmentTypes?resourceId=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });
    test("can get appointmentTypes by id", async () => {
        return request(testApp)
            .get(`/api/appointmentTypes?id=${testEntities.appointmentType.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });

    test("can get appointmentTypes by duration", async () => {
        return request(testApp)
            .get(
                `/api/appointmentTypes?duration=${testEntities.appointmentType.duration}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });

    test("can get appointmentTypes by price", async () => {
        return request(testApp)
            .get(
                `/api/appointmentTypes?price=${testEntities.appointmentType.price}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });

    test("can get appointmentTypes by currency", async () => {
        return request(testApp)
            .get(
                `/api/appointmentTypes?currency=${testEntities.appointmentType.currency}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });

    test("cant get appointmentTypes if types of duration, price, currency, needConfirmation, page or pageSize are wrong", async () => {
        return request(testApp)
            .get(
                `/api/appointmentTypes?duration=23&price=asd&currency=ASD&needConfirmation=asd&page=-1&pageSize=51`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("appointmentTypes.wrongDurationType")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("appointmentTypes.wrongPriceType")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("appointmentTypes.wrongCurrencyType")
                );
                expect(res.body.errors[3].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[4].msg).toMatch(
                    __("general.invalidPageNumber")
                );
                expect(res.body.errors[5].msg).toMatch(
                    __("general.pageSizeTooBig")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
