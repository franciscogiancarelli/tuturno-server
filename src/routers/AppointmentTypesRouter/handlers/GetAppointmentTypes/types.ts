//INCLUDE GET APPOINTMENTS TYPE DECLARATION BELOW THIS COMMENT//

import AppointmentType, { Currency } from "entities/AppointmentType";
import { RequestHandler } from "express";
import { oneOf, query } from "express-validator";
import { __ } from "locales";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateGetAppointmentTypeRequest = [
    query("id").optional().isUUID().withMessage(__("general.invalidId")),
    query("businessId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("resourceId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("duration")
        .optional()
        .isInt()
        .isDivisibleBy(5)
        .withMessage(__("appointmentTypes.wrongDurationType")),
    query("price")
        .optional()
        .isNumeric()
        .withMessage(__("appointmentTypes.wrongPriceType")),
    oneOf(
        [
            query("currency").optional().equals(Currency.ARS),
            query("currency").optional().equals(Currency.EUR),
            query("currency").optional().equals(Currency.USD),
        ],
        __("appointmentTypes.wrongCurrencyType")
    ),
    query("needConfirmation")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("page")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("general.invalidPageNumber")),
    query("pageSize")
        .optional()
        .isInt({ lt: 51 })
        .withMessage(__("general.pageSizeTooBig")),
    validationHandler,
];

export type GetAppointmentTypeQuery = {
    id?: string;
    businessId?: string;
    resourceId: string;
    duration?: number;
    price: number;
    currency: Currency;
    page?: number;
    needConfirmation?: boolean;
    pageSize?: number;
};

export type GetAppointmentTypeHandler = RequestHandler<
    unknown,
    TTResponse<AppointmentType[]> | TTError,
    unknown,
    GetAppointmentTypeQuery
>;

/**
 * @openapi
 *  /api/appointmentTypes/:
 *   get:
 *    summary: List AppointmentTypes
 *    tags:
 *     - AppointmentTypes
 *    description: >
 *      This endpoint allows you to list appointmentTypes.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *    - in: query
 *      name: businessId
 *      description: Filter by business
 *      nullable: true
 *    - in: query
 *      name: AppointmentTypeId
 *      description: Filter by AppointmentType
 *    - in: query
 *      name: id
 *      description: Find by id
 *      type: string
 *      nullable: true
 *    - in: query
 *      name: duration
 *      description: Filter by duration
 *      nullable: true
 *    - in: query
 *      name: price
 *      description: Find by price
 *      type: string
 *    - in: query
 *      name: resourceId
 *      nullable: true
 *      description: Find by resourceId
 *      type: string
 *    - in: query
 *      name: needConfirmation
 *      nullable: true
 *      description: Filter by needConfirmation property.
 *      type: boolean
 *    - in: query
 *      name: page
 *      description: Filter by page
 *      nullable: true
 *    - in: query
 *      name: pageSize
 *      description: Use this for pagination
 *      nullable: true
 *    responses:
 *     200:
 *      description: The appointmentTypes were listed.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           type: array
 *           items:
 *            $ref: '#/components/schemas/AppointmentType'
 */
