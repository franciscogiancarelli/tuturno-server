//INCLUDE ADD APPOINTMENT TYPE DECLARATION BELOW THIS COMMENT//

import AppointmentType, { Currency } from "entities/AppointmentType";
import { RequestHandler } from "express";
import { body, oneOf, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateAddAppointmentTypeRequest = [
    authenticate,
    requireVerifiedEmail,
    query("businessId").notEmpty().withMessage(__("general.missingId")),
    query("businessId").isUUID().withMessage(__("general.invalidId")),
    body("name").notEmpty().withMessage(__("appointmentTypes.missingName")),
    body("description")
        .notEmpty()
        .withMessage(__("appointmentTypes.missingDescription")),
    body("duration")
        .notEmpty()
        .withMessage(__("appointmentTypes.missingDuration")),
    body("price").notEmpty().withMessage(__("appointmentTypes.missingPrice")),
    body("currency")
        .notEmpty()
        .withMessage(__("appointmentTypes.missingCurrency")),
    body("needConfirmation")
        .notEmpty()
        .withMessage(__("appointmentTypes.missingNeedConfirmation")),
    body("duration")
        .isInt()
        .isDivisibleBy(5)
        .withMessage(__("appointmentTypes.wrongDurationType")),
    body("price")
        .isCurrency({ allow_negatives: false })
        .withMessage(__("appointmentTypes.wrongPriceType")),
    oneOf(
        [
            body("currency").equals(Currency.ARS),
            body("currency").equals(Currency.EUR),
            body("currency").equals(Currency.USD),
        ],
        __("appointmentTypes.wrongCurrencyType")
    ),
    body("needConfirmation").isBoolean().withMessage(__("general.wrongState")),
    body("maxDailyUserRequest")
        .optional()
        .isInt({ gt: 0 })
        .withMessage(__("appointmentTypes.wrongMaxUserRequestType")),
    body("maxWeeklyUserRequest")
        .optional()
        .isInt({ gt: 0 })
        .withMessage(__("appointmentTypes.wrongMaxUserRequestType")),
    body("maxMonthlyUserRequest")
        .optional()
        .isInt({ gt: 0 })
        .withMessage(__("appointmentTypes.wrongMaxUserRequestType")),
    body("minimumTimeInAdvance")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("appointmentTypes.wrongMinimumTimeInAdvance")),
    validationHandler,
];

export type AddAppointmentTypeQuery = {
    businessId: string;
};

export type AddAppointmentTypeBody = Omit<
    AppointmentType,
    "id" | "resources" | "createdAt" | "updatedAt" | "deletedAt"
>;

export type AddAppointmentTypeHandler = RequestHandler<
    unknown,
    TTResponse<AppointmentType> | TTError,
    AddAppointmentTypeBody,
    AddAppointmentTypeQuery
>;

/**
 * @openapi
 *  /api/appointmentTypes/:
 *   post:
 *    summary: Create an appointment type
 *    tags:
 *     - AppointmentTypes
 *    description: >
 *      This endpoint allows you to create an appointmentType.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: businessId
 *       in: query
 *       required: true
 *       description: Use this param to add an appointment type for a business
 *       schema:
 *        type: string
 *    requestBody:
 *     required: true
 *     content:
 *      description: Appointment Type body
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         name:
 *          type: string
 *          nullable: false
 *          example: Turno corte simple
 *         description:
 *          type: string
 *          nullable: false
 *          example: lorem ipsum lorem ipsum
 *         price:
 *          type: number
 *          nullable: false
 *          example: 80
 *         currency:
 *          type: enum
 *          nullable: false
 *          example: ARS
 *         duration:
 *          type: number
 *          nullable: false
 *          example: 60
 *         needConfirmation:
 *          type: boolean
 *          nullable: false
 *         maxDailyUserRequest:
 *          type: integer
 *          nullable: true
 *         maxWeeklyUserRequest:
 *          type: integer
 *          nullable: true
 *         maxMonthlyUserRequest:
 *          type: integer
 *          nullable: true
 *         minimumTimeInAdvance:
 *          type: integer
 *          nullable: true
 *    responses:
 *     200:
 *      description: The appointment type was created.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           $ref: '#/components/schemas/AppointmentType'
 */
