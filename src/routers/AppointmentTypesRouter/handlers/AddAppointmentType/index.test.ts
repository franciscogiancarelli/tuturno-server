import initTestEntities, {
    testApp,
    testToken,
    testServer,
    testOtherToken,
    testEntities,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const testAppointmentTypeId = "05673fc6-2c27-4c50-b84e-a551ee520953";
const testAppointmentTypeName = "TestAppointmentType";
const testAppointmentTypePrice = 3000;
const testAppointmentTypeCurrency = "ARS";
const testAppointmentTypeNeedConfirmation = true;
const testAppointmentTypeDescription = "TestAppointmentTypeDescription";
const testAppointmentTypeDuration = 60;
const testAppointmentTypeMaxDailyUserRequest = 4;
const testAppointmentTypeMaxWeeklyUserRequest = 10;
const testAppointmentTypeMaxMonthlyUserRequest = 45;

beforeAll(() => {
    return initTestEntities();
});

describe("AppointmentTypes Router works well", () => {
    test("can add an appointmentType", () => {
        return request(testApp)
            .post(
                `/api/appointmentTypes?businessId=${testEntities.business.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testAppointmentTypeId,
                name: testAppointmentTypeName,
                description: testAppointmentTypeDescription,
                price: testAppointmentTypePrice,
                currency: testAppointmentTypeCurrency,
                duration: testAppointmentTypeDuration,
                needConfirmation: testAppointmentTypeNeedConfirmation,
                maxDailyUserRequest: testAppointmentTypeMaxDailyUserRequest,
                maxWeeklyUserRequest: testAppointmentTypeMaxWeeklyUserRequest,
                maxMonthlyUserRequest: testAppointmentTypeMaxMonthlyUserRequest,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toHaveProperty("id");
                expect(res.body.data.id).toEqual(testAppointmentTypeId);
            });
    });

    test("can't add appointmentType if i am not the business owner", () => {
        return request(testApp)
            .post(
                `/api/appointmentTypes?businessId=${testEntities.business.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                name: testAppointmentTypeName,
                description: testAppointmentTypeDescription,
                price: testAppointmentTypePrice,
                currency: testAppointmentTypeCurrency,
                duration: testAppointmentTypeDuration,
                needConfirmation: testAppointmentTypeNeedConfirmation,
                maxDailyUserRequest: testAppointmentTypeMaxDailyUserRequest,
                maxWeeklyUserRequest: testAppointmentTypeMaxWeeklyUserRequest,
                maxMonthlyUserRequest: testAppointmentTypeMaxMonthlyUserRequest,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("businesses.businessNotFound")
                );
            });
    });

    test("can't add appointmentType businessId not specified", () => {
        return request(testApp)
            .post(`/api/appointmentTypes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                name: testAppointmentTypeName,
                description: testAppointmentTypeDescription,
                price: testAppointmentTypePrice,
                currency: testAppointmentTypeCurrency,
                duration: testAppointmentTypeDuration,
                needConfirmation: testAppointmentTypeNeedConfirmation,
                maxDailyUserRequest: testAppointmentTypeMaxDailyUserRequest,
                maxWeeklyUserRequest: testAppointmentTypeMaxWeeklyUserRequest,
                maxMonthlyUserRequest: testAppointmentTypeMaxMonthlyUserRequest,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't add appointmentType if business is not UUID type", () => {
        return request(testApp)
            .post(`/api/appointmentTypes?businessId=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: testAppointmentTypeName,
                description: testAppointmentTypeDescription,
                currency: testAppointmentTypeCurrency,
                price: testAppointmentTypePrice,
                duration: testAppointmentTypeDuration,
                needConfirmation: testAppointmentTypeNeedConfirmation,
                maxDailyUserRequest: testAppointmentTypeMaxDailyUserRequest,
                maxWeeklyUserRequest: testAppointmentTypeMaxWeeklyUserRequest,
                maxMonthlyUserRequest: testAppointmentTypeMaxMonthlyUserRequest,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't add appointmentType if any of the required body params is not specified", () => {
        return request(testApp)
            .post(
                `/api/appointmentTypes?businessId=${testEntities.business.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({})
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("appointmentTypes.missingName")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("appointmentTypes.missingDescription")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("appointmentTypes.missingDuration")
                );
                expect(res.body.errors[3].msg).toMatch(
                    __("appointmentTypes.missingPrice")
                );
                expect(res.body.errors[4].msg).toMatch(
                    __("appointmentTypes.missingCurrency")
                );
                expect(res.body.errors[5].msg).toMatch(
                    __("appointmentTypes.missingNeedConfirmation")
                );
            });
    });

    test("can't add appointmentType if duration, price, currecy, needConfirmation or MaxUserRequest types are wrong", () => {
        return request(testApp)
            .post(
                `/api/appointmentTypes?businessId=${testEntities.business.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: testAppointmentTypeName,
                description: testAppointmentTypeDescription,
                duration: 23,
                price: -14,
                currency: "ASD",
                needConfirmation: "asd",
                maxDailyUserRequest: "asd",
                maxWeeklyUserRequest: "asd",
                maxMonthlyUserRequest: "asd",
                minimumTimeInAdvance: -1,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("appointmentTypes.wrongDurationType")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("appointmentTypes.wrongPriceType")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("appointmentTypes.wrongCurrencyType")
                );
                expect(res.body.errors[3].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[4].msg).toMatch(
                    __("appointmentTypes.wrongMaxUserRequestType")
                );
                expect(res.body.errors[5].msg).toMatch(
                    __("appointmentTypes.wrongMaxUserRequestType")
                );
                expect(res.body.errors[6].msg).toMatch(
                    __("appointmentTypes.wrongMaxUserRequestType")
                );
                expect(res.body.errors[7].msg).toMatch(
                    __("appointmentTypes.wrongMinimumTimeInAdvance")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
