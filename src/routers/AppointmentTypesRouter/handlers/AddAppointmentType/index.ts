import Business from "entities/Business";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import AppointmentType from "entities/AppointmentType";
import Auth from "typedef/Auth";
import { __ } from "locales";
import { AddAppointmentTypeHandler } from "./types";

const AppointmentTypeRepository = getRepository(AppointmentType);
const BusinessRepository = getRepository(Business);

const AddAppointmentType: AddAppointmentTypeHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);

        /** Find the business */
        const businessId = req.query.businessId;
        const business = await BusinessRepository.createQueryBuilder("business")
            .where("business.id = :businessId", { businessId })
            .andWhere(`business."ownerUid" = :uid`, auth)
            .getOne();
        if (!business) throw __("businesses.businessNotFound");

        /** insert the new appointmentType into the database */
        const appointmentTypeData = req.body;

        const result = await AppointmentTypeRepository.createQueryBuilder()
            .insert()
            .values({
                ...appointmentTypeData,
                business: { id: business.id },
            })
            .returning([
                "id",
                "name",
                "price",
                "currency",
                "description",
                "needConfirmation",
                "maxDailyUserRequest",
                "maxWeeklyUserRequest",
                "maxMonthlyUserRequest",
                "minimumTimeInAdvance",
                "businessId",
            ])
            .execute();
        res.status(200).send(
            new TTResponse<AppointmentType>(
                result.generatedMaps[0] as AppointmentType
            )
        );
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default AddAppointmentType;
