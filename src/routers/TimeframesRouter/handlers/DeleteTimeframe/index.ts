import { CancelledBy } from "entities/Appointment";
import Timeframe from "entities/Timeframe";
import cancelTimeframesAppointments from "helpers/cancelTimeframeAppointments";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { DeleteTimeframeHandler } from "./types";

const TimeframeRepository = getRepository(Timeframe);

const DeleteTimeframe: DeleteTimeframeHandler = async (req, res) => {
    try {
        const force = req.query.force;
        const message = req.body.message ?? "";
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const ids = [...new Set(req.body.timeframesIds)];
        /** Find the timeframes by id */
        const timeframesWithAppointments =
            await TimeframeRepository.createQueryBuilder("timeframe")
                .leftJoinAndSelect(
                    "timeframe.appointments",
                    "appointment",
                    `appointment.cancelledBy = '${CancelledBy.notCancelled}'`
                )
                .leftJoinAndSelect("timeframe.business", "business")
                .where(`timeframe."ownerUid" = :uid`, auth)
                .andWhere("timeframe.id IN (:...ids)", { ids: ids })
                .getMany();

        if (timeframesWithAppointments.length !== ids.length) {
            throw __("timeframes.timeframeNotFound");
        }

        /** Delete the timeframes */
        if (
            timeframesWithAppointments.every(
                (timeframe) => timeframe.appointments.length === 0
            ) ||
            force
        ) {
            const { affected } = await TimeframeRepository.createQueryBuilder(
                "timeframe"
            )
                .softDelete()
                .where("timeframe.id IN (:...ids)", { ids: ids })
                .andWhere(`timeframe."ownerUid" = :uid`, auth)
                .execute();
            if (affected !== ids.length)
                throw __("timeframes.timeframeNotFound");
            /** Cancel the appointments of the deleted appointments if force is set to true */
            if (force)
                await cancelTimeframesAppointments(
                    timeframesWithAppointments,
                    auth,
                    message
                );
            res.status(200).send(
                new TTResponse(__("timeframes.timeframesDeleted"))
            );
        } else {
            /** Don´t delete, throw warning because at least one timeframe has appointments */
            throw new TTError({
                code: "TTS-TI-DE-THA",
                message: __("timeframes.haveActiveAppointmentsOnDelete"),
                status: 400,
            });
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default DeleteTimeframe;
