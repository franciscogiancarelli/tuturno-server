//INCLUDE THE DELETE TIMEFRAME DECLARATION TYPES BELOW THIS COMMENT//
import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateDeleteTimeframeRequest = [
    authenticate,
    requireVerifiedEmail,
    query("force").optional().isBoolean().withMessage(__("general.wrongState")),
    body("timeframesIds")
        .notEmpty()
        .withMessage(__("general.missingParam"))
        .bail()
        .isArray()
        .withMessage(__("general.mustBeArray")),
    body("timeframesIds.*").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type DeleteTimeframeQuery = {
    force?: boolean;
};

export type DeleteTimeframeBody = {
    timeframesIds: string[];
    message?: string;
};

export type DeleteTimeframeHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    DeleteTimeframeBody,
    DeleteTimeframeQuery
>;

/**
 * @openapi
 *  /api/timeframes/:
 *   delete:
 *    summary: Delete Timeframe
 *    tags:
 *     - Timeframe
 *    description: >
 *      This endpoint allows to delete a timeframe of a resource the Timeframe owns.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: force
 *       in: query
 *       required: false
 *       type: boolean
 *       description: If true and timeframe/s have appointments on any of them, it will cancel the appointments.
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              timeframesIds:
 *                type: array
 *                items:
 *                  type: string
 *                  nullable: false
 *              message:
 *                type: string
 *                required: false
 *    responses:
 *     200:
 *      description: The timeframe was deleted
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           type: object
 */
