import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const inexistantTimeframe = "ccd7cf4b-2dfa-4b92-931c-a1c870a65321";

beforeAll(() => {
    return initTestEntities();
});

describe("Timeframe Router works well", () => {
    test("can't delete a timeframe if it's not mine", async () => {
        return request(testApp)
            .delete(`/api/timeframes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                timeframesIds: [testEntities.timeframe.id],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("timeframes.timeframeNotFound")
                );
            });
    });

    test("can't delete a timeframe if no id given", async () => {
        return request(testApp)
            .delete(`/api/timeframes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframesIds: [],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.missingParam")
                );
            });
    });

    test("can't delete a timeframe if parameters have wrong type", async () => {
        return request(testApp)
            .delete(`/api/timeframes?force=asd`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframesIds: ["asd"],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[1].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't delete a timeframe that doesn't exist", async () => {
        return request(testApp)
            .delete(`/api/timeframes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframesIds: [inexistantTimeframe],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("timeframes.timeframeNotFound")
                );
            });
    });

    test("can't delete a timeframe with active appointments ", async () => {
        return request(testApp)
            .delete(`/api/timeframes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframesIds: [testEntities.timeframe.id],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("timeframes.haveActiveAppointmentsOnDelete")
                );
            });
    });
    test("can delete a timeframe if it has appointments but force parameter is passed ", async () => {
        return request(testApp)
            .delete(`/api/timeframes?force=true`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframesIds: [testEntities.timeframe.id],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("timeframes.timeframesDeleted")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
