import { CancelledBy } from "entities/Appointment";
import Timeframe from "entities/Timeframe";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { GetTimeframeHandler } from "./types";

const TimeframeRepository = getRepository(Timeframe);
const PAGE_SIZE = 20;

const GetTimeframes: GetTimeframeHandler = async (req, res) => {
    try {
        let query = TimeframeRepository.createQueryBuilder("timeframe");

        /** Filter by business */
        const businessId = req.query.businessId;
        if (businessId)
            query = query.andWhere("timeframe.businessId = :businessId", {
                businessId,
            });

        /** Filter by From Date in UTC format */
        const from = req.query.from;
        if (from)
            query = query.andWhere("timeframe.to::date >= :from::date", {
                from,
            });

        /** Filter by To Date in UTC format */
        const to = req.query.to;
        if (to)
            query = query.andWhere("timeframe.from::date <= :to::date", { to });

        /** Filter by resource */
        const resourceId = req.query.resourceId;
        if (resourceId)
            query = query.andWhere("timeframe.resourceId = :resourceId", {
                resourceId,
            });

        /** Filter by appointment type */
        const appointmentTypeId = req.query.appointmentTypeId;
        if (appointmentTypeId)
            query = query
                .leftJoinAndSelect("timeframe.resource", "resource")
                .leftJoin("resource.appointmentTypes", "appointmentType")
                .andWhere("appointmentType.id = :appointmentTypeId", {
                    appointmentTypeId,
                });

        /** Filter by id */
        const id = req.query.id;
        if (id) query = query.andWhere("timeframe.id = :id", { id });

        /** Filter by day */
        const day = req.query.day;
        if (day)
            query = query.andWhere(
                "date_trunc('day', timeframe.from) <= :day::date AND date_trunc('day', timeframe.to) >= :day::date",
                { day }
            );

        /** Attach their appointments */
        const withAppointments = req.query.withAppointments;
        if (withAppointments) {
            query = query.leftJoinAndSelect(
                "timeframe.appointments",
                "appointment",
                `appointment.cancelledBy = '${CancelledBy.notCancelled}'`
            );
        }

        /** Attach their resource */
        /** I check for if appointmentTypeId given to prevent calling leftJoin twice */
        const withResource = req.query.withResource;
        if (withResource && !appointmentTypeId) {
            query = query.leftJoinAndSelect("timeframe.resource", "resource");
        }

        /** Paginate */
        if (!from || !to) {
            const page = Number(req.query.page ?? 0);
            query = query.skip(page * (req.query.pageSize ?? PAGE_SIZE));
            query = query.take(req.query.pageSize ?? PAGE_SIZE);
        }

        const timeframes = await query.getMany();
        res.status(200).send(new TTResponse<Timeframe[]>(timeframes));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default GetTimeframes;
