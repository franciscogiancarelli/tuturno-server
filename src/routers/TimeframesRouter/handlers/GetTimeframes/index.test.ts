import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Timeframe Router works well", () => {
    test("can get timeframes", async () => {
        return request(testApp)
            .get(`/api/timeframes?id=${testEntities.timeframe.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.timeframe.id);
            });
    });

    test("can get timeframes by businessId", async () => {
        return request(testApp)
            .get(`/api/timeframes?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.timeframe.id);
            });
    });

    test("can get timeframes by resourceId", async () => {
        return request(testApp)
            .get(`/api/timeframes?resourceId=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.timeframe.id);
            });
    });

    test("can get timeframes by appointmentTypeId", async () => {
        return request(testApp)
            .get(
                `/api/timeframes?appointmentTypeId=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.timeframe.id);
            });
    });

    test("can get timeframes by day", async () => {
        return request(testApp)
            .get(
                `/api/timeframes?day=${
                    testEntities.timeframe.from.split(" ")[0]
                }&id=${testEntities.timeframe.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.timeframe.id);
            });
    });

    test("can get timeframes by page", async () => {
        return request(testApp)
            .get(`/api/timeframes?page=1&id=${testEntities.timeframe.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data.length).toEqual(0);
            });
    });

    test("can get timeframes from and to", async () => {
        return request(testApp)
            .get(
                `/api/timeframes?id=${testEntities.timeframe.id}&from=${testEntities.timeframe.from}&to=${testEntities.timeframe.to}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.data[0].id).toEqual(testEntities.timeframe.id);
            });
    });

    test("can get timeframes with their appointments", async () => {
        await request(testApp)
            .get(`/api/timeframes?withAppointments=true`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.data[0].appointments).toBeInstanceOf(Array);
            });
    });

    test("can get timeframes with their resource", async () => {
        await request(testApp)
            .get(
                `/api/timeframes?withResource=true&id=${testEntities.timeframe.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.data[0].resource.id).toEqual(
                    testEntities.resource.id
                );
            });
    });

    test("can get timeframes with their resource filtering by appointmentType", async () => {
        await request(testApp)
            .get(
                `/api/timeframes?withResource=true&id=${testEntities.timeframe.id}&appointmentTypeId=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.data[0].resource.id).toEqual(
                    testEntities.resource.id
                );
            });
    });

    test("can't get timeframes if parameters have wrong type", async () => {
        return request(testApp)
            .get(
                `/api/timeframes?id=asdf
                &businessId=asdf
                &resourceId=asdf
                &appointmentTypeId=asdf
                &withAppointments=asdf
                &withResource=asdf
                &from="asd"
                &to="asd"
                 &page=-1
                 &pageSize=51`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[1].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[2].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[3].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[4].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[5].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[6].msg).toMatch(
                    __("general.invalidDateTimeFormat")
                );
                expect(res.body.errors[7].msg).toMatch(
                    __("general.invalidDateTimeFormat")
                );
                expect(res.body.errors[8].msg).toMatch(
                    __("general.invalidPageNumber")
                );
                expect(res.body.errors[9].msg).toMatch(
                    __("general.pageSizeTooBig")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
