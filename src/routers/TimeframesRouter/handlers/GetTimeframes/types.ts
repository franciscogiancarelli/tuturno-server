//INCLUDE THE GET TIMEFRAMES DECLARATION TYPES BELOW THIS COMMENT//

import Timeframe from "entities/Timeframe";
import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateGetTimeframeRequest = [
    query("id").optional().isUUID().withMessage(__("general.invalidId")),
    query("businessId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("resourceId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("appointmentTypeId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("withAppointments")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("withResource")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("from")
        .optional()
        .isISO8601()
        .withMessage(__("general.invalidDateTimeFormat")),
    query("to")
        .optional()
        .isISO8601()
        .withMessage(__("general.invalidDateTimeFormat")),
    query("page")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("general.invalidPageNumber")),
    query("pageSize")
        .optional()
        .isInt({ lt: 51 })
        .withMessage(__("general.pageSizeTooBig")),
    validationHandler,
];

export type GetTimeframeQuery = {
    id?: string;
    businessId?: string;
    resourceId?: string;
    appointmentTypeId: string;
    from?: Date;
    to?: Date;
    day?: Date;
    page?: number;
    pageSize?: number;
    withAppointments?: boolean;
    withResource?: boolean;
};

export type GetTimeframeHandler = RequestHandler<
    unknown,
    TTResponse<Timeframe[]> | TTError,
    unknown,
    GetTimeframeQuery
>;

/**
 * @openapi
 *  /api/timeframes/:
 *   get:
 *    summary: List Timeframes
 *    tags:
 *     - Timeframe
 *    description: >
 *      This endpoint allows you to list timeframes.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *    - name: businessId
 *      in: query
 *      description: Filter by Business
 *      required: false
 *      schema:
 *       type: string
 *    - in: query
 *      name: TimeframeId
 *      schema:
 *       type: string
 *      description: Filter by Timeframe
 *      nullable: true
 *    - in: query
 *      name: appointmentTypeId
 *      schema:
 *       type: string
 *      description: Filter by AppointmentType
 *      nullable: true
 *    - in: query
 *      name: withAppointments
 *      schema:
 *       type: boolean
 *      description: List the timeframe's appointments to check for availability
 *    - in: query
 *      name: withResource
 *      schema:
 *       type: boolean
 *      description: List the timeframes with it's resource
 *      nullable: true
 *    - in: query
 *      name: day
 *      schema:
 *       type: date
 *      description: Filter by Day
 *      nullable: true
 *    - in: query
 *      name: page
 *      schema:
 *       type: integer
 *      description: Filter by Page
 *      nullable: true
 *    - in: query
 *      name: pageSize
 *      schema:
 *       type: integer
 *      description: Use for pagination
 *      nullable: true
 *    - in: query
 *      name: from
 *      schema:
 *       type: string
 *      description: Filter by From Date in UTC format
 *      nullable: true
 *    - in: query
 *      name: to
 *      schema:
 *       type: string
 *      description: Filter by To Date in UTC format
 *      nullable: true
 *    - in: query
 *      name: id
 *      schema:
 *       type: string
 *      description: Find by Id
 *      nullable: true
 *    responses:
 *     200:
 *      description: The timeframes were listed.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           type: array
 *           items:
 *            $ref: '#/components/schemas/Timeframe'
 */
