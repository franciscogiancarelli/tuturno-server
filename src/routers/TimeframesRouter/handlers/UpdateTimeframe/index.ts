import { CancelledBy } from "entities/Appointment";
import Timeframe from "entities/Timeframe";
import cancelAppointmentsByBusiness from "helpers/cancelAppointmentsByBusiness";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { UpdateTimeframeHandler } from "./types";

const TimeframeRepository = getRepository(Timeframe);

const UpdateTimeframe: UpdateTimeframeHandler = async (req, res) => {
    try {
        const force = req.query.force;
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id;
        const message = req.body.message;
        delete req.body.message;
        const newTimeframe = req.body;

        /** Find the timeframe with its appointments*/
        const query = TimeframeRepository.createQueryBuilder("timeframe")
            .leftJoinAndSelect(
                "timeframe.appointments",
                "appointment",
                `appointment.cancelledBy = '${CancelledBy.notCancelled}' AND (appointment.from < :newFrom OR appointment.to > :newTo)`,
                { newFrom: newTimeframe.from, newTo: newTimeframe.to }
            )
            .leftJoinAndSelect("timeframe.business", "business")
            .where(`timeframe."ownerUid" = :uid`, auth)
            .andWhere("timeframe.id = :id", { id });

        const timeframe = await query.getOne();

        if (!timeframe) {
            throw __("timeframes.timeframeNotFound");
        } else {
            if (timeframe.appointments.length === 0 || force) {
                /** If timeframe doesn't have appointments or force parameter is passed, update the timeframe*/
                const { affected } =
                    await TimeframeRepository.createQueryBuilder("timeframe")
                        .update(newTimeframe)
                        .where(`timeframe."ownerUid" = :uid`, auth)
                        .andWhere("timeframe.id = :id", { id })
                        .execute();
                if (affected !== 1) {
                    throw __("timeframes.timeframeNotFound");
                }
                if (force && timeframe.appointments.length > 0) {
                    /** Cancel affected appointments and send cancelation emails to appointment owners*/
                    const ids = timeframe.appointments.map(({ id }) => id);
                    await cancelAppointmentsByBusiness(ids, auth, message);
                }
                res.status(200).send(
                    new TTResponse(__("timeframes.timeframesUpdated"))
                );
            } else {
                /** Force parameter is not passed, so must throw error if timeframe has active appointments*/
                throw new TTError({
                    code: "TTS-TI-UP-THA",
                    message: __("timeframes.haveActiveAppointments"),
                    status: 400,
                });
            }
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default UpdateTimeframe;
