//INCLUDE THE UPDATE TIMEFRAME DECLARATION TYPES BELOW THIS COMMENT//

import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateUpdateTimeframeRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id")
        .notEmpty()
        .withMessage(__("general.missingId"))
        .bail()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("force").optional().isBoolean().withMessage(__("general.wrongState")),
    body("id").isEmpty().withMessage(__("general.cantChangeId")),
    body("from")
        .optional()
        .isISO8601()
        .withMessage(__("general.invalidDateTimeFormat")),
    body("to")
        .optional()
        .isISO8601()
        .withMessage(__("general.invalidDateTimeFormat")),
    body("width")
        .optional()
        .isInt({ gt: 0 })
        .withMessage(__("timeframes.wrongWidthType")),
    body("businessId").isEmpty().withMessage(__("general.cantChangeId")),
    body("resourceId").isEmpty().withMessage(__("general.cantChangeId")),
    body("from")
        .custom((from, { req }) => {
            if (!from && !req.body.to && !req.body.width) {
                return false;
            }
            return true;
        })
        .withMessage(__("timeframes.mustProvideData")),

    validationHandler,
];

export type UpdateTimeframeQuery = {
    id: string;
    force?: boolean;
};

export type UpdateTimeframeBody = {
    from?: Date;
    to?: Date;
    width?: number;
    message?: string;
};

export type UpdateTimeframeHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    UpdateTimeframeBody,
    UpdateTimeframeQuery
>;

/**
 * @openapi
 *  /api/timeframes/:
 *   put:
 *    summary: update Timeframes
 *    tags:
 *     - Timeframe
 *    description: >
 *     You can use this endpoint to update some properties of a timeframe.
 *     This will only work if you are the owner of said timeframe. Otherwise it
 *     it will tell you it's unable to find the timeframe. Keep in mind, DateTimes must be  *     compliant with ISO 8601. If no tz is passed, it will be interpreted as UTC.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *       description: The id of the timeframe you want to update.
 *     - name: force
 *       in: query
 *       required: false
 *       type: boolean
 *       description: If true and timeframe has appointments on it, it will cancel the appointments.
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         from:
 *          type: string
 *          required: false
 *          example: 2020-10-10 00:00:00
 *         to:
 *          type: string
 *          required: false
 *          example: 2020-10-11 00:00:00
 *         width:
 *          type: number
 *          required: false
 *          example: 1
 *         message:
 *          type: string
 *          required: false
 *          example: Sorry for the cancelation
 *    responses:
 *     200:
 *      description: The timeframe was updated.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 *           example: Timeframe updated.
 */
