import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const updatedTimeframeFrom = new Date("2033-10-10 16:00:00").toISOString();

beforeAll(() => {
    return initTestEntities();
});

describe("Timeframe Router works well", () => {
    test("can't update a timeframe's id, resourceId or businessId", async () => {
        return request(testApp)
            .put(`/api/timeframes?id=${testEntities.timeframe.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: "Asdfasdf",
                businessId: "Asdfasdf",
                resourceId: "Asdfasdf",
                width: testEntities.timeframe.width + 1,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.cantChangeId")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("general.cantChangeId")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("general.cantChangeId")
                );
            });
    });

    test("can't update a timeframe if no id is given.", async () => {
        return request(testApp)
            .put(`/api/timeframes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                width: testEntities.timeframe.width + 1,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't update a timeframe if no id is not UUID type.", async () => {
        return request(testApp)
            .put(`/api/timeframes?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                width: testEntities.timeframe.width + 1,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't update a timeframe if you are not the owner.", async () => {
        return request(testApp)
            .put(`/api/timeframes?id=${testEntities.timeframe.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                width: testEntities.timeframe.width + 1,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("timeframes.timeframeNotFound")
                );
            });
    });

    test("can't update a timeframe if no body params are passed.", async () => {
        return request(testApp)
            .put(`/api/timeframes?id=${testEntities.timeframe.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({})
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("timeframes.mustProvideData")
                );
            });
    });

    test("can't update a timeframe if  body  or query params have wrong type.", async () => {
        return request(testApp)
            .put(`/api/timeframes?id=${testEntities.timeframe.id}&force=tru`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: "asd",
                to: "asd",
                width: "asd",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.wrongState")
                );

                expect(res.body.errors[1].msg).toMatch(
                    __("general.invalidDateTimeFormat")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("general.invalidDateTimeFormat")
                );
                expect(res.body.errors[3].msg).toMatch(
                    __("timeframes.wrongWidthType")
                );
            });
    });

    test("can't update a timeframe if it has affected appointments, and force parameter is not passed", async () => {
        return request(testApp)
            .put(`/api/timeframes?id=${testEntities.timeframe.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: updatedTimeframeFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("timeframes.haveActiveAppointments")
                );
            });
    });

    test("can update a timeframe with force parameter", async () => {
        return request(testApp)
            .put(`/api/timeframes?id=${testEntities.timeframe.id}&force=true`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                to: updatedTimeframeFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("timeframes.timeframesUpdated")
                );
            });
    });
});

afterAll(async () => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
