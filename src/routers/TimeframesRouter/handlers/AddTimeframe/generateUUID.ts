import { getManager } from "typeorm";

async function generateUUID(): Promise<string> {
    const EntityManager = getManager();
    const recurrencyUUIDQuery = await EntityManager.query(
        "SELECT uuid_generate_v4()"
    );
    return recurrencyUUIDQuery[0].uuid_generate_v4;
}

export default generateUUID;
