import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const testTimeframeId = "6959af9e-f0a1-4ab7-aaca-9d67bf161473";
const testTimeframeFrom = "2020-10-10 08:00:00";
const testTimeframeTo = "2020-10-10 12:00:00";
const testEndRecurrency = "2020-10-22 00:00:00";
const testTimeframeWidth = 1;

beforeAll(() => {
    return initTestEntities();
});

describe("Timeframe Router works well", () => {
    test("can't add a timeframe if the resource is not mine", async () => {
        return request(testApp)
            .post(`/api/timeframes?resourceId=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                timeframes: [
                    {
                        from: testTimeframeFrom,
                        to: testTimeframeTo,
                        width: testTimeframeWidth,
                    },
                ],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("resources.resourceNotFound")
                );
            });
    });

    test("can't add a timeframe if no resourceId given.", async () => {
        return request(testApp)
            .post(`/api/timeframes`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframes: [
                    {
                        from: testTimeframeFrom,
                        to: testTimeframeTo,
                        width: testTimeframeWidth,
                    },
                ],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't add a timeframe if  resourceId is not UUID type.", async () => {
        return request(testApp)
            .post(`/api/timeframes?resourceId=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframes: [
                    {
                        from: testTimeframeFrom,
                        to: testTimeframeTo,
                        width: testTimeframeWidth,
                    },
                ],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't add a timeframe if no from,to or width is given.", async () => {
        return request(testApp)
            .post(`/api/timeframes?resourceId=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframes: [
                    {
                        param: "test",
                    },
                ],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].param).toMatch("timeframes[0].from");
                expect(res.body.errors[0].location).toMatch("body");
                expect(res.body.errors[0].msg).toMatch(
                    __("timeframes.missingFrom")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("timeframes.missingTo")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("timeframes.missingWidth")
                );
            });
    });

    test("can't add a timeframe if from,to or width have wrong type.", async () => {
        return request(testApp)
            .post(`/api/timeframes?resourceId=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframes: [
                    {
                        from: "asd",
                        to: "asd",
                        width: "asd",
                    },
                ],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].param).toMatch("timeframes[0].from");
                expect(res.body.errors[0].location).toMatch("body");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.invalidDateTimeFormat")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("general.invalidDateTimeFormat")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("timeframes.wrongWidthType")
                );
            });
    });

    test("can add a timeframe", async () => {
        return request(testApp)
            .post(`/api/timeframes?resourceId=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframes: [
                    {
                        id: testTimeframeId,
                        from: testTimeframeFrom,
                        to: testTimeframeTo,
                        width: testTimeframeWidth,
                    },
                ],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testTimeframeId);
            });
    });

    test("can add recurrent timeframes", async () => {
        return request(testApp)
            .post(`/api/timeframes?resourceId=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                timeframes: [
                    {
                        id: testTimeframeId,
                        from: testTimeframeFrom,
                        to: testTimeframeTo,
                        width: testTimeframeWidth,
                    },
                ],
                endRecurrency: testEndRecurrency,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data).toHaveLength(2);
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
