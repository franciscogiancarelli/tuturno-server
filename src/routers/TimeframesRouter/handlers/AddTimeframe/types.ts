//INCLUDE THE ADD TIMEFRAME DECLARATION TYPES BELOW THIS COMMENT//

import Timeframe from "entities/Timeframe";
import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateAddTimeframeRequest = [
    authenticate,
    requireVerifiedEmail,
    query("resourceId").notEmpty().withMessage(__("general.missingId")),
    query("resourceId").isUUID().withMessage(__("general.invalidId")),
    body("endRecurrency")
        .custom((endRecurrency, { req }) => {
            if (!endRecurrency && req.body.timeframes.lenght > 1) {
                return false;
            }
            return true;
        })
        .withMessage("timeframes.mustSendEndRecurrency"),
    body("timeframes")
        .notEmpty()
        .isArray()
        .withMessage(__("general.missingParam")),
    body("timeframes.*.from")
        .notEmpty()
        .withMessage(__("timeframes.missingFrom"))
        .bail()
        .isISO8601()
        .withMessage(__("general.invalidDateTimeFormat")),
    body("timeframes.*.to")
        .notEmpty()
        .withMessage(__("timeframes.missingTo"))
        .bail()
        .isISO8601()
        .withMessage(__("general.invalidDateTimeFormat")),
    body("timeframes.*.width")
        .notEmpty()
        .withMessage(__("timeframes.missingWidth"))
        .bail()
        .isInt({ gt: 0 })
        .withMessage(__("timeframes.wrongWidthType")),
    validationHandler,
];

export type AddTimeframeQuery = {
    resourceId: string;
};

export type AddTimeframeBody = {
    timeframes: { id?: string; from: string; to: string; width: number }[];
    endRecurrency?: string;
};

export type AddTimeframeHandler = RequestHandler<
    unknown,
    TTResponse<Timeframe[]> | TTError,
    AddTimeframeBody,
    AddTimeframeQuery
>;

export type TimeframeInsert = {
    id?: string;
    recurrentId?: string;
    from: string;
    to: string;
    width: number;
    business: { id: string };
    owner: { uid: string };
    resource: { id: string };
};

/**
 * @openapi
 * /api/timeframes/:
 *  post:
 *    summary: Add a timeframe or a list of timeframes
 *    tags:
 *      - Timeframe
 *    description: >
 *      This endpoint allows you to add timeframes. Keep in mind, the DateTimes
 *      must be compliant with ISO 8601. If no timezone is specified, it will be interpreted  *      as UTC.
 *    security:
 *      - BearerAuth: []
 *    parameters:
 *      - name: resourceId
 *        in: query
 *        required: true
 *        description: Use this param to specify the resource you want to add one or many timeframes.
 *        schema:
 *          type: string
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              timeframes:
 *                type: array
 *                items:
 *                  type: object
 *                  properties:
 *                    from:
 *                      type: date
 *                      nullable: false
 *                      example: "2020-10-10 9:00:00"
 *                    to:
 *                      type: date
 *                      nullable: false
 *                      example: "2020-10-10 18:00:00"
 *                    width:
 *                      type: integer
 *                      example: 1
 *                      nullable: false
 *              endRecurrency:
 *               type: string;
 *               nullable: true;
 *    responses:
 *      200:
 *        description: The timeframes were added.
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                code:
 *                  type: string
 *                data:
 *                  type: array
 *                  items:
 *                    $ref: "#/components/schemas/Timeframe"
 */
