import Resource from "entities/Resource";
import Timeframe from "entities/Timeframe";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { AddTimeframeHandler, TimeframeInsert } from "./types";
import generateUUID from "./generateUUID";
import { addDays } from "date-fns";

const ResourceRepository = getRepository(Resource);
const TimeframeRepository = getRepository(Timeframe);

const AddTimeframe: AddTimeframeHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);

        const endRecurrency = req.body.endRecurrency
            ? new Date(req.body.endRecurrency)
            : undefined;

        const timeframes = req.body.timeframes;
        /** Find the resource */
        const resourceId = req.query.resourceId;
        const resource = await ResourceRepository.createQueryBuilder("resource")
            .where("resource.id = :resourceId", { resourceId })
            .leftJoinAndSelect("resource.business", "business")
            .leftJoinAndSelect("resource.owner", "owner")
            .andWhere("owner.uid = :uid", auth)
            .getOne();
        if (!resource) throw __("resources.resourceNotFound");

        /** Create the timeframes list */
        const timeframesDTO = [];

        if (!endRecurrency) {
            /** Create just one new timeframe */
            timeframesDTO.push({
                id: timeframes[0]["id"],
                from: timeframes[0]["from"],
                to: timeframes[0]["to"],
                width: timeframes[0]["width"],
                owner: { uid: resource.owner.uid },
                business: { id: resource.business.id },
                resource: { id: resource.id },
                appointments: [],
            });
        } else {
            /** Is neccesary to create recurrent timeframes */
            const recurrencyUUID = await generateUUID();

            timeframes.forEach((timeframe, i) => {
                let timeframeFromDate = new Date(timeframes[i].from);
                let timeframeToDate = new Date(timeframes[i].to);
                while (timeframeFromDate < (endRecurrency as Date)) {
                    timeframesDTO.push({
                        from: timeframeFromDate.toISOString(),
                        to: timeframeToDate.toISOString(),
                        width: timeframes[i]["width"],
                        owner: { uid: resource.owner.uid },
                        business: { id: resource.business.id },
                        resource: { id: resource.id },
                        appointments: [],
                        recurrentId: recurrencyUUID,
                    });
                    timeframeFromDate = addDays(timeframeFromDate, 7);
                    timeframeToDate = addDays(timeframeToDate, 7);
                }
            });
        }
        const newTimeframes: TimeframeInsert[] = timeframesDTO;

        /**Insert the new timeframes */
        const result = await TimeframeRepository.createQueryBuilder()
            .insert()
            .values(newTimeframes)
            .returning([
                "id",
                "recurrentId",
                "from",
                "to",
                "width",
                "resource",
                "business",
                "owner",
            ])
            .execute();

        /** Send the new Timeframes */
        if (result.identifiers.length === newTimeframes.length) {
            res.status(200).send(
                new TTResponse(result.generatedMaps as Timeframe[])
            );
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default AddTimeframe;
