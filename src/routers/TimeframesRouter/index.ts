import express from "express";
import AddTimeframe from "./handlers/AddTimeframe/index";
import { validateAddTimeframeRequest } from "./handlers/AddTimeframe/types";
import DeleteTimeframe from "./handlers/DeleteTimeframe/index";
import { validateDeleteTimeframeRequest } from "./handlers/DeleteTimeframe/types";
import GetTimeframe from "./handlers/GetTimeframes/index";
import { validateGetTimeframeRequest } from "./handlers/GetTimeframes/types";
import UpdateTimeframe from "./handlers/UpdateTimeframe/index";
import { validateUpdateTimeframeRequest } from "./handlers/UpdateTimeframe/types";

const TimeframesRouter = express.Router({ mergeParams: true });

TimeframesRouter.get("/", validateGetTimeframeRequest, GetTimeframe);

TimeframesRouter.post("/", validateAddTimeframeRequest, AddTimeframe);
TimeframesRouter.put("/", validateUpdateTimeframeRequest, UpdateTimeframe);
TimeframesRouter.delete("/", validateDeleteTimeframeRequest, DeleteTimeframe);

export default TimeframesRouter;
