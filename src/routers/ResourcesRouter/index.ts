import express from "express";
import GetResources from "./handlers/GetResources/index";
import DeleteResource from "./handlers/DeleteResource/index";
import AddResource from "./handlers/AddResource/index";
import UpdateResource from "./handlers/UpdateResource/index";
import RelateAppointmentType from "./handlers/RelateAppointmentType/index";
import { validateUpdateResourceRequest } from "./handlers/UpdateResource/types";
import { validateGetResourceRequest } from "./handlers/GetResources/types";
import { validateAddResourceRequest } from "./handlers/AddResource/types";
import { validateRelateAppointmentTypeRequest } from "./handlers/RelateAppointmentType/types";
import { validateDeleteResourceRequest } from "./handlers/DeleteResource/types";

const ResourcesRouter = express.Router({ mergeParams: true });

ResourcesRouter.get("/", validateGetResourceRequest, GetResources);

ResourcesRouter.post("/", validateAddResourceRequest, AddResource);

ResourcesRouter.put("/", validateUpdateResourceRequest, UpdateResource);

ResourcesRouter.post(
    "/relateAppointmentType",
    validateRelateAppointmentTypeRequest,
    RelateAppointmentType
);

ResourcesRouter.delete("/", validateDeleteResourceRequest, DeleteResource);

export default ResourcesRouter;
