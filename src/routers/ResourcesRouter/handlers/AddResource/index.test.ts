import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const testResourceId = "045379a4-db3c-4d55-a4e2-72e2a3374656";
const testResourceName = "TestResource";
const testResourceInterval = 15;

beforeAll(() => {
    return initTestEntities();
});

describe("Resource Router works well", () => {
    test("can add a resource", () => {
        return request(testApp)
            .post(`/api/resources?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testResourceId,
                name: testResourceName,
                interval: testResourceInterval,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.data).toHaveProperty("id");
                expect(res.body.data.id).toEqual(testResourceId);
            });
    });

    test("can't add a resource for a business that's not mine.", () => {
        return request(testApp)
            .post(`/api/resources?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                name: testResourceName,
                interval: testResourceInterval,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("businesses.businessNotFound")
                );
            });
    });

    test("can't add a resource if no businessId given.", () => {
        return request(testApp)
            .post(`/api/resources`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                id: testResourceId,
                name: testResourceName,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't add a resource if businessId is not UUID type.", () => {
        return request(testApp)
            .post(`/api/resources?businessId=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                id: testResourceId,
                name: testResourceName,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't add a resource if it doesn't have a name and interval type is wrong", () => {
        return request(testApp)
            .post(`/api/resources?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                id: testResourceId,
                interval: 23,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("resources.missingName")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("resources.wrongIntervalType")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
