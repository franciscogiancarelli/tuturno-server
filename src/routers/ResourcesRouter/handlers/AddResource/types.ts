//INCLUDE ADD RESOURCE DECLARATION TYPES BELOW THIS COMMENT//
import Resource from "entities/Resource";
import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateAddResourceRequest = [
    authenticate,
    requireVerifiedEmail,
    query("businessId").notEmpty().withMessage(__("general.missingId")),
    query("businessId").isUUID().withMessage(__("general.invalidId")),
    body("name").notEmpty().withMessage(__("resources.missingName")),
    body("interval").notEmpty().withMessage(__("resources.missingInterval")),
    body("interval")
        .isInt()
        .isDivisibleBy(5)
        .withMessage(__("resources.wrongIntervalType")),
    validationHandler,
];

export type AddResourceQuery = {
    businessId: string;
};

export type AddResourceBody = Omit<
    Resource,
    "id" | "createdAt" | "updatedAt" | "deletedAt"
>;

export type AddResourceHandler = RequestHandler<
    unknown,
    TTResponse<Resource> | TTError,
    AddResourceBody,
    AddResourceQuery
>;

/**
 * @openapi
 *  /api/resources/:
 *   post:
 *    summary: Add Resource
 *    tags:
 *     - Resources
 *    description: >
 *      This endpoint allows you to add a new resource. You can also specify an array with the appointments types objects that will be related with this new resource.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: businessId
 *       in: query
 *       required: true
 *       description: Use this param to add a resource from a Business
 *       schema:
 *        type: string
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         name:
 *          nullable: false
 *          type: string
 *          example: Peluquero 1
 *         description:
 *          nullable: true
 *          type: string
 *          example: Descripcion peluquero 1
 *         interval:
 *          nullable: false
 *          type: number
 *          example: 15
 *         appointmentTypes:
 *          nullable: true
 *          type: array
 *          example: 15
 *    responses:
 *     200:
 *      description: The resource was created.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           type: object
 */
