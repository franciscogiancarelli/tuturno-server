import Business from "entities/Business";
import Resource from "entities/Resource";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { AddResourceHandler } from "./types";

const BusinessRepository = getRepository(Business);
const ResourceRepository = getRepository(Resource);

const AddResource: AddResourceHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);

        /** Find the business */
        const businessId = req.query.businessId;
        const business = await BusinessRepository.createQueryBuilder("business")
            .where("business.id = :businessId", { businessId })
            .andWhere(`business."ownerUid" = :uid`, auth)
            .leftJoinAndSelect("business.owner", "owner")
            .getOne();
        if (!business) throw __("businesses.businessNotFound");

        /** Create the resource */
        const newResourceData = ResourceRepository.create(req.body);
        newResourceData.business = business;
        newResourceData.owner = business.owner;

        /** Save and return the new Resource */
        const newResource = await ResourceRepository.save(newResourceData);
        res.status(200).send(new TTResponse<Resource>(newResource));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default AddResource;
