//INCLUDE DELETE RESOURCE DECLARATION TYPES BELOW THIS COMMENT//
import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateDeleteResourceRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type DeleteResourceQuery = {
    id: string;
};

export type DeleteResourceHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    DeleteResourceQuery
>;

/**
 * @openapi
 *  /api/resources/:
 *   delete:
 *    summary: Delete Resource
 *    tags:
 *     - Resources
 *    description: >
 *      This endpoint allows to delete a resource the Resource owns.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: false
 *       description: The id of the Resource you want to delete.
 *       schema:
 *        type: string
 *    responses:
 *     200:
 *      description: The resource was deleted
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 *           example: Resource safely removed.
 */
