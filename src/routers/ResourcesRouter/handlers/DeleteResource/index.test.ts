import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Resource Router works well", () => {
    test("can't delete a resource that's not mine", async () => {
        return request(testApp)
            .delete(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("resources.resourceNotFound")
                );
            });
    });

    test("can't delete a resource if no id given", async () => {
        return request(testApp)
            .delete(`/api/resources`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't delete a resource if id is not UUID type.", () => {
        return request(testApp)
            .delete(`/api/resources?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can delete a resource", async () => {
        return request(testApp)
            .delete(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("resources.resourceDeleted")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
