import AppointmentType from "entities/AppointmentType";
import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateUpdateResourceRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    body("id").isEmpty().withMessage(__("general.cantChangeId")),
    body("businessId").isEmpty().withMessage(__("general.cantChangeId")),
    body("name")
        .custom((name, { req }) => {
            if (
                !name &&
                !req.body.description &&
                !req.body.interval &&
                !req.body.appointmentTypes
            ) {
                return false;
            }
            return true;
        })
        .withMessage(__("resources.mustProvideData")),
    body("appointmentTypes").optional().isArray(),
    body("appointmentTypes[0]").optional().isObject(),
    body("appointmentTypes[0].id").optional().isUUID(),
    body("interval")
        .optional()
        .isInt()
        .isDivisibleBy(5)
        .withMessage(__("resources.wrongIntervalType")),
    validationHandler,
];

export type UpdateResourceQuery = {
    id: string;
};

export type UpdateResourceBody = {
    name?: string;
    description?: string;
    interval?: number;
    appointmentTypes?: AppointmentType[];
};

export type UpdateResourceHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    UpdateResourceBody,
    UpdateResourceQuery
>;

/**
 * @openapi
 *  /api/resources:
 *   put:
 *    summary: Update Resource
 *    tags:
 *     - Resources
 *    description: >
 *     You can use this endpoint to update some properties of a resource.
 *     This will only work if you are the owner of said resource. Otherwise it
 *     it will tell you it's unable to find the resource.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *       description: The id of the resource you want to update.
 *    requestBody:
 *     required: true
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         name:
 *          type: string
 *          required: false
 *          example: Recurso 1
 *         description:
 *          type: string
 *          required: false
 *          example: Descripción recurso 1
 *         interval:
 *          type: number
 *          required: false
 *          example: 60
 *         appointmentTypes:
 *          type: array
 *          required: false
 *          items:
 *           type: object
 *           properties:
 *            id:
 *             type: string
 *             required: true
 *    responses:
 *     200:
 *      description: The resource was updated.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 *           example: Resource updated.
 */
