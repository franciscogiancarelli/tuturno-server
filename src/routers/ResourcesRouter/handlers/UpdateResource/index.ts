import AppointmentType from "entities/AppointmentType";
import Resource from "entities/Resource";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { UpdateResourceHandler } from "./types";

const ResourceRepository = getRepository(Resource);
const AppointmentTypeRepository = getRepository(AppointmentType);

const UpdateResource: UpdateResourceHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id as string;
        const newResourceData = req.body;

        /** Find the resource */
        const resource = await ResourceRepository.findOne({
            id,
            owner: { uid: auth.uid },
        });
        if (!resource) throw new TTError(__("resources.cantUpdateResource"));

        /** Update the resource */
        if (newResourceData.name) resource.name = newResourceData.name;
        if (newResourceData.description)
            resource.description = newResourceData.description;
        if (newResourceData.interval)
            resource.interval = newResourceData.interval;
        if (newResourceData.appointmentTypes) {
            /**
             * Find the business appointment types to make sure,
             * the appoitment types sent are valid */
            const businessAppointmentTypes =
                await AppointmentTypeRepository.createQueryBuilder(
                    "appointment_types"
                )
                    .leftJoin("appointment_types.business", "business")
                    .leftJoin("business.resources", "resource")
                    .where(`resource.id = :resourceId`, {
                        resourceId: resource.id,
                    })
                    .distinct()
                    .getMany();
            /** Add only the appointment types that belong to the resource's business */
            resource.appointmentTypes = newResourceData.appointmentTypes.filter(
                (appointmentType) => {
                    return businessAppointmentTypes.find(
                        (businessAppointmentType) => {
                            return (
                                appointmentType.id ===
                                businessAppointmentType.id
                            );
                        }
                    );
                }
            );
        }

        /** Save the resource */
        await ResourceRepository.save(resource);

        res.status(200).send(new TTResponse(__("resources.resourceUpdated")));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default UpdateResource;
