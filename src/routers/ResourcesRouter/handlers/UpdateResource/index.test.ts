import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Resource Router works well", () => {
    test("can't update a resource's id", async () => {
        return request(testApp)
            .put(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: "asdfasdf",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.cantChangeId")
                );
            });
    });

    test("can't update a resource's business", async () => {
        return request(testApp)
            .put(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                businessId: "Asdfadsfasd",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.cantChangeId")
                );
            });
    });

    test("can't update a resource if no id given", async () => {
        return request(testApp)
            .put(`/api/resources`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: "Asdfsdf",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't update a resource if resource id is not UUID type and interval type is wrong", async () => {
        return request(testApp)
            .put(`/api/resources?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: "Asdfsdf",
                interval: 41,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[1].msg).toMatch(
                    __("resources.wrongIntervalType")
                );
            });
    });

    test("can't update a resource if I'm not the owner", async () => {
        return request(testApp)
            .put(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                name: "Asdfsdf",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("resources.cantUpdateResource")
                );
            });
    });

    test("can't update a resource if no body params are provided", async () => {
        return request(testApp)
            .put(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({})
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("resources.mustProvideData")
                );
            });
    });

    test("can update a resource", async () => {
        return request(testApp)
            .put(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: `${testEntities.resource.name}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("resources.resourceUpdated")
                );
            });
    });

    test("can update a resource's appointment types", async () => {
        return request(testApp)
            .put(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentTypes: [{ id: testEntities.appointmentType.id }],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.message).toMatch(
                    __("resources.resourceUpdated")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
