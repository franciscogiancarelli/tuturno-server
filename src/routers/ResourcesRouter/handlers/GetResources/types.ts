//INCLUDE GET RESOURCES DECLARATION TYPES BELOW THIS COMMENT//

import Resource from "entities/Resource";
import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateGetResourceRequest = [
    query("id").optional().isUUID().withMessage(__("general.invalidId")),
    query("businessId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("appointmentTypeId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("withTypes")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("page")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("general.invalidPageNumber")),
    query("pageSize")
        .optional()
        .isInt({ lt: 51 })
        .withMessage(__("general.pageSizeTooBig")),
    validationHandler,
];

export type GetResourceQuery = {
    id?: string;
    businessId?: string;
    appointmentTypeId: string;
    name?: string;
    description?: string;
    interval?: number;
    page?: number;
    pageSize?: number;
    withTypes?: boolean;
};

export type GetResourceHandler = RequestHandler<
    unknown,
    TTResponse<Resource[]> | TTError,
    unknown,
    GetResourceQuery
>;

/**
 * @openapi
 *  /api/resources/:
 *   get:
 *    summary: Get Resources
 *    tags:
 *     - Resources
 *    description: >
 *      This endpoint allows you to get a list of resources
 *    parameters:
 *     - name: id
 *       in: query
 *       required: false
 *       description: Use this param to filter by Resource
 *       schema:
 *        type: string
 *     - name: businessId
 *       in: query
 *       required: false
 *       description: Use this param to filter by Business
 *       schema:
 *        type: string
 *     - name: appointmentTypeId
 *       in: query
 *       required: false
 *       description: Use this param to filter by appointment type
 *       schema:
 *        type: string
 *     - name: withTypes
 *       in: query
 *       required: false
 *       description: Use this param to attach  appointments type. Must be boolean true or false.
 *       schema:
 *        type: boolean
 *     - name: page
 *       in: query
 *       required: false
 *       description: Use this param for pagination
 *       schema:
 *        type: integer
 *     - name: pageSize
 *       in: query
 *       required: false
 *       description: Use this param for pagination
 *       schema:
 *        type: integer
 *    responses:
 *     200:
 *      description: Returns a paginated list of resources.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          data:
 *           type: array
 *           items:
 *            $ref: '#/components/schemas/Resource'
 */
