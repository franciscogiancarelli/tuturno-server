import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Resource Router works well", () => {
    test("can get resources", async () => {
        return request(testApp)
            .get(`/api/resources?id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.resource.id);
            });
    });

    test("can get resources by businessId", async () => {
        return request(testApp)
            .get(`/api/resources?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.resource.id);
            });
    });

    test("can get resources by appointmentType", async () => {
        return request(testApp)
            .get(
                `/api/resources?appointmentTypeId=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.resource.id);
            });
    });

    test("can get resources with its appointmentTypes", async () => {
        return request(testApp)
            .get(`/api/resources?withTypes=true&id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].appointmentTypes).toBeInstanceOf(Array);
                expect(res.body.data[0].appointmentTypes[0].id).toEqual(
                    testEntities.appointmentType.id
                );
            });
    });

    test("can get resources by page", async () => {
        return request(testApp)
            .get(`/api/resources?page=1&id=${testEntities.resource.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data.length).toEqual(0);
            });
    });
});

test("can't get resources if withType param has an invalid value", async () => {
    return request(testApp)
        .get(`/api/resources?withTypes=asdf&id=${testEntities.resource.id}`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .set("Authorization", `Bearer ${testToken}`)
        .then((res) => {
            expect(res.statusCode).toEqual(400);
            expect(res.body).toHaveProperty("code");
            expect(res.body.code).toEqual("400");
            expect(res.body).toHaveProperty("errors");
            expect(res.body.errors[0].msg).toMatch(__("general.wrongState"));
        });
});

test("can't get resource if resource id is not UUID type", async () => {
    return request(testApp)
        .get(`/api/resources?id=asdf`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .set("Authorization", `Bearer ${testToken}`)
        .then((res) => {
            expect(res.statusCode).toEqual(400);
            expect(res.body).toHaveProperty("code");
            expect(res.body.code).toEqual("400");
            expect(res.body).toHaveProperty("errors");
            expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
        });
});

test("can't get resources if business id and appointment type id are not UUID type", async () => {
    return request(testApp)
        .get(`/api/resources?id=asdf&businessId=asdf`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .set("Authorization", `Bearer ${testToken}`)
        .then((res) => {
            expect(res.statusCode).toEqual(400);
            expect(res.body).toHaveProperty("code");
            expect(res.body.code).toEqual("400");
            expect(res.body).toHaveProperty("errors");
            expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            expect(res.body.errors[1].msg).toMatch(__("general.invalidId"));
        });
});

test("can't get resources if page or pageSize have invalid values", async () => {
    return request(testApp)
        .get(
            `/api/resources?id=${testEntities.resource.id}&page=-1&pageSize=51`
        )
        .then((res) => {
            expect(res.statusCode).toEqual(400);
            expect(res.body).toHaveProperty("code");
            expect(res.body.code).toEqual("400");
            expect(res.body).toHaveProperty("errors");
            expect(res.body.errors[0].msg).toMatch(
                __("general.invalidPageNumber")
            );
            expect(res.body.errors[1].msg).toMatch(
                __("general.pageSizeTooBig")
            );
        });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
