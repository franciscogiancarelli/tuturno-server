import Resource from "entities/Resource";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { GetResourceHandler } from "./types";

const ResourceRepository = getRepository(Resource);
const PAGE_SIZE = 20;

const GetResources: GetResourceHandler = async (req, res) => {
    try {
        let query = ResourceRepository.createQueryBuilder("resource");

        /** Filter by business */
        const businessId = req.query.businessId;
        if (businessId)
            query = query.andWhere("resource.businessId = :businessId", {
                businessId,
            });

        /** Filter by appointment type */
        const appointmentTypeId = req.query.appointmentTypeId;
        if (appointmentTypeId)
            query = query
                .leftJoin("resource.appointmentTypes", "appointmentType")
                .andWhere("appointmentType.id = :appointmentTypeId", {
                    appointmentTypeId,
                });

        /** Filter by id */
        const id = req.query.id;
        if (id) query = query.andWhere("resource.id = :id", { id });

        /** Attach appointmentTypes */
        const withTypes = req.query.withTypes;
        if (withTypes)
            query = query.leftJoinAndSelect(
                "resource.appointmentTypes",
                "appointmentTypes"
            );

        /** Paginate */
        const page = Number(req.query.page ?? 0);
        query = query.skip(page * (req.query.pageSize ?? PAGE_SIZE));
        query = query.take(req.query.pageSize ?? PAGE_SIZE);

        const resources = await query.getMany();
        res.status(200).send(new TTResponse<Resource[]>(resources));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default GetResources;
