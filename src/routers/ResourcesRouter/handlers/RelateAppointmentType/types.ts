//INCLUDE UPDATE RELATE APPOINTMENT TYPE DECLARATION TYPES BELOW THIS COMMENT//

import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateRelateAppointmentTypeRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    query("appointmentTypeId").notEmpty().withMessage(__("general.missingId")),
    query("appointmentTypeId").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type RelateAppointmentTypeQuery = {
    id: string;
    appointmentTypeId: string;
};

export type RelateAppointmentTypeHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    RelateAppointmentTypeQuery
>;

/**
 * @openapi
 *  /api/resources/relateappointmenttype/:
 *   post:
 *    summary: Relate Appointment Type to a Resource
 *    tags:
 *     - Resources
 *    description: >
 *      This endpoint allows you to relate a resource with an appointment type.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *       description: this is the resource Id. Use this param to relate a resource with an appointment type
 *       schema:
 *        type: string
 *     - name: appointmentTypeId
 *       in: query
 *       required: true
 *       description: Use this param to relate a resource with an appointment type
 *       schema:
 *        type: string
 *    responses:
 *     200:
 *      description: The relation was created.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
