import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const inexistantAppointmentTypeId = "d13c5e08-6cc3-4e1c-a33b-54de0f9ad657";

beforeAll(() => {
    return initTestEntities();
});

describe("Resource Router works well", () => {
    test("can't relate an appointmentType to a resource I'm not the owner", async () => {
        return request(testApp)
            .post(
                `/api/resources/relateAppointmentType?id=${testEntities.resource.id}&appointmentTypeId=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("resources.cantRelateResource")
                );
            });
    });

    test("can't relate an appointmentType to a resource if no resource id given", async () => {
        return request(testApp)
            .post(
                `/api/resources/relateAppointmentType?appointmentTypeId=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't relate an appointmentType if resource id is not UUID type", async () => {
        return request(testApp)
            .post(
                `/api/resources/relateAppointmentType?id=asdf&appointmentTypeId=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't relate an appointmentType to a resource if no appointmentTypeId given", async () => {
        return request(testApp)
            .post(
                `/api/resources/relateAppointmentType?id=${testEntities.resource.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't relate an appointmentType if appointmentTypeId is not UUID type", async () => {
        return request(testApp)
            .post(
                `/api/resources/relateAppointmentType?id=${testEntities.appointmentType.id}&appointmentTypeId=asdf`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't relate an inexistent appointmentType to a resource.", async () => {
        return request(testApp)
            .post(
                `/api/resources/relateAppointmentType?id=${testEntities.resource.id}&appointmentTypeId=${inexistantAppointmentTypeId}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("resources.appointmentTypeDoesntExist")
                );
            });
    });

    test("can relate an appointmentType to the resource", async () => {
        return request(testApp)
            .post(
                `/api/resources/relateAppointmentType?id=${testEntities.resource.id}&appointmentTypeId=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("resources.appointmentTypeRelated")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
