import Resource from "entities/Resource";
import AppointmentType from "entities/AppointmentType";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { __ } from "locales";
import { RelateAppointmentTypeHandler } from "./types";

const ResourceRepository = getRepository(Resource);
const AppointmentTypeRepository = getRepository(AppointmentType);

const RelateAppointmentType: RelateAppointmentTypeHandler = async (
    req,
    res
) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id as string;
        const appointmentTypeId = req.query.appointmentTypeId as string;

        /** Find the resource */
        const resource = await ResourceRepository.createQueryBuilder("resource")
            .leftJoinAndSelect("resource.appointmentTypes", "appointmentTypes")
            .leftJoinAndSelect("resource.business", "business")
            .where("resource.id = :id", { id: id })
            .andWhere(`resource."ownerUid" = :uid`, auth)
            .getOne();
        if (!resource) throw __("resources.cantRelateResource");

        /** Find the appointmentType */
        const appointmentType =
            await AppointmentTypeRepository.createQueryBuilder(
                "appointment_type"
            )
                .where("appointment_type.id = :appointmentTypeId", {
                    appointmentTypeId,
                })
                .andWhere(`appointment_type."businessId" = :businessId`, {
                    businessId: resource.business.id,
                })
                .getOne();
        if (!appointmentType) throw __("resources.appointmentTypeDoesntExist");

        resource.appointmentTypes.push(appointmentType);

        await ResourceRepository.save(resource);
        res.send(
            new TTResponse<string>(__("resources.appointmentTypeRelated"))
        );
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default RelateAppointmentType;
