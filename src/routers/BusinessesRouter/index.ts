import express from "express";
import GetBusinesses from "./handlers/GetBusinesses/index";
import AddBusiness from "./handlers/AddBusiness/index";
import DeleteBusiness from "./handlers/DeleteBusiness/index";
import UpdateBusiness from "./handlers/UpdateBusiness/index";
import RecoverBusiness from "./handlers/RecoverBusiness/index";
import { validateAddBusinessRequest } from "./handlers/AddBusiness/types";
import { validateDeleteBusinessRequest } from "./handlers/DeleteBusiness/types";
import { validateUpdateBusinessRequest } from "./handlers/UpdateBusiness/types";
import { validateRecoverBusinessRequest } from "./handlers/RecoverBusiness/types";
import { validateGetBusinessRequest } from "./handlers/GetBusinesses/types";
import { validateGetFeaturedBusinessRequest } from "./handlers/GetFeaturedBusinesses/types";
import GetFeaturedBusinesses from "./handlers/GetFeaturedBusinesses";
import { validateAddBusinessImageRequest } from "./handlers/AddBusinessImage/types";
import AddBusinessImage from "./handlers/AddBusinessImage";
import { validateDeleteBusinessImageRequest } from "./handlers/DeleteBusinessImage/types";
import DeleteBusinessImage from "./handlers/DeleteBusinessImage";

const BusinessesRouter = express.Router();

BusinessesRouter.get("/", validateGetBusinessRequest, GetBusinesses);
BusinessesRouter.get("/featuredBusinesses", validateGetFeaturedBusinessRequest, GetFeaturedBusinesses);
BusinessesRouter.post("/", validateAddBusinessRequest, AddBusiness);
BusinessesRouter.put("/", validateUpdateBusinessRequest, UpdateBusiness);
BusinessesRouter.delete("/", validateDeleteBusinessRequest, DeleteBusiness);
BusinessesRouter.patch("/", validateRecoverBusinessRequest, RecoverBusiness);
BusinessesRouter.post(
    "/addImage",
    validateAddBusinessImageRequest,
    AddBusinessImage
);
BusinessesRouter.delete(
    "/deleteImage",
    validateDeleteBusinessImageRequest,
    DeleteBusinessImage
);

export default BusinessesRouter;
