import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("DeleteBusinessImage", () => {
    test("can delete image", async () => {
        return request(testApp)
            .delete(
                `/api/businesses/deleteImage?id=${testEntities.business.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                imageUrl: `https://tuturno-space.fra1.digitaloceanspaces.com/images/business-images/${testEntities.business.id}/0.webp`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
