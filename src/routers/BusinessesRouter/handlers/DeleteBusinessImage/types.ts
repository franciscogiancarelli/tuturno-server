import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateDeleteBusinessImageRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    body("imageUrl").notEmpty().withMessage(__("businesses.missingImageUrl")),
    validationHandler,
];

export type DeleteBusinessImageQuery = {
    id: string;
};
export type DeleteBusinessImageBody = {
    imageUrl: string;
};

export type DeleteBusinessImageHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    DeleteBusinessImageBody,
    DeleteBusinessImageQuery
>;

/**
 * @openapi
 *  /api/businesses/deleteImage:
 *   delete:
 *    summary: Delete a business Image
 *    tags:
 *     - Businesses
 *    description: >
 *     You can use this endpoint to delete a business image
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *       description: The id of the business you want to remove the image from (you must be the owner).
 *    requestBody:
 *     required: true
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         imageUrl:
 *          type: string
 *          description: url of the image you want to delete
 *          required: false
 *    responses:
 *     200:
 *      description: The business was updated.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
