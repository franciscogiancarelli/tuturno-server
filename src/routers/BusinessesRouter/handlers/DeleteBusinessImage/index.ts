import Business from "entities/Business";
import removeImage from "helpers/removeImage";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { DeleteBusinessImageHandler } from "./types";

const BusinessRepository = getRepository(Business);

const DeleteBusinessImage: DeleteBusinessImageHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const imageUrl = req.body.imageUrl;
        const business = await BusinessRepository.findOne({
            id: req.query.id,
            owner: { uid: auth.uid },
        });
        if (!business) throw new TTError(__("businesses.businessNotFound"));
        if (
            imageUrl.includes(business.id) &&
            business.images?.includes(imageUrl)
        ) {
            await removeImage(req.body.imageUrl);
            business.images = business.images.filter(
                (image) => image !== imageUrl
            );
            await BusinessRepository.save(business);
            res.status(200).send(
                new TTResponse(__("businesses.businessUpdated"))
            );
        } else throw new TTError(__("businesses.businessNotFound"));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default DeleteBusinessImage;
