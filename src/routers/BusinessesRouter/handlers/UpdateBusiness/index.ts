/* eslint-disable no-console */
import Business from "entities/Business";
import { RequestHandler } from "express";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { UpdateBusinessBody } from "./types";
import uploadImage from "helpers/uploadImage";

const BusinessRepository = getRepository(Business);

const UpdateBusiness: RequestHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id;
        const { cover, ...newBusinessData }: UpdateBusinessBody = req.body;

        /** Find the business */
        const business = await BusinessRepository.createQueryBuilder("business")
            .where("business.id = :id", { id })
            .andWhere(`business."ownerUid" = :uid`, auth)
            .getOne();
        if (!business) throw __("businesses.businessNotFound");

        /** Update data */
        await BusinessRepository.update({ id: business.id }, newBusinessData);

        /** Update cover if sent */
        if (cover) {
            const coverUrl = await uploadImage(
                cover,
                `business-covers/${business.id}`
            );
            await BusinessRepository.update(
                { id: business.id },
                { cover: coverUrl }
            );
        }

        res.status(200).send(new TTResponse(__("businesses.businessUpdated")));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default UpdateBusiness;
