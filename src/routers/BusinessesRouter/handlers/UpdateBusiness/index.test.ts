import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Businesses Router works well", () => {
    beforeAll(() => {
        jest.mock("fs");
    });

    test("can update a business if no id given", async () => {
        return request(testApp)
            .put(`/api/businesses`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: `${testEntities.business.name}_updated`,
                description: `${testEntities.business.description}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't update a business's if id is not valid", async () => {
        return request(testApp)
            .put(`/api/businesses?id=asdfasdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: `${testEntities.business.name}_updated`,
                description: `${testEntities.business.description}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't update a business's it's not mine", async () => {
        return request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                name: `${testEntities.business.name}_updated`,
                description: `${testEntities.business.description}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("businesses.businessNotFound")
                );
            });
    });

    test("can't update business's createdAt date", async () => {
        return request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                createdAt: "asdfasdf",
                name: `${testEntities.business.name}_updated`,
                description: `${testEntities.business.description}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("businesses.cantUpdateCreatedAt")
                );
            });
    });

    test("can't update business's deletedAt date", async () => {
        return request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                deletedAt: "asdfasdf",
                name: `${testEntities.business.name}_updated`,
                description: `${testEntities.business.description}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("businesses.cantUpdateDeletedAt")
                );
            });
    });

    test("can update a business", async () => {
        return request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: `${testEntities.business.name}_updated`,
                description: `${testEntities.business.description}_updated`,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("businesses.businessUpdated")
                );
            });
    });

    test("can't update business's phone if it has wrong format, and cant update business name if it is empty", async () => {
        return request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: "",
                description: `${testEntities.business.description}_updated`,
                phone: "123",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("businesses.missingName")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("general.wrongPhone")
                );
            });
    });

    test("can update a business's cover with all supported image types", async () => {
        await request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                cover: "data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAKZCAYAAABAw",
            })
            .then((res) => expect(res.statusCode).toEqual(200));
        await request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                cover: "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAKZCAYAAABAw",
            })
            .then((res) => expect(res.statusCode).toEqual(200));
        await request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                cover: "data:image/webp;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAKZCAYAAABAw",
            })
            .then((res) => expect(res.statusCode).toEqual(200));
        return request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                cover: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAKZCAYAAABAw",
            })
            .then((res) => expect(res.statusCode).toEqual(200));
    });

    test("can't update a business's cover with an unsupported image type", async () => {
        return request(testApp)
            .put(`/api/businesses?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                cover: "data:image/asdf;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAKZCAYAAABAw",
            })
            .then((res) => expect(res.statusCode).toEqual(500));
    });

    afterAll(() => {
        jest.mock("fs");
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
