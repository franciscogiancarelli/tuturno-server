import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateUpdateBusinessRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    body("createdAt")
        .isEmpty()
        .withMessage(__("businesses.cantUpdateCreatedAt")),
    body("deletedAt")
        .isEmpty()
        .withMessage(__("businesses.cantUpdateDeletedAt")),
    body("name")
        .optional()
        .notEmpty()
        .withMessage(__("businesses.missingName")),
    body("phone")
        .optional()
        .isMobilePhone("es-AR")
        .withMessage(__("general.wrongPhone")),
    validationHandler,
];

export type UpdateBusinessQuery = {
    id: string;
};
export type UpdateBusinessBody = {
    name?: string;
    address?: string;
    phone?: string;
    type?: string;
    latitude?: number;
    longitude?: number;
    locality?: string;
    postalCode?: string;
    country?: string;
    cover?: string;
    images?: string[];
};

export type UpdateBusinessHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    UpdateBusinessBody,
    UpdateBusinessQuery
>;

/**
 * @openapi
 *  /api/businesses:
 *   put:
 *    summary: Update Business
 *    tags:
 *     - Businesses
 *    description: >
 *     You can use this endpoint to update some properties of a business.
 *     This will only work if you are the owner of said business. Otherwise it
 *     it will tell you it's unable to find the business.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *       description: The id of the business you want to update.
 *    requestBody:
 *     required: true
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         name:
 *          type: string
 *          required: false
 *          example: Barbería
 *         type:
 *          type: string
 *          required: false
 *          example: Gimnasio
 *         cover:
 *          type: string
 *          required: false
 *          example: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAclBMVEXMzMwAAADR0dHS0tLV1dW3t7elpaWIiIjAwMCwsLDLy8tbW1t3d3d8fHy/v7/ExMSVlZU6OjoeHh6np6dnZ2ckJCRvb29KSkpVVVWYmJiNjY0XFxcSEhJFRUUrKyuWlpYiIiIzMzNHR0dpaWk+Pj4LCwsebs21AAAE4UlEQVR4nO3Za5eiOBAGYKoSRECIFwSvjHa3//8vbgUEEnBn17M9O8w57/Ohu40YoaxUSjoIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/nzs6UeVYP9AO8KT1/+LdxjP4k/MPHmr34wj1ylpxoI4Kr6+img5nCqHWXr9yHbq7XcIs3CYJZZZqiIK+2k4WKyqa3rK35/4l9HkaWJyuhFVn8WdKOrOlAuiQyk/KvPmR8o1LZ4v4bwiSlel/Pzsng33dP60I9GcgjJQt9KeWE5lqCWftTlQ1p4pVxTZFNcL2pq3plcSxy4mOV12Wsk8fKJNM8Yh7WN5L52UVM4oKD21atMkWXZnp6+0bJ7J6NiOqZjSd86dFxR1MVEpdYtRLehk/+Tb5Zl3uuxDNyfL9jwdHLdDCVX6OaQzCscv/AlD5bK7WKaymyXQm0pCy+shELzdzC9RVLqdjBkq2lOP+2glXpL7O8h0zk0ty6OPSdQfoQ5X+Zu/6mGnO70V7P9HSD8mF5Vs7UrxoqWqu7MbrfPhgVqPL0rWXOjGpOjzRN3txElXr6xpmv526nCbnpJpkoLrajh1ztoa00ik5Hav0scmqRx26algiElJ3cFqZwflebeGbA9zWzzxizThHR25+Tid1Di6OW7O5+d1qvWk+iaPgw6GmATJZd+Ub9Yx2WdkTebD0ZKAM4uJTml6Rrog+ytxs1ritHMeLS9tpqgjpaOYyj5j9+3Q6U9qynKl47TtT6SEeIfvv+dSvs3L1ZzTqm1Y1s5zcZM7HTaXs+ywepolEqWmMofOAtHHm+0Mr3lTWGQZuscXl/dan19NepPpCfH10QyGo5i4jyQosnyklkzalvBZQJ2Y8GLz+MqKmjbNkCq9mJSPecUkedFG6q7b+lmeNJlSn6Yh0ffn/uTERLrhXEsfK4snDeaeJ7Ky8/GY5P6qvVDz9/WkGUnkW4wOfJJ4z+1p2Hc2W/OMnG6qD0dzrifebvscGrp4f99ZjHsrCR7Vo6+GdsdqwyT9SRtD25QNPVtE8eiTUIdZ7Ts8Xg/N0Ec/dD54/YmfUlJeC6kpS28CQ1UYt450kp8mUFcv7hJnCa/bn+xn1Z+ogkYjEpKr04jfnGPT2rt625doNtuLF5QdjRyZa7el44c8Mm4CGqf1nwGmT/8jkj7zw3l67S4Xv1tV7Y4ju8/WXT5J2FvQOg9DE/DdrcNsizp/Dd/7pBeMv+t6vgEfne94lu023KJphofq5J26XThtr2HOfqb0k3f1RKVnpyo1O7oU2b5e872e1dI5+BVfCqC/M8sm8rzpyIau7qc9tGp2+Uz2LqvbdyQMWRdZDjbtvZpH94VSNv7pd4vfyPgh4E86aXujrdEMJXfZRphZh2evwiZOX8Lm8fJ2U78XS4v2aewsSoebdlC2py87pCQks6qw3u2RZmN5XDedQzuYVFSvsuxKdeh9mrn7yCSvph96NrlwqrIoWtW0feadFC46ZFH5oGJOWRLwj9Jr1hcrR9SPFnvapscXN47+wbLsCzSbU7p90L04Jn0ZMdHH5XEv4zllSWD/wzJ6+Po/PvYu9n/9/04ziz9NMzSrJAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPgWfwFLyzCJlWZyEQAAAABJRU5ErkJggg==
 *         address:
 *          type: string
 *          required: false
 *          example: General Lopez 2567
 *         latitude:
 *          type: float
 *          example: -30.2352234
 *         longitude:
 *          type: float
 *          required: false
 *          example: -61.2352234
 *    responses:
 *     200:
 *      description: The business was updated.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 *           example: Business updated.
 */
