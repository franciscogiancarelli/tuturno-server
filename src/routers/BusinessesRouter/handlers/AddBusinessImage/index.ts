import Business from "entities/Business";
import uploadImage from "helpers/uploadImage";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { AddBusinessImageHandler } from "./types";

const BusinessRepository = getRepository(Business);

const AddBusinessImage: AddBusinessImageHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const business = await BusinessRepository.findOne({
            id: req.query.id,
            owner: { uid: auth.uid },
        });
        if (!business) throw new TTError(__("businesses.businessNotFound"));
        let imageId: number;
        if (business.images?.length) {
            const lastImageUrl = business.images[business.images?.length - 1];
            imageId =
                parseInt(lastImageUrl.split("/").slice(-1)[0].split(".")[0]) +
                1;
        } else imageId = 0;
        const imageUrl = await uploadImage(
            req.body.image,
            `business-images/${business.id}/${imageId}`
        );
        business.images = [...(business.images ?? []), imageUrl];
        await BusinessRepository.save(business);
        res.status(200).send(new TTResponse(__("businesses.businessUpdated")));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default AddBusinessImage;
