import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateAddBusinessImageRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    body("image").notEmpty().withMessage(__("businesses.invalidImage")),
    validationHandler,
];

export type AddBusinessImageQuery = {
    id: string;
};
export type AddBusinessImageBody = {
    image: string;
};

export type AddBusinessImageHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    AddBusinessImageBody,
    AddBusinessImageQuery
>;

/**
 * @openapi
 *  /api/businesses/addImage:
 *   post:
 *    summary: Add a business Image
 *    tags:
 *     - Businesses
 *    description: >
 *     You can use this endpoint to add an image to business images list
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *       description: The id of the business you want to add the image to (you must be the owner).
 *    requestBody:
 *     required: true
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         image:
 *          type: string
 *          description: url with image in base64 format
 *          required: false
 *    responses:
 *     200:
 *      description: The business was updated.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
