import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("AddBusinessImage", () => {
    beforeAll(() => {
        jest.mock("fs");
    });

    test("can add image", async () => {
        return request(testApp)
            .post(`/api/businesses/addImage?id=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                image: "data:image/webp;base64,asdfwefkjndfweDQWFWsdagasrtasdgare",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
            });
    });

    afterAll(() => {
        jest.dontMock("fs");
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
