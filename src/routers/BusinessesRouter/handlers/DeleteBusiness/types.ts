//INCLUDE DELETE BUSINESSES TYPES DECLARATION BELOW THIS COMMENT//

import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateDeleteBusinessRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type DeleteBusinessQuery = {
    id: string;
};

export type DeleteBusinessHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    DeleteBusinessQuery
>;

/**
 * @openapi
 *  /api/businesses:
 *   delete:
 *    summary: Delete Business
 *    tags:
 *     - Businesses
 *    description: >
 *     You can use this endpoint to delete a business. Keep in mind it will
 *     only flag the business as deleted, but it will remain on the database
 *     so you can restore it later. But keep in mind it might be errased in
 *     a close future if you don't restore it. Of course, this will only work
 *     if you are the owner. Otherwise it will not even find the business.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: false
 *       description: The id of the business you want to delete.
 *       schema:
 *        type: string
 *    responses:
 *     200:
 *      description: The business was deleted.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 *           example: Business safely removed.
 */
