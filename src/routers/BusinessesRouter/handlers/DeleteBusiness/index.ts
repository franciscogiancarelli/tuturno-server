import Business from "entities/Business";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { DeleteBusinessHandler } from "./types";

const BusinessRepository = getRepository(Business);

const DeleteBusiness: DeleteBusinessHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id;

        /** Find the business to delete */
        const business = await BusinessRepository.createQueryBuilder("business")
            .where("business.id = :id", { id })
            .andWhere(`business."ownerUid" = :uid`, auth)
            .leftJoinAndSelect("business.owner", "owner")
            .getOne();
        if (!business) throw __("businesses.businessNotFound");

        /** Delete the business */
        await BusinessRepository.softRemove(business);
        res.status(200).send(
            new TTResponse(__("businesses.businessSafelyRemoved"))
        );
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default DeleteBusiness;
