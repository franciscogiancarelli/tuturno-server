import Business from "entities/Business";
import { RequestHandler } from "express";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";

const BusinessRepository = getRepository(Business);

const RecoverBusiness: RequestHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id;

        /** Find the business to restore */
        const business = await BusinessRepository.createQueryBuilder("business")
            .withDeleted()
            .where("id = :id", { id })
            .andWhere(`business."ownerUid" = :uid`, auth)
            .getOne();
        if (!business) throw __("businesses.businessNotFound");

        /** Restore the business */
        await BusinessRepository.restore({
            id: business.id,
        });
        res.status(200).send(new TTResponse(__("businesses.businessRestored")));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default RecoverBusiness;
