//INCLUDE RECOVER BUSINESSES TYPES DOCUMENTATION IN THIS SECTION//

import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateRecoverBusinessRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type RecoverBusinessQuery = {
    id: string;
};

export type RecoverBusinessHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    RecoverBusinessQuery
>;

/**
 * @openapi
 *  /api/businesses:
 *   patch:
 *    summary: Restore Business
 *    tags:
 *     - Businesses
 *    description: >
 *     This endpoint allows you to restore a previously deleted business.
 *     You must be the owner and the business must be in a deleted state in order
 *     for this to work.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: false
 *       description: The id of the business you want to restore.
 *       schema:
 *        type: string
 *    responses:
 *     200:
 *      description: The business was deleted.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 *           example: Business safely removed.
 */
