//INCLUDE GET BUSINESSES TYPES DECLARATION BELOW THIS COMMENT//

import Business from "entities/Business";
import { RequestHandler } from "express";
import optionallyAuthenticate from "middlewares/optionallyAuthenticate";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateGetFeaturedBusinessRequest = [
    optionallyAuthenticate,
    validationHandler,
];

export type GetFeaturedBusinessQuery = null;

export type GetFeaturedBusinessHandler = RequestHandler<
    unknown,
    TTResponse<Business[]> | TTError,
    unknown,
    GetFeaturedBusinessQuery
>;

/**
 * @openapi
 *  /api/businesses/featuredBusinesses:
 *   get:
 *    summary: Get featured Businesses
 *    tags:
 *     - Businesses
 *    description: >
 *     You can use this endpoint to list a number of businesses to display on a  *     carrussel. If you dont specify a quantity, it will list 5.
 *    responses:
 *     200:
 *      description: Returns a paginated list of businesses.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          data:
 *           type: array
 *           items:
 *            $ref: '#/components/schemas/Business'
 */
