import initTestEntities, {
    testApp,
    testServer,
    testTestingToken,
} from "initTestEntities";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Businesses Router works well", () => {
    test("can get featured businesses", async () => {
        return request(testApp)
            .get(`/api/businesses/featuredBusinesses`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });

    test("can get fake businesses if user is testing", async () => {
        return request(testApp)
            .get(`/api/businesses/featuredBusinesses`)
            .set("Authorization", `Bearer ${testTestingToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].isFake).toEqual(true);
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
