import Business from "entities/Business";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { GetFeaturedBusinessHandler } from "./types";
import seedrandom from "seedrandom";
import User from "entities/User";
import Auth from "typedef/Auth";

const BusinessRepository = getRepository(Business);
const UserRepository = getRepository(User);
const BUSINESSES_TO_GET = 5;

const GetFeaturedBusinesses: GetFeaturedBusinessHandler = async (req, res) => {
    try {
        /** Set the seed*/
        const today = new Date();
        const seed = `${
            today.getFullYear() + (today.getMonth() + 1) + today.getDate()
        }`;

        let query = BusinessRepository.createQueryBuilder("business");

        /** Show fake businesses if user is testing */
        const authorization = req.headers.authorization;
        if (authorization) {
            const auth = JSON.parse(authorization) as Auth;
            const user = await UserRepository.findOneOrFail({ uid: auth.uid });
            if (user.isTesting) {
                query = query.andWhere("business.isFake = true");
            } else {
                query = query.andWhere("business.isFake = false");
            }
        } else {
            query = query.andWhere("business.isFake = false");
        }

        /** Count total number of businesses*/
        const totalNumberOfBusiness = await query.getCount();
        const quantity = Math.min(BUSINESSES_TO_GET, totalNumberOfBusiness);
        /**Choose a random number. Sustract quantity to ensurance being always inside the limit */
        const random = Math.floor(
            seedrandom(seed).quick() * (totalNumberOfBusiness - quantity)
        );

        query = query.take(quantity);
        query = query.skip(random);

        const selectedBusinesses = await query.getMany();

        res.status(200).send(new TTResponse<Business[]>(selectedBusinesses));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default GetFeaturedBusinesses;

/**FOR THE FUTURE: HOW TO LIST GET THE TOTAL NUMBER OF BUSINESS TYPES WITH A SUBQUERY.
     * DATE: 29/01/2022. AUTHOR: IGNACIO LUPOTTI
    // const { count: totalNumberOfBusinessTypes } = await getConnection()
    //     .createQueryBuilder()
    //     .select("COUNT(1)", "count")
    //     .from(
    //         (subQuery) =>
    //             subQuery
    //                 .select("sub_business.type")
    //                 .groupBy("sub_business.type")
    //                 .from(Business, "sub_business"),
    //         "sub_business"
    //     )
    //     .getRawOne();
    */
