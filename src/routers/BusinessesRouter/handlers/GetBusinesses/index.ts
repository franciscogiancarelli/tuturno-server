import Business from "entities/Business";
import User from "entities/User";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { GetBusinessHandler } from "./types";

const BusinessRepository = getRepository(Business);
const UserRepository = getRepository(User);
const PAGE_SIZE = 10;

const GetBusinesses: GetBusinessHandler = async (req, res) => {
    try {
        let query = BusinessRepository.createQueryBuilder("business")
            .distinct()
            .select("business.id", "id")
            .addSelect("business.name", "name")
            .addSelect("business.description", "description")
            .addSelect("business.phone", "phone")
            .addSelect("business.cover", "cover")
            .addSelect("business.address", "address")
            .addSelect("business.createdAt", "createdAt")
            .addSelect("business.updatedAt", "updatedAt")
            .addSelect("business.type", "type")
            .addSelect("business.latitude", "latitude")
            .addSelect("business.longitude", "longitude")
            .addSelect("business.images", "images")
            .addSelect(`business.ownerUid`, "ownerUid");

        /** Filter by search query */
        const search = req.query.search;
        if (search) {
            query = query.andWhere(
                `business."searchVector" @@ plainto_tsquery('spanish',:search)`,
                { search }
            );
        }

        /** Filter by similar name */
        const name = req.query.name;
        if (name) {
            query = query.andWhere("business.name ~* :name", {
                name,
            });
        }

        /** Filter by type */
        const type = req.query.type;
        if (type) {
            query = query.andWhere("business.type ~* :type", {
                type,
            });
        }

        /** Sort by coordinates */
        const latitude = Number(req.query.latitude);
        const longitude = Number(req.query.longitude);
        if (latitude && longitude) {
            query = query.addSelect(
                `
				6371*(
					2 * atan2(
						sqrt(sin(radians(:latitude::double precision - business.latitude)/2)*sin(radians(:latitude::double precision - business.latitude)/2)+cos(radians(business.latitude))*cos(radians(latitude))*sin(radians(:longitude::double precision - business.longitude)/2)*sin(radians(:longitude::double precision - business.longitude)/2)),
						sqrt(1 - sin(radians(:latitude::double precision - business.latitude)/2)*sin(radians(:latitude::double precision - business.latitude)/2)+cos(radians(business.latitude))*cos(radians(latitude))*sin(radians(:longitude::double precision - business.longitude)/2)*sin(radians(:longitude::double precision - business.longitude)/2))
					)
				)
			`,
                "distance"
            );
            query = query.setParameters({ latitude, longitude });
            query = query.orderBy("distance", "ASC");
        }

        /** Filter by ID */
        const id = req.query.id;
        if (id) {
            query = query.andWhere("business.id = :id", { id });
        }

        /** Show fake businesses if user is testing */
        const authorization = req.headers.authorization;
        if (authorization) {
            const auth = JSON.parse(authorization) as Auth;
            const user = await UserRepository.findOneOrFail({ uid: auth.uid });
            if (user.isTesting) {
                query = query.andWhere("business.isFake = true");
            } else {
                query = query.andWhere("business.isFake = false");
            }
        } else {
            query = query.andWhere("business.isFake = false");
        }

        /** Filter by owner */
        const ownerUid = req.query.ownerUid;
        if (ownerUid) {
            query = query.andWhere(`business."ownerUid" = :ownerUid`, {
                ownerUid,
            });
        }

        /** Filter by deleted state */
        const deleted = req.query.deleted;
        if (deleted === "include") {
            query = query.withDeleted();
        } else if (deleted === "only") {
            query = query
                .withDeleted()
                .andWhere("business.deletedAt is not null");
        }

        /** Paginate */
        const page = Number(req.query.page ?? 0);
        query = query.skip(page * (req.query.pageSize ?? PAGE_SIZE));
        query = query.take(req.query.pageSize ?? PAGE_SIZE);

        const businesses = await query.getRawMany();
        res.status(200).send(new TTResponse<Business[]>(businesses));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default GetBusinesses;
