import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testTestingToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const testInvalidPageNumber = -1;
const testInvalidPageSize = 51;

beforeAll(() => {
    return initTestEntities();
});

describe("Businesses Router works well", () => {
    test("can get businesses", async () => {
        return request(testApp)
            .get(`/api/businesses?id=${testEntities.business.id}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.business.id);
                expect(res.body.data[0].name).toEqual(
                    testEntities.business.name
                );
                expect(res.body.data[0].phone).toEqual(
                    testEntities.business.phone
                );
            });
    });

    test("can get businesses by name", async () => {
        return request(testApp)
            .get(
                `/api/businesses?id=${testEntities.business.id}&name=${testEntities.business.name}`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.business.id);
                expect(res.body.data[0].name).toEqual(
                    testEntities.business.name
                );
            });
    });

    test("can get businesses by type", async () => {
        return request(testApp)
            .get(
                `/api/businesses?id=${testEntities.business.id}&type=${testEntities.business.type}`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.business.id);
                expect(res.body.data[0].name).toEqual(
                    testEntities.business.name
                );
            });
    });

    test("can get businesses by coordinates", async () => {
        return request(testApp)
            .get(
                `/api/businesses?id=${testEntities.business.id}&latitude=30&longitude=30`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.business.id);
                expect(res.body.data[0].name).toEqual(
                    testEntities.business.name
                );
            });
    });

    test("can get businesses by owner", async () => {
        return request(testApp)
            .get(
                `/api/businesses?id=${testEntities.business.id}&ownerUid=${testEntities.user.uid}`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.business.id);
                expect(res.body.data[0].name).toEqual(
                    testEntities.business.name
                );
            });
    });

    test("can get businesses including deleted", async () => {
        return request(testApp)
            .get(
                `/api/businesses?id=${testEntities.business.id}&deleted=include`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(testEntities.business.id);
                expect(res.body.data[0].name).toEqual(
                    testEntities.business.name
                );
            });
    });

    test("can get only deleted businesses", async () => {
        return request(testApp)
            .get(`/api/businesses?deleted=only`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });

    test("can't get businesses by wrong deleted state", async () => {
        return request(testApp)
            .get(
                `/api/businesses?id=${testEntities.business.id}&deleted=inexistantDeletedState`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("businesses.wrongDeletedState")
                );
            });
    });

    test("can't get businesses by wrong ID type", async () => {
        return request(testApp)
            .get(`/api/businesses?id=asdf`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't get businesses if page or page size have invalid values", async () => {
        return request(testApp)
            .get(
                `/api/businesses?id=${testEntities.business.id}&page=${testInvalidPageNumber}&pageSize=${testInvalidPageSize}`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.invalidPageNumber")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("general.pageSizeTooBig")
                );
            });
    });

    test("can get fake businesses if user is testing", async () => {
        return request(testApp)
            .get(`/api/businesses?id=${testEntities.fakeBusiness.id}`)
            .set("Authorization", `Bearer ${testTestingToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.data[0].id).toEqual(
                    testEntities.fakeBusiness.id
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
