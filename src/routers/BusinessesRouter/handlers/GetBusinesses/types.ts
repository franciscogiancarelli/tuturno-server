import Business from "entities/Business";
import { RequestHandler } from "express";
import { oneOf, query } from "express-validator";
import { __ } from "locales";
import optionallyAuthenticate from "middlewares/optionallyAuthenticate";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateGetBusinessRequest = [
    optionallyAuthenticate,
    query("id").optional().isUUID().withMessage(__("general.invalidId")),
    oneOf(
        [
            query("deleted").optional().equals("include"),
            query("deleted").optional().equals("only"),
        ],
        __("businesses.wrongDeletedState")
    ),
    query("page")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("general.invalidPageNumber")),
    query("deleted").optional().isString().isIn(["include", "only"]),
    query("pageSize")
        .optional()
        .isInt({ lt: 51 })
        .withMessage(__("general.pageSizeTooBig")),
    validationHandler,
];

export type GetBusinessQuery = {
    id?: string;
    ownerUid?: string;
    search?: string;
    name?: string;
    address?: string;
    phone?: string;
    type?: string;
    latitude?: number;
    longitude?: number;
    locality?: string;
    postalCode?: string;
    country?: string;
    cover?: string;
    images?: string[];
    page?: number;
    pageSize?: number;
    deleted?: string;
};

export type GetBusinessHandler = RequestHandler<
    unknown,
    TTResponse<Business[]> | TTError,
    unknown,
    GetBusinessQuery
>;

/**
 * @openapi
 *  /api/businesses/:
 *   get:
 *    summary: List Businesses
 *    tags:
 *     - Businesses
 *    description: >
 *     You can use this endpoint to list the businesses available.
 *     It's paginated and the size of the page is 10 by default.
 *     You can specify the page to go throw the different pages.
 *     This endpoint also allows you send many different params
 *     (none of them are required) to filtered or sort the listing.
 *     So you can use this same endpoint to search. For searching though
 *     you should always use the search and latitude and longitude params,
 *     as those are the only ones optimized.
 *     Use the other only if no other option is available.
 *    parameters:
 *     - name: search
 *       in: query
 *       required: false
 *       description: Use this param to search by type, name or description.
 *       schema:
 *        type: string
 *     - name: id
 *       in: query
 *       required: false
 *       description: Use this param to search by ID.
 *       schema:
 *        type: uuid
 *     - name: name
 *       in: query
 *       required: false
 *       description: Use this param to search by name similarity.
 *       schema:
 *        type: string
 *     - name: type
 *       in: query
 *       required: false
 *       description: Use this param to search by type similarity.
 *       schema:
 *        type: string
 *     - name: latitude
 *       in: query
 *       required: false
 *       description: Use this param with longitude to sort by distance.
 *       schema:
 *        type: number
 *     - name: longitude
 *       in: query
 *       required: false
 *       description: Use this param with latitude to sort by distance.
 *       schema:
 *        type: number
 *     - name: ownerUid
 *       in: query
 *       required: false
 *       description: Use this param to search by owner.
 *       schema:
 *        type: string
 *     - name: deleted
 *       in: query
 *       required: false
 *       description: Use this param to filter by deleted state ().
 *       schema:
 *        type: string
 *        enum: [include, only]
 *     - name: page
 *       in: query
 *       required: false
 *       description: Use this param to specify the page you want to list from
 *       schema:
 *        type: integer
 *     - name: pageSize
 *       in: query
 *       required: false
 *       description: Use this param to fix the size of the page.
 *       schema:
 *        type: integer
 *    responses:
 *     200:
 *      description: Returns a paginated list of businesses.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          data:
 *           type: array
 *           items:
 *            $ref: '#/components/schemas/Business'
 */
