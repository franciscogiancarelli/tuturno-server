import Business from "entities/Business";

/**
 * This function takes information about the Business
 * and choose the best default cover for it.
 */
function getDefaultCover({ type }: Pick<Business, "name" | "type">): string {
    const normalizedType = type
        .normalize("NFD")
        .replace(/\p{Diacritic}/gu, "")
        .toLowerCase();
    switch (normalizedType) {
        case "peluqueria":
            return "https://tuturno-space.fra1.digitaloceanspaces.com/images/business-covers/peluqueria.jpg";
        case "gimnasio":
            return "https://tuturno-space.fra1.digitaloceanspaces.com/images/business-covers/gimnasio.webp";
        default:
            return "https://tuturno-space.fra1.digitaloceanspaces.com/images/business-covers/default.webp";
    }
}

export default getDefaultCover;
