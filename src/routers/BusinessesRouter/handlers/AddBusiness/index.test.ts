import faker from "faker";
import initTestEntities, {
    fakerPhoneNumberFormat,
    testApp,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const testBusinessId = faker.datatype.uuid();
const testBusinessName = "TestBusiness";
const testBusinessPhone = faker.phone.phoneNumber(fakerPhoneNumberFormat);

beforeAll(() => {
    return initTestEntities();
});

describe("Businesses Router works well", () => {
    test("can add a business", () => {
        return request(testApp)
            .post("/api/businesses")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testBusinessId,
                name: testBusinessName,
                type: "wierd",
                address: "test address",
                phone: testBusinessPhone,
                latitude: 0,
                longitude: 0,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data.id).toEqual(testBusinessId);
            });
    });

    test("can add a gym business", () => {
        return request(testApp)
            .post("/api/businesses")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: testBusinessName,
                type: "gimnasio",
                address: "test address",
                phone: testBusinessPhone,
                latitude: 0,
                longitude: 0,
            })
            .then((res) => expect(res.statusCode).toEqual(200));
    });

    test("can add a hair salon business", () => {
        return request(testApp)
            .post("/api/businesses")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                name: testBusinessName,
                type: "peluqueria",
                address: "test address",
                phone: testBusinessPhone,
                latitude: 0,
                longitude: 0,
            })
            .then((res) => expect(res.statusCode).toEqual(200));
    });

    test("can prevent anauth users from creating a business", () => {
        return request(testApp)
            .post("/api/businesses")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .send({
                id: testBusinessId,
                name: testBusinessName,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(403);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("403");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("general.unauthorized"));
            });
    });

    test("can prevent invalid auth users from creating a business", () => {
        return request(testApp)
            .post("/api/businesses")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}_invalid`)
            .send({
                id: testBusinessId,
                name: testBusinessName,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(401);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("401");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("general.invalidToken"));
            });
    });

    test("can't add a business without name", () => {
        return request(testApp)
            .post("/api/businesses")
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testBusinessId,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("businesses.missingName")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("businesses.missingAddress")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("businesses.missingType")
                );
                expect(res.body.errors[3].msg).toMatch(
                    __("businesses.missingPhone")
                );
                expect(res.body.errors[4].msg).toMatch(
                    __("businesses.missingLatitude")
                );
                expect(res.body.errors[5].msg).toMatch(
                    __("businesses.missingLongitude")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
