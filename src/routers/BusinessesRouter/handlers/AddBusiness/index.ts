import Business from "entities/Business";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import getDefaultCover from "./getDefaultCover";
import { AddBusinessHandler } from "./types";

const BusinessRepository = getRepository(Business);

const AddBusiness: AddBusinessHandler = async (req, res) => {
    try {
        const newBusinessData = req.body;
        const auth: Auth = JSON.parse(req.headers.authorization as string);

        /** Insert the new business into the database */
        const result = await BusinessRepository.createQueryBuilder()
            .insert()
            .values({
                ...newBusinessData,
                owner: { uid: auth.uid },
                cover: getDefaultCover(newBusinessData),
            })
            .returning([
                "id",
                "name",
                "address",
                "phone",
                "latitude",
                "longitude",
            ])
            .execute();

        if (result.identifiers.length === 1) {
            res.status(200).send(
                new TTResponse(result.generatedMaps[0] as Business)
            );
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default AddBusiness;
