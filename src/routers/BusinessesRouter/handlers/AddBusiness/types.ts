import Business from "entities/Business";
import { RequestHandler } from "express";
import { body } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateAddBusinessRequest = [
    authenticate,
    requireVerifiedEmail,
    body("name").notEmpty().withMessage(__("businesses.missingName")),
    body("address").notEmpty().withMessage(__("businesses.missingAddress")),
    body("type").notEmpty().withMessage(__("businesses.missingType")),
    body("phone")
        .notEmpty()
        .withMessage(__("businesses.missingPhone"))
        .bail()
        .isMobilePhone("es-AR")
        .withMessage(__("general.wrongPhone")),
    body("cover")
        .not()
        .exists()
        .withMessage(__("businesses.cantCreateWithCover")),
    body("images")
        .not()
        .exists()
        .withMessage(__("businesses.cantCreateWithImages")),
    body("latitude").isNumeric().withMessage(__("businesses.missingLatitude")),
    body("longitude")
        .isNumeric()
        .withMessage(__("businesses.missingLongitude")),
    validationHandler,
];

export type AddBusinessBody = {
    name: string;
    address: string;
    phone: string;
    type: string;
    latitude: number;
    longitude: number;
    locality?: string;
    postalCode?: string;
    country?: string;
    images?: string[];
};

export type AddBusinessHandler = RequestHandler<
    unknown,
    TTResponse<Business> | TTError,
    AddBusinessBody
>;

/**
 * @openapi
 *  /api/businesses/:
 *   post:
 *    summary: Add Business
 *    tags:
 *     - Businesses
 *    description: >
 *      This endpoint allows you to add a new business with you as it's owner.
 *      If the creation is successfull you should receive the created Business
 *      with it's generated id so you can access the business with it. Remember phone must have country area (Argentina 549) and zone area.
 *    security:
 *     - BearerAuth: []
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         name:
 *          type: string
 *          example: Barbería
 *         phone:
 *          type: number
 *          example: 5493425194789
 *         type:
 *          type: string
 *          example: Gimnasio
 *         address:
 *          type: string
 *          example: General Lopez 2567
 *         latitude:
 *          type: float
 *          example: -30.2352234
 *         longitude:
 *          type: float
 *          example: -61.2352234
 *         locality:
 *          type: string
 *          example: Rosario
 *         postalCode:
 *          type: string
 *          example: S2000
 *         country:
 *          type: string
 *          example: Argentina
 *    responses:
 *     200:
 *      description: The business was created.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           $ref: '#/components/schemas/Business'
 */
