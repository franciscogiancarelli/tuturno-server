import sgMail from "@sendgrid/mail";
import Appointment from "entities/Appointment";
import User from "entities/User";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

async function sendConfirmedAppointmentEmailToCustomer(
    event: string,
    appointment: Appointment,
    customer: User
): Promise<void> {
    /** Send email invite */
    const appointmentDate = new Date(appointment.from).toLocaleDateString();
    const appointmentTime = new Date(appointment.from).toLocaleTimeString();
    await sgMail.send({
        to: customer.email,
        subject: "Turno confirmado!",
        from: {
            name: "Tuturno",
            email: "noreply@tuturno.com.ar",
        },
        templateId: "d-689e6f2cc2ec44bca95b8d68ea57804b",
        dynamicTemplateData: {
            title: `🗓 🤓 Turno confirmado`,
            subtitle: appointment.type.name,
            subject: `Se ha confirmado tu turno en ${appointment.business.name}!`,
            description: `Ya registramos tu turno 😄 para el  ${appointmentDate} a las ${appointmentTime}. Te enviamos una invitación para que puedas agregarlo a tu calendario. ¡Esperamos que lo disfrutes! Haz clic abajo para ver el turno.`,
            language: "es-AR",
            action: {
                name: "Ver turno",
                href: `${process.env.HOST}/appointment/${appointment.id}`,
            },
        },
        attachments: [
            {
                content: Buffer.from(event).toString("base64"),
                type: "text/calendar",
                filename: "invite.ics",
                disposition: "attachment",
            },
        ],
        mailSettings: {
            sandboxMode: {
                enable: process.env.JEST_WORKER_ID !== undefined,
            },
        },
    });
}

export default sendConfirmedAppointmentEmailToCustomer;
