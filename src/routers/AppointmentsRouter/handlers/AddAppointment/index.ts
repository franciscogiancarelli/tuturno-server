import Appointment, { CancelledBy } from "entities/Appointment";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import User from "entities/User";
import Auth from "typedef/Auth";
import Timeframe from "entities/Timeframe";
import AppointmentType from "entities/AppointmentType";
import { __ } from "locales";
import { AddAppointmentHandler } from "./types";
import add from "date-fns/add/index";
import createAppointmentEvent from "helpers/createEvent";
import sendNotConfirmedAppointmentEmailToCustomer from "./sendNotConfirmedAppointmentEmailToCustomer";
import sendAppointmentEmailToBusiness from "./sendAppointmentEmailToBusiness";
import sendConfirmedAppointmentEmailToCustomer from "../helpers/sendConfirmedAppointmentEmailToCustomer";

const AppointmentRepository = getRepository(Appointment);
const AppointmentTypeRepository = getRepository(AppointmentType);
const UsersRepository = getRepository(User);
const TimeframeRepository = getRepository(Timeframe);

const AddAppointment: AddAppointmentHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        /** Get the owner */
        const owner = await UsersRepository.createQueryBuilder("user")
            .where("uid = :uid", { uid: auth.uid })
            .getOneOrFail();

        /** Find the appointmentType */
        const appointmentType =
            await AppointmentTypeRepository.createQueryBuilder(
                "appointment_type"
            )
                .where("appointment_type.id = :appointmentTypeId ", {
                    appointmentTypeId: req.query.appointmentTypeId,
                })
                .getOne();

        if (!appointmentType) throw __("appointments.appointmentTypeNotFound");

        /** Check is not overpassed the appointmentType max request for a single user */
        if (appointmentType.maxMonthlyUserRequest) {
            const monthlyUserRequest =
                await AppointmentRepository.createQueryBuilder("appointment")
                    .where("appointment.owner=:id", { id: owner.uid })
                    .andWhere(
                        "date_trunc('month',appointment.from)=date_trunc('month', :from::timestamp)",
                        { from: req.body.from }
                    )
                    .getCount();
            if (monthlyUserRequest >= appointmentType.maxMonthlyUserRequest)
                throw __("appointments.excedeedMaxUserMonthlyRequest");
        }

        if (appointmentType.maxWeeklyUserRequest) {
            const weeklyUserRequest =
                await AppointmentRepository.createQueryBuilder("appointment")
                    .where("appointment.owner=:id", { id: owner.uid })
                    .andWhere(
                        "date_trunc('week',appointment.from)=date_trunc('week', :from::timestamp)",
                        { from: req.body.from }
                    )
                    .getCount();
            if (weeklyUserRequest >= appointmentType.maxWeeklyUserRequest)
                throw __("appointments.excedeedMaxUserWeeklyRequest");
        }

        if (appointmentType.maxDailyUserRequest) {
            const dailyUserRequest =
                await AppointmentRepository.createQueryBuilder("appointment")
                    .where("appointment.owner=:id", { id: owner.uid })
                    .andWhere(
                        "date_trunc('day',appointment.from)=date_trunc('day', :from::timestamp)",
                        { from: req.body.from }
                    )
                    .getCount();
            if (dailyUserRequest >= appointmentType.maxDailyUserRequest)
                throw __("appointments.excedeedMaxUserDailyRequest");
        }
        /** Check is not overpassed the minimum time in advance to request an appointment, in minutes */
        if (appointmentType.minimumTimeInAdvance) {
            const currentTime = +new Date();
            const appointmentTime = +new Date(req.body.from);
            const minutesDiff = (appointmentTime - currentTime) / (1000 * 60);

            if (minutesDiff < appointmentType.minimumTimeInAdvance)
                throw __("appointments.excedeedMinimumTimeInAdvance");
        }

        /** Find the timeframe */
        const timeframeId = req.query.timeframeId;
        const fromDate = new Date(req.body.from);
        const from = req.body.from;
        const to = add(fromDate, {
            minutes: appointmentType.duration,
        }).toISOString();

        const timeframe = await TimeframeRepository.createQueryBuilder(
            "timeframe"
        )
            .leftJoinAndSelect("timeframe.resource", "resource")
            .leftJoinAndSelect("timeframe.business", "business")
            .leftJoinAndSelect("timeframe.owner", "owner")
            .leftJoinAndSelect("resource.appointmentTypes", "appointmentType")
            .where("timeframe.id = :id", { id: timeframeId })
            .andWhere("appointmentType.id = :appointmentTypeId", {
                appointmentTypeId: appointmentType.id,
            })
            .andWhere("timeframe.from <= :from", { from: from })
            .andWhere("(timeframe.to) >= :to", { to: to })
            .getOne();

        if (!timeframe) {
            throw __("appointments.timeframeNotFound");
        }

        /** Create the appointment */

        const appointment = AppointmentRepository.create({
            id: req.body.id,
            from: from,
            to: to,
            business: timeframe.business,
            resource: timeframe.resource,
            type: appointmentType,
            timeframe: timeframe,
            owner,
            notes: req.body.notes,
        });

        /** Check if the timeframe is not full */
        const timeframeAppointmentsInTimeFrame =
            await AppointmentRepository.createQueryBuilder("appointment")
                .where("appointment.timeframeId = :timeframeId")
                .andWhere("appointment.from < :to and appointment.to > :from", {
                    timeframeId: timeframeId,
                    from: appointment.from,
                    to: appointment.to,
                })
                .andWhere("appointment.cancelledBy= :notCancelled", {
                    notCancelled: CancelledBy.notCancelled,
                })
                .getCount();
        if (timeframeAppointmentsInTimeFrame >= timeframe.width) {
            throw __("appointments.timeframeIsFull");
        }

        /** Add confirmed property based on appointment type needConfirmation property*/
        if (appointmentType.needConfirmation) {
            appointment.confirmed = false;
        }

        /** Save the new appointment */
        const newAppointment = await AppointmentRepository.save(appointment);

        /** Send email to owner */
        if (appointment.type.needConfirmation) {
            await sendNotConfirmedAppointmentEmailToCustomer(
                owner,
                appointment
            );
        } else {
            const event = await createAppointmentEvent(
                newAppointment,
                timeframe,
                appointmentType,
                owner
            );

            await sendConfirmedAppointmentEmailToCustomer(
                /** Create ics file */
                event,
                appointment,
                owner
            );
        }

        /** Send event to the business's email */
        await sendAppointmentEmailToBusiness(
            timeframe.owner,
            timeframe.business,
            newAppointment
        );

        /** Return the newly created appointment */
        res.status(200).send(new TTResponse<Appointment>(newAppointment));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default AddAppointment;
