import sgMail from "@sendgrid/mail";
import Appointment from "entities/Appointment";
import Business from "entities/Business";
import User from "entities/User";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

async function sendAppointmentEmailToBusiness(
    owner: User,
    business: Business,
    appointment: Appointment
): Promise<void> {
    /** Send email invite */
    const appointmentNotes = appointment.notes ? appointment.notes : "ninguna";
    const appointmentDate = new Date(appointment.from).toLocaleDateString();
    const appointmentTime = new Date(appointment.from).toLocaleTimeString();
    await sgMail.send({
        to: owner.email,
        from: {
            name: "Tuturno",
            email: "noreply@tuturno.com.ar",
        },
        templateId: "d-689e6f2cc2ec44bca95b8d68ea57804b",
        dynamicTemplateData: {
            title: appointment.type.needConfirmation
                ? `Tienes un nuevo turno para ${business.name}. Quieres confirmarlo?`
                : `Tienes un nuevo turno para ${business.name}`,
            subtitle: appointment.type.name,
            subject: appointment.type.needConfirmation
                ? `Solicitaron un nuevo turno para ${business.name}!`
                : `Tienes un nuevo turno para ${business.name}!`,
            description: appointment.type.needConfirmation
                ? `
            ${appointment.owner.name} pidió un turno "${appointment.type.name}" de ${appointment.resource.name} para el ${appointmentDate} a las ${appointmentTime}. Aclaración del cliente: ${appointmentNotes}. Haz clic abajo para ver el turno y confirmarlo.`
                : `
            ${appointment.owner.name} pidió un turno "${appointment.type.name}" de ${appointment.resource.name} para el ${appointmentDate} a las ${appointmentTime}. Aclaración del cliente: ${appointmentNotes}. Haz clic abajo para ver el turno.`,
            language: "es-AR",
            action: {
                name: appointment.type.needConfirmation
                    ? "Confirmar o cancelar turno"
                    : "Ver calendario actualizado",
                href: appointment.type.needConfirmation
                    ? `${process.env.HOST}/my-businesses/${business.id}/appointments?id=${appointment.id}`
                    : `${process.env.HOST}/my-businesses/${business.id}/appointments?initialView=week`,
            },
        },
        mailSettings: {
            sandboxMode: {
                enable: process.env.JEST_WORKER_ID !== undefined,
            },
        },
    });
}

export default sendAppointmentEmailToBusiness;
