import sgMail from "@sendgrid/mail";
import Appointment from "entities/Appointment";
import User from "entities/User";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

async function sendNotConfirmedAppointmentEmailToCustomer(
    customer: User,
    appointment: Appointment
): Promise<void> {
    /** Send email invite */
    const appointmentTime = new Date(appointment.from).toLocaleTimeString();
    const appointmentDate = new Date(appointment.from).toLocaleDateString();
    await sgMail.send({
        to: customer.email,
        subject: "Turno solicitado!",
        from: {
            name: "Tuturno",
            email: "noreply@tuturno.com.ar",
        },
        dynamicTemplateData: {
            title: `🗓 🤓 Turno solicitado`,
            subtitle: appointment.type.name,
            subject: `Has solicitado un turno en ${appointment.business.name}!`,
            description: `El turno fue solicitado para el  ${appointmentDate} a las ${appointmentTime}. Está a la espera de confirmación. Recibirás un correo cuando el turno sea confirmado.`,
            language: "es-AR",
            action: {
                name: "Ver turno",
                href: `${process.env.HOST}/appointment/${appointment.id}`,
            },
        },
        templateId: "d-689e6f2cc2ec44bca95b8d68ea57804b",
        mailSettings: {
            sandboxMode: {
                enable: process.env.JEST_WORKER_ID !== undefined,
            },
        },
    });
}

export default sendNotConfirmedAppointmentEmailToCustomer;
