//INCLUIDE  ADD APPOINMENT TYPE DEFINITION BELOW THIS COMMENT//

import Appointment from "entities/Appointment";
import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateAddAppointmentRequest = [
    authenticate,
    requireVerifiedEmail,
    query("timeframeId").notEmpty().withMessage(__("general.missingId")),
    query("appointmentTypeId").notEmpty().withMessage(__("general.missingId")),
    query("timeframeId").isUUID().withMessage(__("general.invalidId")),
    query("appointmentTypeId").isUUID().withMessage(__("general.invalidId")),
    body("from").notEmpty().withMessage(__("appointments.missingFrom")),
    body("from").isISO8601().withMessage(__("general.invalidDateTimeFormat")),
    validationHandler,
];

export type AddAppointmentQuery = {
    timeframeId: string;
    appointmentTypeId: string;
};

export type AddAppointmentBody = {
    id?: string;
    from: Date;
    notes?: string;
};

export type AddAppointmentHandler = RequestHandler<
    unknown,
    TTResponse<Appointment> | TTError,
    AddAppointmentBody,
    AddAppointmentQuery
>;

/**
 * @openapi
 *  /api/appointments/:
 *   post:
 *    summary: Request an appointment
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows you to request an appointment for you
 *      on a given timeframe. It will check if there's availability
 *      on the appointment. Keep in mind: from parameter must be *      compliant with ISO8601. If no TZ is given, it will be *      interpreted as UTC
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *    - in: query
 *      name: timeframeId
 *      required: true
 *      schema:
 *       type: string
 *    - in: query
 *      name: appointmentTypeId
 *      required: true
 *      schema:
 *       type: string
 *    requestBody:
 *     required: true
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         from:
 *          type: date
 *          nullable: false
 *          example: 2020-10-10 08:00:00+00:00
 *         notes:
 *          type: string
 *          nullable: true
 *          example: "string"
 *    responses:
 *     200:
 *      description: The appointment was created.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           $ref: '#/components/schemas/Appointment'
 */
