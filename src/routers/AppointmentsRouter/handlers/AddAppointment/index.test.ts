import AppointmentType from "entities/AppointmentType";
import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection, getRepository } from "typeorm";

const fakeTimeframeId = "88309cae-c167-438e-b1ce-af2ce5346e50";
const testAppointmentId = "669d38af-33ce-4bd7-9c33-7357cb6b9dab";
const testAppointmentFrom = new Date("2033-10-10 09:00:00").toISOString();
const testExcedeedMaxUserRequestAppointmentFrom = new Date(
    "2033-10-10 14:00:00"
).toISOString();
const testAppointmentTypeId = testEntities.appointmentType.id;
const testTodayAppointmentFrom = testEntities.todayTimeframe.from;
beforeAll(() => {
    return initTestEntities();
});

describe("Appointments Router works well", () => {
    test("can add an appointment with notes", async () => {
        return request(testApp)
            .post(
                `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testAppointmentTypeId}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testAppointmentId,
                from: testAppointmentFrom,
                notes: "example note",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.data).toHaveProperty("id");
                expect(res.body.data.id).toEqual(testAppointmentId);
            });
    });

    test("can`t add more appointments than available width", async () => {
        await request(testApp)
            .post(
                `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testAppointmentTypeId}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: testAppointmentFrom,
            });
        return request(testApp)
            .post(
                `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testAppointmentTypeId}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: testAppointmentFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("appointments.timeframeIsFull")
                );
            });
    });

    test("can`t add an appointment on an inexistant timeframe", async () => {
        return request(testApp)
            .post(
                `/api/appointments?timeframeId=${fakeTimeframeId}&appointmentTypeId=${testAppointmentTypeId}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: testAppointmentFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("appointments.timeframeNotFound")
                );
            });
    });

    test("can`t add an appointment if no timeframe or appointmentType are specified.", async () => {
        return request(testApp)
            .post(`/api/appointments?`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: testAppointmentFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
                expect(res.body.errors[1].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't add an appointment if  timeframe, appointmentType or from have wrong types.", async () => {
        return request(testApp)
            .post(`/api/appointments?timeframeId=asdf&appointmentTypeId=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: "asd",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[1].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[2].msg).toMatch(
                    __("general.invalidDateTimeFormat")
                );
            });
    });

    test("can't add an appointment if body from is not specified.", async () => {
        return request(testApp)
            .post(
                `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testAppointmentTypeId}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({})
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("appointments.missingFrom")
                );
            });
    });

    test("can`t add an appointment if maxUserRequest monthly limit is overpassed", async () => {
        return request(testApp)
            .post(
                `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testEntities.maxUserRequestAppointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: testExcedeedMaxUserRequestAppointmentFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("appointments.excedeedMaxUserMonthlyRequest")
                );
            });
    });

    test("can`t add an appointment if maxUserRequest weekly limit is overpassed", async () => {
        /** Increase appointment type maximum request for a single user monthly, to test maximum weekly limit */
        const AppointmentTypeRepository = getRepository(AppointmentType);
        await AppointmentTypeRepository.update(
            { id: testEntities.maxUserRequestAppointmentType.id },
            { maxMonthlyUserRequest: 10 }
        );

        return request(testApp)
            .post(
                `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testEntities.maxUserRequestAppointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: testExcedeedMaxUserRequestAppointmentFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("appointments.excedeedMaxUserWeeklyRequest")
                );
            });
    });

    test("can`t add an appointment if maxUserRequest daily limit is overpassed", async () => {
        /** Increase appointment type maximum request for a single user weekly, to test maximum daily limit */
        const AppointmentTypeRepository = getRepository(AppointmentType);
        await AppointmentTypeRepository.update(
            { id: testEntities.maxUserRequestAppointmentType.id },
            { maxWeeklyUserRequest: 10 }
        );

        return request(testApp)
            .post(
                `/api/appointments?timeframeId=${testEntities.timeframe.id}&appointmentTypeId=${testEntities.maxUserRequestAppointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: testExcedeedMaxUserRequestAppointmentFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("appointments.excedeedMaxUserDailyRequest")
                );
            });
    });

    test("can`t add an appointment if minimum time in advance is overpassed", async () => {
        return request(testApp)
            .post(
                `/api/appointments?timeframeId=${testEntities.todayTimeframe.id}&appointmentTypeId=${testEntities.appointmentType.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                from: testTodayAppointmentFrom,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("appointments.excedeedMinimumTimeInAdvance")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
