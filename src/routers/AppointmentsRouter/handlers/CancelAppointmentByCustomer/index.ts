import Appointment, { CancelledBy } from "entities/Appointment";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import sgMail from "@sendgrid/mail";
import { getRepository } from "typeorm";
import { CancelAppointmentByCustomerHandler } from "./types";
import sendEmailCancelByCustomer from "./sendEmailCancelByCustomer";
import Business from "entities/Business";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

const AppointmentRepository = getRepository(Appointment);

const CancelAppointmentByCustomer: CancelAppointmentByCustomerHandler = async (
    req,
    res
) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id;
        const { affected } = await AppointmentRepository.createQueryBuilder(
            "appointment"
        )
            .update({ cancelledBy: CancelledBy.customer })
            .where("appointment.id = :id", { id })
            .andWhere('appointment."ownerUid" = :uid', auth)
            .execute();
        if (affected !== 1) {
            throw __("appointments.cantCancel");
        } else {
            const appointment = await AppointmentRepository.createQueryBuilder(
                "appointment"
            )
                .where("appointment.id = :id", { id: id })
                .leftJoinAndSelect("appointment.owner", "owner")
                .leftJoinAndSelect("appointment.resource", "resource")
                .leftJoinAndSelect("appointment.business", "business")
                .leftJoinAndSelect("appointment.type", "appointmentType")
                .getOneOrFail();

            const BusinessRepository = getRepository(Business);
            const business = await BusinessRepository.createQueryBuilder(
                "business"
            )
                .where("id = :businessId", {
                    businessId: appointment.business.id,
                })
                .leftJoinAndSelect("business.owner", "owner")
                .getOneOrFail();

            await sendEmailCancelByCustomer(business, appointment);

            res.send(new TTResponse(__("appointments.appointmentCancelled")));
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default CancelAppointmentByCustomer;
