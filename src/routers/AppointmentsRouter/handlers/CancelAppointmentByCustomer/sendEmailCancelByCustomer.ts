import sgMail from "@sendgrid/mail";
import Appointment from "entities/Appointment";
import Business from "entities/Business";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

async function sendEmailCancelByCustomer(
    business: Business,
    appointment: Appointment
): Promise<void> {
    const appointmentTime = new Date(appointment.from).toLocaleTimeString();
    const appointmentDate = new Date(appointment.from).toLocaleDateString();
    /** Send email invite */
    await sgMail.send({
        to: business.owner.email,
        from: {
            name: "Tuturno",
            email: "noreply@tuturno.com.ar",
        },
        templateId: "d-689e6f2cc2ec44bca95b8d68ea57804b",
        dynamicTemplateData: {
            title: `${appointment.owner.name} te canceló un turno`,
            subtitle: appointment.type.name,
            subject: "Turno cancelado",
            description: `${appointment.owner.name} ha cancelado su turno del ${appointmentDate} a las ${appointmentTime} para el turno ${appointment.type.name} con ${appointment.resource.name}, en el negocio ${business.name}.`,
            language: "es-AR",
            action: {
                name: "Ver calendario actualizado",
                href: `${process.env.HOST}/my-businesses/${business.id}/timeframes`,
            },
        },
        mailSettings: {
            sandboxMode: {
                enable: process.env.JEST_WORKER_ID !== undefined,
            },
        },
    });
}

export default sendEmailCancelByCustomer;
