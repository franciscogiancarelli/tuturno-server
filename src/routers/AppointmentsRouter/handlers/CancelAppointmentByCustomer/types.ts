//INCLUIDE  DELETE APPOINMENT TYPE DEFINITION BELOW THIS COMMENT//

import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateCancelAppointmentByCustomerRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type CancelAppointmentByCustomerQuery = {
    id: string;
};

export type CancelAppointmentByCustomerHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    CancelAppointmentByCustomerQuery
>;

/**
 * @openapi
 *  /api/appointments/cancelAppointmentByCustomer:
 *   delete:
 *    summary: Cancel an appointment
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows a customer to cancel
 *      an appointment.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *    responses:
 *     200:
 *      description: The appointment was canceled.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
