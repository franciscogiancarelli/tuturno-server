import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import Auth from "typedef/Auth";
import sgMail from "@sendgrid/mail";
import { SendAppointmentHandler } from "./types";
import { getRepository } from "typeorm";
import { __ } from "locales";
import getOrCreateUser from "./getOrCreateUser";
import Appointment, { RequestedBy } from "entities/Appointment";
import AppointmentType from "entities/AppointmentType";
import { add, parseISO } from "date-fns";
import Timeframe from "entities/Timeframe";
import sendEmailInvite from "./sendEmailInvite";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

const AppointmentRepository = getRepository(Appointment);
const TimeframeRepository = getRepository(Timeframe);
const AppointmentTypeRepository = getRepository(AppointmentType);

const SendAppointment: SendAppointmentHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);

        /** Get the appointmentType */
        const appointmentType = await AppointmentTypeRepository.findOne({
            id: req.body.appointmentTypeId,
        });
        if (!appointmentType)
            throw new TTError(__("appointments.appointmentTypeNotFound"));

        /** Get the timeframe */
        const timeframe = await TimeframeRepository.createQueryBuilder(
            "timeframe"
        )
            .leftJoinAndSelect("timeframe.resource", "resource")
            .leftJoinAndSelect("timeframe.business", "business")
            .leftJoinAndSelect("resource.appointmentTypes", "appointmentType")
            .where("timeframe.id = :id", { id: req.body.timeframeId })
            .andWhere("appointmentType.id = :appointmentTypeId", {
                appointmentTypeId: req.body.appointmentTypeId,
            })
            .andWhere(`business."ownerUid" = :uid`, { uid: auth.uid })
            .andWhere("timeframe.from <= :from", {
                from: req.body.appointmentFrom,
            })
            .andWhere("(timeframe.to) >= :to", {
                to: add(parseISO(req.body.appointmentFrom), {
                    minutes: appointmentType.duration,
                }),
            })
            .getOne();
        if (!timeframe) throw new TTError(__("appointments.timeframeNotFound"));
        const business = timeframe.business;
        const resource = timeframe.resource;

        /** Check if the timeframe is not full */
        const timeframeAppointmentsInTimeFrame =
            await AppointmentRepository.createQueryBuilder("appointment")
                .where("appointment.timeframeId = :timeframeId")
                .andWhere("appointment.from < :to and appointment.to > :from", {
                    timeframeId: req.body.timeframeId,
                    from: parseISO(req.body.appointmentFrom),
                    to: add(parseISO(req.body.appointmentFrom), {
                        minutes: appointmentType.duration,
                    }),
                })
                .getCount();
        if (timeframeAppointmentsInTimeFrame >= timeframe.width) {
            throw __("appointments.timeframeIsFull");
        }

        /** Get or create the user */
        const customer = await getOrCreateUser(
            req.body.customerEmail,
            req.body.customerName,
            req.body.customerPhone
        );

        /** Create the appointment */
        const appointmentInsertResponse = await AppointmentRepository.insert({
            from: req.body.appointmentFrom,
            to: add(parseISO(req.body.appointmentFrom), {
                minutes: appointmentType.duration,
            }),
            type: { id: appointmentType.id },
            accepted: null,
            owner: { uid: customer.uid },
            business: { id: business.id },
            timeframe: { id: timeframe.id },
            resource: { id: resource.id },
            requestedBy: RequestedBy.business,
        });

        await sendEmailInvite(
            customer,
            business,
            appointmentType,
            {
                id: appointmentInsertResponse.identifiers[0].id,
                from: parseISO(req.body.appointmentFrom),
                to: add(parseISO(req.body.appointmentFrom), {
                    minutes: appointmentType.duration,
                }),
            },
            timeframe
        );

        res.status(200).send(
            new TTResponse(__("appointments.appointmentSent"))
        );
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default SendAppointment;
