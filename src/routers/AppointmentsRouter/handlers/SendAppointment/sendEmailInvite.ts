import sgMail from "@sendgrid/mail";
import Appointment from "entities/Appointment";
import AppointmentType from "entities/AppointmentType";
import Business from "entities/Business";
import jwt from "jsonwebtoken";
import createAppointmentEvent from "helpers/createEvent";
import Timeframe from "entities/Timeframe";
import User from "entities/User";

sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);

async function sendEmailInvite(
    owner: Pick<User, "email" | "name">,
    business: Business,
    appointmentType: AppointmentType,
    appointment: Pick<Appointment, "id" | "from" | "to">,
    timeframe: Timeframe
): Promise<void> {
    const appointmentDate = new Date(appointment.from).toLocaleDateString();
    const appointmentTime = new Date(appointment.from).toLocaleTimeString();

    const token = jwt.sign(
        { id: appointment.id },
        process.env.SECRET as string
    );

    const event = await createAppointmentEvent(
        appointment,
        timeframe,
        appointmentType,
        owner
    );

    /** Send email invite */
    await sgMail.send({
        to: owner.email,
        from: {
            name: "Tuturno",
            email: "noreply@tuturno.com.ar",
        },
        templateId: "d-689e6f2cc2ec44bca95b8d68ea57804b",
        dynamicTemplateData: {
            title: `${business.name} te envió un turno`,
            subtitle: appointmentType.name,
            subject: "Invitación",
            description: `Ya registramos tu turno para ${appointmentDate} a las ${appointmentTime} , pero para confirmarlo tenes que ingresar en aceptar. En caso de que no quieras el turno, podés ignorar el mensaje.`,
            language: "es-AR",
            action: {
                name: "Aceptar el turno",
                href: `${process.env.HOST}/api/appointments/accept/${appointment.id}?token=${token}`,
            },
        },
        attachments: [
            {
                content: Buffer.from(event).toString("base64"),
                type: "application/ics",
                filename: "invite.ics",
                disposition: "attachment",
            },
        ],
        mailSettings: {
            sandboxMode: {
                enable: process.env.JEST_WORKER_ID !== undefined,
            },
        },
    });
}

export default sendEmailInvite;
