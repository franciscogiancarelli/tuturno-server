import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const testAppointmentFrom = new Date("2033-10-10 09:00:00").toISOString();
const testAppointmentFromTwo = new Date("2033-10-10 10:00:00").toISOString();
const testAppointmentFromThree = new Date("2033-10-10 11:00:00").toISOString();

beforeAll(() => {
    return initTestEntities();
});

describe("SendAppointment", () => {
    test("can send an appointment for a registered user", async () => {
        return request(testApp)
            .post(`/api/appointments/send`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentFrom: testAppointmentFrom,
                appointmentTypeId: testEntities.appointmentType.id,
                timeframeId: testEntities.timeframe.id,
                customerName: testEntities.user.name,
                customerEmail: testEntities.user.email,
                customerPhone: testEntities.user.phone,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.message).toEqual(
                    __("appointments.appointmentSent")
                );
            });
    });

    test("can send an appointment for an unregistered user", async () => {
        return request(testApp)
            .post(`/api/appointments/send`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentFrom: testAppointmentFromTwo,
                appointmentTypeId: testEntities.appointmentType.id,
                timeframeId: testEntities.timeframe.id,
                customerName: "Pedro",
                customerEmail: "noexistiasdfasdfs@email.com",
                customerPhone: "+5423423243",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
            });
    });
    test("can't send an appointment if appointment type not found", async () => {
        return request(testApp)
            .post(`/api/appointments/send`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentFrom: testAppointmentFromThree,
                appointmentTypeId: testEntities.timeframe.id,
                timeframeId: testEntities.timeframe.id,
                customerName: "Pedro",
                customerEmail: "noexistis@email.com",
                customerPhone: "+5423423243",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body.message).toEqual(
                    __("appointments.appointmentTypeNotFound")
                );
            });
    });
    test("can't send an appointment if timeframe not found", async () => {
        return request(testApp)
            .post(`/api/appointments/send`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentFrom: testAppointmentFromThree,
                appointmentTypeId: testEntities.appointmentType.id,
                timeframeId: testEntities.appointment.id,
                customerName: "Pedro",
                customerEmail: "noexistis@email.com",
                customerPhone: "+5423423243",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body.message).toEqual(
                    __("appointments.timeframeNotFound")
                );
            });
    });

    test("can't send an appointment if timeframe is full", async () => {
        return request(testApp)
            .post(`/api/appointments/send`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentFrom: testEntities.appointment.from,
                appointmentTypeId: testEntities.appointmentType.id,
                timeframeId: testEntities.timeframe.id,
                customerName: "Pedro",
                customerEmail: "noexistis@email.com",
                customerPhone: "+5423423243",
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body.message).toEqual(
                    __("appointments.timeframeIsFull")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
