import User from "entities/User";
import { getRepository } from "typeorm";

const UserRepository = getRepository(User);

async function getOrCreateUser(
    email: string,
    name: string,
    phone: string
): Promise<Pick<User, "name" | "email" | "phone" | "uid">> {
    return new Promise((res) => {
        UserRepository.findOne({
            email,
        }).then((customer) => {
            if (customer) res(customer);
            else {
                /** If the customer is not yet in the platform, we create a user for him. */
                UserRepository.insert({
                    name,
                    email,
                    phone,
                }).then(({ identifiers }) => {
                    res({ name, email, phone, uid: identifiers[0].uid });
                });
            }
        });
    });
}

export default getOrCreateUser;
