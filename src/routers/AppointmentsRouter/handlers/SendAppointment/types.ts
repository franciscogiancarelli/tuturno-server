import { RequestHandler } from "express";
import { body } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateSendAppointmentRequest = [
    authenticate,
    requireVerifiedEmail,
    body("timeframeId")
        .notEmpty()
        .withMessage(__("general.missingId"))
        .bail()
        .isUUID()
        .withMessage(__("general.invalidId")),
    body("appointmentTypeId")
        .notEmpty()
        .withMessage(__("general.missingId"))
        .bail()
        .isUUID()
        .withMessage(__("general.invalidId")),
    body("appointmentFrom")
        .notEmpty()
        .withMessage(__("appointments.missingFrom"))
        .bail()
        .isISO8601()
        .withMessage(__("general.invalidDateTimeFormat")),
    body("customerName").notEmpty().withMessage(__("me.missingName")),
    body("customerEmail").notEmpty().withMessage(__("me.missingEmail")),
    body("customerPhone").notEmpty().withMessage(__("me.missingPhone")),
    validationHandler,
];

export type SendAppointmentQuery = null;

export type SendAppointmentBody = {
    appointmentFrom: string;
    appointmentTypeId: string;
    timeframeId: string;
    customerName: string;
    customerEmail: string;
    customerPhone: string;
};

export type SendAppointmentHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    SendAppointmentBody,
    SendAppointmentQuery
>;

/**
 * @openapi
 *  /api/appointments/send:
 *   post:
 *    summary: Send an appointment
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows the business to send an appointment to a user.
 *      He must specify the user's name, email and phone, and if matched with any user
 *      currently on the platform, we will add the appointment to he's appointments, otherwise
 *      we will create the user.
 *    security:
 *     - BearerAuth: []
 *    requestBody:
 *     required: true
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         appointmentFrom:
 *          type: date
 *          nullable: false
 *         appointmentTypeId:
 *          type: string
 *          nullable: false
 *         customerName:
 *          type: string
 *          nullable: false
 *         customerEmail:
 *          type: string
 *          nullable: false
 *         customerPhone:
 *          type: string
 *          nullable: false
 *         timeframeId:
 *          type: string
 *          nullable: false
 *    responses:
 *     200:
 *      description: The appointment was sent.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
