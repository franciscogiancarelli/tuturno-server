// SAME TYPE THAN Confirm VALIDATION

import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateConfirmAppointmentRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type ConfirmAppointmentQuery = {
    id: string;
};

export type ConfirmAppointmentHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    ConfirmAppointmentQuery
>;

/**
 * @openapi
 *  /api/appointments/confirmAppointment:
 *   put:
 *    summary: Allow the owner of the business, to confirm and appointment from an appointment type that needs confirmation.
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows you to confirm
 *      an appointment. It currently modifies the confirmed atribute
 *      from the appointment table on the database.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *    responses:
 *     200:
 *      description: The appointment was confirmed.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
