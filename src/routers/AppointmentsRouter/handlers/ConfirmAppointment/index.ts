import Appointment from "entities/Appointment";
import createAppointmentEvent from "helpers/createEvent";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import sendConfirmedAppointmentEmailToCustomer from "../helpers/sendConfirmedAppointmentEmailToCustomer";
import { ConfirmAppointmentHandler } from "./types";

const AppointmentRepository = getRepository(Appointment);

const ConfirmAppointment: ConfirmAppointmentHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id;
        const appointment = await AppointmentRepository.createQueryBuilder(
            "appointment"
        )
            .where("appointment.id = :id", { id })
            .leftJoinAndSelect("appointment.business", "business")
            .leftJoinAndSelect("appointment.timeframe", "timeframe")
            .leftJoinAndSelect("appointment.type", "appointmentType")
            .leftJoinAndSelect("timeframe.business", "timeframe_business")
            .leftJoinAndSelect("appointment.owner", "owner")
            .andWhere('business."ownerUid" = :uid ', auth)
            .andWhere("appointment.confirmed = FALSE")
            .getOne();
        if (!appointment) throw __("appointments.cantConfirmAppointment");

        appointment.confirmed = true;

        /** Create ics file */
        const event = await createAppointmentEvent(
            appointment,
            appointment.timeframe,
            appointment.type,
            appointment.owner
        );

        await sendConfirmedAppointmentEmailToCustomer(
            event,
            appointment,
            appointment.owner
        );

        await AppointmentRepository.save({ ...appointment });

        const ttresponse = new TTResponse(
            __("appointments.appointmentConfirmed")
        );
        res.status(200).send(ttresponse);
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default ConfirmAppointment;
