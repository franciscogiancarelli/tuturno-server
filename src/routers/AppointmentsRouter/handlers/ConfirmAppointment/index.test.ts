import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("ConfirmAppointment", () => {
    test("can confirm an appointment", async () => {
        return request(testApp)
            .put(
                `/api/appointments/confirmAppointment?id=${testEntities.notConfirmedAppointment.id}`
            )
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toEqual(
                    __("appointments.appointmentConfirmed")
                );
            });
    });

    test("can't confirm an appointmnent of a business I don't own", async () => {
        return request(testApp)
            .put(
                `/api/appointments/confirmAppointment?id=${testEntities.notConfirmedAppointment.id}`
            )
            .set("Authorization", `Bearer ${testOtherToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toEqual(
                    __("appointments.cantConfirmAppointment")
                );
            });
    });

    test("can't confirm an appointment if no id given.", async () => {
        return request(testApp)
            .put(`/api/appointments/confirmAppointment`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't confirm an appointment if ID is not UUID type.", async () => {
        return request(testApp)
            .put(`/api/appointments/confirmAppointment?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't confirm an appointment if the appointment is already confirmed", async () => {
        return request(testApp)
            .put(
                `/api/appointments/confirmAppointment?id=${testEntities.appointment.id}`
            )
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body.message).toEqual(
                    __("appointments.cantConfirmAppointment")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
