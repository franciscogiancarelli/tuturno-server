import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import validationHandler from "middlewares/validationHandler";

export const validateAcceptAppointmentRequest = [
    query("token").isJWT().withMessage(__("general.invalidToken")),
    validationHandler,
];

export type AcceptAppointmentQuery = {
    token: string;
};

export type AcceptAppointmentBody = null;

export type AcceptAppointmentHandler = RequestHandler<
    { id: string },
    string,
    AcceptAppointmentBody,
    AcceptAppointmentQuery
>;

/**
 * @openapi
 *  /api/appointments/accept/{appointmentId}:
 *   get:
 *    summary: Accept an appointment
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows the user to report the appointment sent by email as accepted
 */
