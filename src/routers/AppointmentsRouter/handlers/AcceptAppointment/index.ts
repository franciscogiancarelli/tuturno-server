import TTError from "typedef/TTError";
import { stringedSuccessfulAcceptedAppointment } from "views/SuccessfulAcceptedAppointment";
import { stringedUnsuccessfulAcceptedAppointment } from "views/UnsuccessfulAcceptedAppointment";
import { AcceptAppointmentHandler } from "./types";
import jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import Appointment from "entities/Appointment";

const AppointmentRepository = getRepository(Appointment);

const AcceptAppointment: AcceptAppointmentHandler = async (req, res) => {
    try {
        const decodedToken = jwt.verify(
            req.query.token,
            process.env.SECRET as string
        ) as { id: string };
        if (decodedToken.id === req.params.id) {
            await AppointmentRepository.update(
                { id: decodedToken.id },
                { accepted: true }
            );
            res.status(200).send(stringedSuccessfulAcceptedAppointment);
        } else {
            res.status(400).send(stringedUnsuccessfulAcceptedAppointment);
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(
            stringedUnsuccessfulAcceptedAppointment
        );
    }
};

export default AcceptAppointment;
