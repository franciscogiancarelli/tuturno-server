import initTestEntities, {
    testApp,
    testEntities,
    testServer,
} from "initTestEntities";
import { getConnection } from "typeorm";
import request from "supertest";
import jwt from "jsonwebtoken";
import { stringedSuccessfulAcceptedAppointment } from "views/SuccessfulAcceptedAppointment";
import { stringedUnsuccessfulAcceptedAppointment } from "views/UnsuccessfulAcceptedAppointment";

beforeAll(() => {
    return initTestEntities();
});

describe("AcceptAppointment", () => {
    test("can accept an appointment", async () => {
        const token = jwt.sign(
            { id: testEntities.appointment.id },
            process.env.SECRET as string
        );
        return request(testApp)
            .get(
                `/api/appointments/accept/${testEntities.appointment.id}?token=${token}`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.text).toEqual(stringedSuccessfulAcceptedAppointment);
            });
    });

    test("can't accept an appointment if token does not match", async () => {
        const token = jwt.sign(
            { id: "wrong-id" },
            process.env.SECRET as string
        );
        return request(testApp)
            .get(
                `/api/appointments/accept/${testEntities.appointment.id}?token=${token}`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.text).toEqual(
                    stringedUnsuccessfulAcceptedAppointment
                );
            });
    });

    test("can't accept an appointment if token is wrong", async () => {
        const token = jwt.sign({ id: "wrong-id" }, "wrong-secret");
        return request(testApp)
            .get(
                `/api/appointments/accept/${testEntities.appointment.id}?token=${token}`
            )
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.text).toEqual(
                    stringedUnsuccessfulAcceptedAppointment
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
