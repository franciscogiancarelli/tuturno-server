import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateReportAbsenteeRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type ReportAbsenteeQuery = {
    id: string;
};

export type ReportAbsenteeHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    ReportAbsenteeQuery
>;

/**
 * @openapi
 *  /api/appointments/reportAbsentee:
 *   put:
 *    summary: Allow the owner of the business, to report an absentee on an appointment.
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows you to report the absentee
 *      of an appointment. It currently modifies the absentee atribute
 *      from the appointment table on the database.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *    responses:
 *     200:
 *      description: The absentee was reported.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
