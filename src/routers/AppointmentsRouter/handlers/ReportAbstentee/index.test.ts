import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Can report absentee", () => {
    test("can report an absentee", async () => {
        return request(testApp)
            .put(
                `/api/appointments/reportAbsentee?id=${testEntities.reviewMakerAppointment.id}`
            )
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toEqual(
                    __("appointments.absenteeReported")
                );
            });
    });

    test("can't report an absentee for a business I don't own", async () => {
        return request(testApp)
            .put(
                `/api/appointments/reportAbsentee?id=${testEntities.appointment.id}`
            )
            .set("Authorization", `Bearer ${testOtherToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toEqual(
                    __("appointments.cantReportAbsentee")
                );
            });
    });
    test("can't report absentee of an appointment if no id given.", async () => {
        return request(testApp)
            .put(`/api/appointments/reportAbsentee`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't delete an appointment if ID is not UUID type.", async () => {
        return request(testApp)
            .put(`/api/appointments/reportAbsentee?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
