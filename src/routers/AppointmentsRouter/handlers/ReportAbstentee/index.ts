import Appointment from "entities/Appointment";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { ReportAbsenteeHandler } from "./types";

const AppointmentRepository = getRepository(Appointment);

const ReportAbsentee: ReportAbsenteeHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id as string;
        const appointment = await AppointmentRepository.createQueryBuilder(
            "appointment"
        )
            .where("appointment.id = :id", { id })
            .andWhere("appointment.from < now()")
            .leftJoinAndSelect("appointment.business", "business")
            .andWhere('business."ownerUid" = :uid ', auth)
            .getOne();
        if (!appointment) throw __("appointments.cantReportAbsentee");
        await AppointmentRepository.save({ ...appointment, absent: true });
        const ttresponse = new TTResponse(__("appointments.absenteeReported"));
        res.status(200).send(ttresponse);
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default ReportAbsentee;
