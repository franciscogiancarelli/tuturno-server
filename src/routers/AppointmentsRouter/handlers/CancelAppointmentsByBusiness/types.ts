//INCLUIDE  DELETE APPOINMENT TYPE DEFINITION BELOW THIS COMMENT//

import { RequestHandler } from "express";
import { body } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateCancelAppointmentsByBusinessRequest = [
    authenticate,
    requireVerifiedEmail,
    body("appointmentsIds")
        .notEmpty()
        .withMessage(__("general.missingParam"))
        .bail()
        .isArray()
        .withMessage(__("general.mustBeArray")),
    body("appointmentsIds.*").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type CancelAppointmentsByBusinessBody = {
    appointmentsIds: string[];
    message?: string;
};

export type CancelAppointmentsByBusinessHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    CancelAppointmentsByBusinessBody,
    unknown
>;

/**
 * @openapi
 *  /api/appointments/cancelAppointmentsByBusiness:
 *   delete:
 *    summary: Allow business to cancel one or many appointments.
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows a business to cancel one or many appointments.
 *    security:
 *     - BearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              appointmentsIds:
 *                type: array
 *                items:
 *                  type: string
 *                  nullable: false
 *              message:
 *                type: string
 *    responses:
 *     200:
 *      description: The appointment was canceled.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
