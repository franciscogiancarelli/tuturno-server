import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
    testOtherToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Appointments Router works well", () => {
    test("can cancel an appointment, even if is sent twice", async () => {
        return request(testApp)
            .delete(`/api/appointments/cancelAppointmentsByBusiness`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentsIds: [
                    testEntities.appointment.id,
                    testEntities.appointment.id,
                ],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("appointments.appointmentCancelled")
                );
            });
    });

    test("can't cancel an appointment if i am not the owner of the business.", async () => {
        return request(testApp)
            .delete(`/api/appointments/cancelAppointmentsByBusiness`)
            .send({
                appointmentsIds: [testEntities.appointment.id],
            })
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                appointmentsIds: [testEntities.appointment.id],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(__("appointments.cantCancel"));
            });
    });

    test("can't delete an appointment if no id given.", async () => {
        return request(testApp)
            .delete(`/api/appointments/cancelAppointmentsByBusiness`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentsIds: [],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.missingParam")
                );
            });
    });

    test("can't delete an appointment if ID is not UUID type.", async () => {
        return request(testApp)
            .delete(`/api/appointments/cancelAppointmentsByBusiness?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                appointmentsIds: ["asd"],
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
