import cancelAppointmentsByBusiness from "helpers/cancelAppointmentsByBusiness";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { CancelAppointmentsByBusinessHandler } from "./types";

const CancelAppointmentsByBusiness: CancelAppointmentsByBusinessHandler =
    async (req, res) => {
        try {
            const auth: Auth = JSON.parse(req.headers.authorization as string);
            const ids = [...new Set(req.body.appointmentsIds)];
            const message = req.body.message;

            const response: TTResponse<string> =
                await cancelAppointmentsByBusiness(ids, auth, message);
            res.status(200).send(response);
        } catch (error) {
            const tterror = new TTError(error);
            res.status(500).send(tterror);
        }
    };

export default CancelAppointmentsByBusiness;
