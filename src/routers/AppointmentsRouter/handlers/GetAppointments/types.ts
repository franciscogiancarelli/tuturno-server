//INCLUIDE GET APPOINMENTS TYPE DEFINITION BELOW THIS COMMENT//

import Appointment, { RequestedBy } from "entities/Appointment";
import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

/**Posible values of cancelledBy param*/
export type CancelledByParam =
    | "include"
    | "any"
    | "onlyByCustomer"
    | "onlyByBusiness";
/**Posible values of requestedBy param*/
export type RequestedByKeys = keyof typeof RequestedBy;

export const validateGetAppointmentRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").optional().isUUID().withMessage(__("general.invalidId")),
    query("businessId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("timeframeId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("resourceId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("day")
        .optional()
        .isISO8601()
        .withMessage(__("appointments.wrongDayType")),
    query("fromClients")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("future")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("from")
        .optional()
        .isISO8601()
        .withMessage("general.invalidDateFormat"),
    query("to").optional().isISO8601().withMessage("general.invalidDateFormat"),
    query("withBusiness")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("withType")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("customerUid")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("confirmed")
        .optional()
        .isBoolean()
        .withMessage(__("general.wrongState")),
    query("requestedBy")
        .optional()
        .custom((requestedBy: RequestedByKeys) => {
            if (
                requestedBy !== RequestedBy.business &&
                requestedBy !== RequestedBy.customer
            ) {
                return false;
            }
            return true;
        })
        .withMessage(__("appointments.wrongRequestedByValue")),
    query("cancelledBy")
        .optional()
        .custom((cancelledBy: CancelledByParam) => {
            if (
                cancelledBy !== "any" &&
                cancelledBy !== "include" &&
                cancelledBy !== "onlyByBusiness" &&
                cancelledBy !== "onlyByCustomer"
            ) {
                return false;
            }
            return true;
        })
        .withMessage(__("appointments.wrongCancelledByValue")),
    query("page")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("general.invalidPageNumber")),
    query("pageSize")
        .optional()
        .isInt({ lt: 51 })
        .withMessage(__("general.pageSizeTooBig")),
    validationHandler,
];

export type GetAppointmentQuery = {
    id?: string;
    businessId?: string;
    appointmentTypeId?: string;
    timeframeId?: string;
    fromClients?: boolean;
    customerUid?: string;
    pageSize?: number;
    from?: string;
    resourceId?: string;
    to?: string;
    day: number;
    withBusiness?: boolean;
    future?: boolean;
    confirmed?: boolean;
    requestedBy?: string;
    cancelledBy?: string;
    page?: number;
    withType?: boolean;
};

export type GetAppointmentHandler = RequestHandler<
    unknown,
    TTResponse<Appointment[]> | TTError,
    unknown,
    GetAppointmentQuery
>;

/**
 * @openapi
 *  /api/appointments/:
 *   get:
 *    summary: Get appointments
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows you to list the appointments you own, and the appointments
 *      for a business you are owner of.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - in: query
 *       name: fromClients
 *       nullable: true
 *       description: show my client's appointments, or my appointments.
 *       schema:
 *        type: boolean
 *     - in: query
 *       name: resourceId
 *       nullable: true
 *       description: shows only the appointments associated with the given resource.
 *       schema:
 *        type: string
 *     - in: query
 *       name: id
 *       nullable: true
 *       description: get appointment by id.
 *       schema:
 *        type: string
 *     - in: query
 *       name: businessId
 *       nullable: true
 *       description: get appointment by business id.
 *       schema:
 *        type: string
 *     - in: query
 *       name: timeframeId
 *       nullable: true
 *       description: get appointment by timeframe id.
 *       schema:
 *        type: string
 *     - in: query
 *       name: customerUid
 *       nullable: true
 *       description: get appointment by timeframe id.
 *       schema:
 *        type: string
 *     - in: query
 *       name: day
 *       nullable: true
 *       description: get appointment by day.
 *       schema:
 *        type: string
 *     - in: query
 *       name: from
 *       nullable: true
 *       description: get appointment by from date.
 *       schema:
 *        type: string
 *     - in: query
 *       name: to
 *       nullable: true
 *       description: get appointment by to date.
 *       schema:
 *        type: string
 *     - in: query
 *       name: pageSize
 *       nullable: true
 *       description: page size requested
 *       schema:
 *        type: integer
 *     - in: query
 *       name: future
 *       nullable: true
 *       description: if true, only get future appointments
 *       schema:
 *        type: boolean
 *     - in: query
 *       name: confirmed
 *       nullable: true
 *       description: if true, only get confirmed appointments
 *       schema:
 *        type: boolean
 *     - in: query
 *       name: withAppointmentType
 *       nullable: true
 *       description: if true, attach appointment type data.
 *       schema:
 *        type: boolean
 *     - in: query
 *       name: withBusiness
 *       nullable: true
 *       description: if true, attach business data.
 *       schema:
 *        type: boolean
 *     - in: query
 *       name: requestedBy
 *       nullable: true
 *       description: Possible values are customer or business
 *       schema:
 *        type: string
 *     - in: query
 *       name: cancelledBy
 *       nullable: true
 *       description: Possible values are include, onlyByCustomer, onlyByBusiness, any
 *       schema:
 *        type: string
 *     - in: query
 *       name: page
 *       nullable: true
 *       description: paginate the results.
 *       schema:
 *        type: integer
 *     - in: query
 *       name: pageSize
 *       nullable: true
 *       description: fix the page Size.
 *       schema:
 *        type: integer
 *    responses:
 *     200:
 *      description: The appointments were list succesfuly.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          data:
 *           type: array
 *           items:
 *            $ref: '#/components/schemas/Appointment'
 */
