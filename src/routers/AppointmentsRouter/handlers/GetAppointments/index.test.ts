import { RequestedBy } from "entities/Appointment";
import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Appointments Router works well", () => {
    test("can get my appointments", async () => {
        return request(testApp)
            .get(`/api/appointments`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
            });
    });

    test("can get my appointments filtered by timeframe, day, business and with types", async () => {
        return request(testApp)
            .get(
                `/api/appointments?businessId=${testEntities.business.id}&timeframeId=${testEntities.timeframe.id}&id=${testEntities.appointment.id}&day=${testEntities.timeframe.from}&withType=true`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].id).toEqual(
                    testEntities.appointment.id
                );
            });
    });

    test("can get appointments filtering by requestedBy property", async () => {
        return request(testApp)
            .get(`/api/appointments?requestedBy=${RequestedBy.customer}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].requestedBy).toEqual(
                    RequestedBy.customer
                );
            });
    });

    test("can get appointments filtering by cancelledBy property", async () => {
        return request(testApp)
            .get(`/api/appointments?cancelledBy=include`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("data");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].cancelledBy).toBeDefined();
            });
    });

    test("can't get my appointments if ids are not UUID type", async () => {
        return request(testApp)
            .get(
                `/api/appointments?businessId=asdf
                }&timeframeId=asdf&id=asdf`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[1].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[2].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't get my appointments if query parameters have wrong types", async () => {
        return request(testApp)
            .get(
                `/api/appointments?day=asdf
                }&fromClients=asdf&future=asdf&withBusiness=asdf&withType=asdf&confirmed=asd&requestedBy=asd&cancelledBy=asd&page=-1&pageSize=51`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("appointments.wrongDayType")
                );
                expect(res.body.errors[1].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[3].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[4].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[5].msg).toMatch(
                    __("general.wrongState")
                );
                expect(res.body.errors[6].msg).toMatch(
                    __("appointments.wrongRequestedByValue")
                );
                expect(res.body.errors[7].msg).toMatch(
                    __("appointments.wrongCancelledByValue")
                );
                expect(res.body.errors[8].msg).toMatch(
                    __("general.invalidPageNumber")
                );
                expect(res.body.errors[9].msg).toMatch(
                    __("general.pageSizeTooBig")
                );
            });
    });

    test("can list appointments from and to a certain date", async () => {
        return request(testApp)
            .get(
                `/api/appointments?from=${testEntities.appointment.from}&to=${testEntities.appointment.from}&id=${testEntities.appointment.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.status).toBe(200);
                expect(res.body.data[0].id).toBe(testEntities.appointment.id);
            });
    });

    test("can list appointments by customerUid", async () => {
        return request(testApp)
            .get(`/api/appointments?customerUid=${testEntities.user.uid}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.status).toBe(200);
                expect(res.body.data[0].id).toBe(
                    testEntities.reviewMakerAppointment.id
                );
            });
    });

    test("can list my business's customer's appointments", async () => {
        return request(testApp)
            .get(
                `/api/appointments?fromClients=true&businessId=${testEntities.business.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.status).toBe(200);
                expect(res.body.data[0].owner.uid).toBe(testEntities.user.uid);
            });
    });

    test("can list my business's resource's customer's appointments", async () => {
        return request(testApp)
            .get(
                `/api/appointments?fromClients=true&resourceId=${testEntities.resource.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.status).toBe(200);
                expect(res.body.data[0].owner.uid).toBe(testEntities.user.uid);
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
