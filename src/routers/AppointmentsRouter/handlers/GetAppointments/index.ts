import Appointment, { CancelledBy } from "entities/Appointment";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { CancelledByParam, GetAppointmentHandler } from "./types";

const AppointmentRepository = getRepository(Appointment);

const PAGE_SIZE = 20;

const GetAppointments: GetAppointmentHandler = async (req, res) => {
    try {
        let query = AppointmentRepository.createQueryBuilder("appointment");
        const auth: Auth = JSON.parse(req.headers.authorization ?? `{}`);

        /** Attach the business */
        query = query.leftJoinAndSelect("appointment.business", "business");

        /** Only list appointments if request comes from appointment's owner or business's owner */
        const fromClients = req.query.fromClients;
        if (fromClients) {
            query = query
                .where(`business."ownerUid" = :uid`, auth)
                .leftJoinAndSelect("appointment.owner", "owner");
        } else {
            query = query.where(`appointment."ownerUid" = :uid`, auth);
        }

        /** Filter by customer */
        const customerUid = req.query.customerUid;
        if (customerUid)
            query = query.andWhere(`appointment."ownerUid" = :customerUid`, {
                customerUid,
            });

        /** Filter by business */
        const businessId = req.query.businessId;
        if (businessId)
            query = query.andWhere("appointment.businessId = :businessId", {
                businessId,
            });

        /** Filter by business */
        const resourceId = req.query.resourceId;
        if (resourceId)
            query = query.andWhere("appointment.resourceId = :resourceId", {
                resourceId,
            });

        /** Filter by timeframe */
        const timeframeId = req.query.timeframeId;
        if (timeframeId)
            query = query.andWhere("appointment.timeframeId = :timeframeId", {
                timeframeId,
            });

        /** Filter by id */
        const id = req.query.id;
        if (id) query = query.andWhere("appointment.id = :id", { id });

        /** Filter by confirmed */
        const confirmed = req.query.confirmed;
        if (confirmed) query = query.andWhere("appointment.confirmed = TRUE");

        /** Filter by day */
        const day = req.query.day;
        if (day)
            query = query.andWhere(
                "date_trunc('day', appointment.from) <= :day::date AND date_trunc('day', appointment.to) >= :day::date",
                { day }
            );

        /** Filter by from date */
        const from = req.query.from;
        if (from)
            query = query.andWhere(
                `date_trunc('day', appointment.to) >= :from::date`,
                { from }
            );

        /** Filter by to date */
        const to = req.query.to;
        if (to)
            query = query.andWhere(
                `date_trunc('day', appointment.from) <= :to::date`,
                { to }
            );

        /** Filter by requestedBy property*/
        const requestedBy = req.query.requestedBy;
        if (requestedBy) {
            query = query.andWhere(`appointment.requestedBy=:requestedBy`, {
                requestedBy,
            });
        }

        /** Filter by cancelledBy property*/
        const cancelledByValues: Record<CancelledByParam, CancelledBy[]> = {
            include: [
                CancelledBy.business,
                CancelledBy.customer,
                CancelledBy.notCancelled,
            ],
            any: [CancelledBy.business, CancelledBy.customer],
            onlyByCustomer: [CancelledBy.customer],
            onlyByBusiness: [CancelledBy.business],
        };

        /** Filter cancelled appointments by default */
        if (!req.query.cancelledBy) {
            query = query.andWhere(`appointment.cancelledBy=:notCancelled`, {
                notCancelled: CancelledBy.notCancelled,
            });
        } else {
            const cancelledBy: CancelledByParam = req.query
                .cancelledBy as CancelledByParam;
            query = query.andWhere(`appointment.cancelledBy IN (:...actor)`, {
                actor: cancelledByValues[cancelledBy],
            });
        }

        /** Show only future appointments */
        const future = req.query.future;
        if (future) query = query.andWhere("appointment.from > now()");

        /** Attach type data */
        const withType = req.query.withType;
        if (withType)
            query = query.leftJoinAndSelect("appointment.type", "type");

        /** Attach business */
        const withBusiness = req.query.withBusiness;
        if (withBusiness)
            query = query.leftJoinAndSelect("appointment.business", "business");

        /** Paginate */
        const page = Number(req.query.page ?? 0);
        query = query.skip(page * (req.query.pageSize ?? PAGE_SIZE));
        query = query.take(req.query.pageSize ?? PAGE_SIZE);

        /** Order by starting DateTime and filter previous appointments */
        query = query.orderBy("appointment.from");

        const appointments = await query.getMany();
        res.status(200).send(new TTResponse<Appointment[]>(appointments));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default GetAppointments;
