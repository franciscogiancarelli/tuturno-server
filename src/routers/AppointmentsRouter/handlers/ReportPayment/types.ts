import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateReportPaymentRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    body("payed").isBoolean().withMessage(__("general.wrongState")),
    validationHandler,
];

export type ReportPaymentQuery = {
    id: string;
};

export type ReportPaymentBody = {
    payed: boolean;
};

export type ReportPaymentHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    ReportPaymentBody,
    ReportPaymentQuery
>;

/**
 * @openapi
 *  /api/appointments/reportPayment:
 *   put:
 *    summary: Allow the owner of the business, to report the payment status of an appointment.
 *    tags:
 *     - Appointments
 *    description: >
 *      This endpoint allows you to report the payment status
 *      of an appointment. It currently modifies the pay atribute
 *      from the appointment table on the database.
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *    requestBody:
 *     required: true
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         payed:
 *          type: boolean
 *          nullable: false
 *    responses:
 *     200:
 *      description: The payment status was reported.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *          message:
 *           type: string
 */
