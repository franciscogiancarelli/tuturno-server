import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Can report payment", () => {
    test("can report a Payment", async () => {
        await request(testApp)
            .put(
                `/api/appointments/reportPayment?id=${testEntities.appointment.id}`
            )
            .set("Authorization", `Bearer ${testToken}`)
            .send({ payed: true })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("200");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toEqual(
                    __("appointments.paymentStatusReported")
                );
            });
        return request(testApp)
            .get(`/api/appointments?id=${testEntities.appointment.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.body.data[0].payed).toEqual(true);
            });
    });

    test("can't report a payment for a business I don't own", async () => {
        return request(testApp)
            .put(
                `/api/appointments/reportPayment?id=${testEntities.appointment.id}`
            )
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({ payed: true })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toEqual(
                    __("appointments.cantReportPayment")
                );
            });
    });

    test("can't report a payment if no id given.", async () => {
        return request(testApp)
            .put(`/api/appointments/reportPayment`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({ payed: true })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't report a payment if no payment status given.", async () => {
        return request(testApp)
            .put(
                `/api/appointments/reportPayment?id=${testEntities.appointment.id}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("general.wrongState")
                );
            });
    });

    test("can't report a payment if ID is not UUID type.", async () => {
        return request(testApp)
            .put(`/api/appointments/reportPayment?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({ payed: true })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
