import Appointment from "entities/Appointment";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { ReportPaymentHandler } from "./types";

const AppointmentRepository = getRepository(Appointment);

const ReportPayment: ReportPaymentHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id;
        const payed = req.body.payed;
        const appointment = await AppointmentRepository.createQueryBuilder(
            "appointment"
        )
            .where("appointment.id = :id", { id })
            .leftJoinAndSelect("appointment.business", "business")
            .andWhere('business."ownerUid" = :uid ', auth)
            .getOne();
        if (!appointment) throw __("appointments.cantReportPayment");
        await AppointmentRepository.save({ ...appointment, payed });
        const ttresponse = new TTResponse(
            __("appointments.paymentStatusReported")
        );
        res.status(200).send(ttresponse);
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default ReportPayment;
