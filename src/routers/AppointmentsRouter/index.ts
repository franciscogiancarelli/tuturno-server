import express from "express";
import GetAppointments from "./handlers/GetAppointments/index";
import AddAppointment from "./handlers/AddAppointment/index";
import ReportAbsentee from "./handlers/ReportAbstentee/index";
import ReportPayment from "./handlers/ReportPayment/index";
import { validateGetAppointmentRequest } from "./handlers/GetAppointments/types";
import { validateAddAppointmentRequest } from "./handlers/AddAppointment/types";
import { validateConfirmAppointmentRequest } from "./handlers/ConfirmAppointment/types";
import ConfirmAppointment from "./handlers/ConfirmAppointment";
import CancelAppointmentByCustomer from "./handlers/CancelAppointmentByCustomer";
import { validateCancelAppointmentByCustomerRequest } from "./handlers/CancelAppointmentByCustomer/types";
import { validateReportAbsenteeRequest } from "./handlers/ReportAbstentee/types";
import { validateReportPaymentRequest } from "./handlers/ReportPayment/types";
import { validateSendAppointmentRequest } from "./handlers/SendAppointment/types";
import SendAppointment from "./handlers/SendAppointment";
import { validateAcceptAppointmentRequest } from "./handlers/AcceptAppointment/types";
import AcceptAppointment from "./handlers/AcceptAppointment";
import { validateCancelAppointmentsByBusinessRequest } from "./handlers/CancelAppointmentsByBusiness/types";
import CancelAppointmentsByBusiness from "./handlers/CancelAppointmentsByBusiness";

const AppointmentsRouter = express.Router({ mergeParams: true });

AppointmentsRouter.get("/", validateGetAppointmentRequest, GetAppointments);

AppointmentsRouter.post("/", validateAddAppointmentRequest, AddAppointment);

AppointmentsRouter.delete(
    "/cancelAppointmentByCustomer",
    validateCancelAppointmentByCustomerRequest,
    CancelAppointmentByCustomer
);

AppointmentsRouter.delete(
    "/cancelAppointmentsByBusiness",
    validateCancelAppointmentsByBusinessRequest,
    CancelAppointmentsByBusiness
);

AppointmentsRouter.put(
    "/reportAbsentee",
    validateReportAbsenteeRequest,
    ReportAbsentee
);

AppointmentsRouter.put(
    "/reportPayment",
    validateReportPaymentRequest,
    ReportPayment
);

AppointmentsRouter.put(
    "/confirmAppointment",
    validateConfirmAppointmentRequest,
    ConfirmAppointment
);

AppointmentsRouter.post(
    "/send",
    validateSendAppointmentRequest,
    SendAppointment
);

AppointmentsRouter.get(
    "/accept/:id",
    validateAcceptAppointmentRequest,
    AcceptAppointment
);

export default AppointmentsRouter;
