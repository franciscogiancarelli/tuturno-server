import Business from "entities/Business";
import Review from "entities/Review";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { AddReviewHandler } from "./types";

const BusinessRepository = getRepository(Business);
const ReviewRepository = getRepository(Review);

const AddReview: AddReviewHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);

        /** Find the business */
        const businessId = req.query.businessId;
        const business = await BusinessRepository.createQueryBuilder("business")
            .where("business.id = :businessId", { businessId })
            .leftJoin("business.appointments", "appointment")
            .andWhere(`appointment."ownerUid" = :uid`, auth)
            .andWhere(`appointment.confirmed = TRUE`)
            .andWhere("appointment.from <= now()")
            .getOne();
        if (!business) throw __("reviews.businessNotFound");
        const newReviewData = req.body;

        /** Create the Review */
        const result = await ReviewRepository.createQueryBuilder()
            .insert()
            .values({
                ...newReviewData,
                owner: { uid: auth.uid },
                business: { id: business.id },
            })
            .returning(["id", "score", "description", "owner"])
            .execute();

        if (result.identifiers.length === 1) {
            res.status(200).send(
                new TTResponse(result.generatedMaps[0] as Review)
            );
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default AddReview;
