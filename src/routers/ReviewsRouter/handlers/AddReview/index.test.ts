import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
    testOtherToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

const testReviewId = "1ca2f540-785e-4ee7-a5e0-d8e0540cd9fd";
const testReviewScore = 4;
const testReviewDescription = "Test review";

beforeAll(() => {
    return initTestEntities();
});

describe("Can add reviews", () => {
    test("can add a review", async () => {
        return request(testApp)
            .post(`/api/reviews?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testReviewId,
                score: testReviewScore,
                description: testReviewDescription,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.data).toHaveProperty("id");
                expect(res.body.data.id).toEqual(testReviewId);
            });
    });

    test("can't add a review if no businessId is given", async () => {
        return request(testApp)
            .post(`/api/reviews`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testReviewId,
                description: testReviewDescription,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't add a review if businessId is not UUID type", async () => {
        return request(testApp)
            .post(`/api/reviews/?businessId=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testReviewId,
                description: testReviewDescription,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't add a review without score", async () => {
        return request(testApp)
            .post(`/api/reviews?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testReviewId,
                description: testReviewDescription,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("reviews.missingScore")
                );
            });
    });

    test("can't add a review with wrong score type", async () => {
        return request(testApp)
            .post(`/api/reviews?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .send({
                id: testReviewId,
                description: testReviewDescription,
                score: 6,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(
                    __("reviews.wrongScoreType")
                );
            });
    });

    test("can't add a review if I don't have an appointment on that business", async () => {
        return request(testApp)
            .post(`/api/reviews?businessId=${testEntities.otherBusiness.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                id: testReviewId,
                score: testReviewScore,
                description: testReviewDescription,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("reviews.businessNotFound")
                );
            });
    });

    test("can't add a review if business is deleted", async () => {
        return request(testApp)
            .post(`/api/reviews?businessId=${testEntities.deletedBusiness.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .send({
                id: testReviewId,
                score: testReviewScore,
                description: testReviewDescription,
            })
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("500");
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toMatch(
                    __("reviews.businessNotFound")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
