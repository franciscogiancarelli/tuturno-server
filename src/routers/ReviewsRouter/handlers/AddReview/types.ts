//INCLUDE ADD REVIEW DECLARATION TYPES BELOW THIS COMMENT//

import Review from "entities/Review";
import { RequestHandler } from "express";
import { body, query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateAddReviewRequest = [
    authenticate,
    requireVerifiedEmail,
    query("businessId").notEmpty().withMessage(__("general.missingId")),
    query("businessId").isUUID().withMessage(__("general.invalidId")),
    body("score").notEmpty().withMessage(__("reviews.missingScore")),
    body("score")
        .isInt({ gt: 0, lt: 6 })
        .withMessage(__("reviews.wrongScoreType")),
    validationHandler,
];

export type AddReviewQuery = {
    businessId: string;
};

export type AddReviewBody = {
    score: number;
    description?: string;
};

export type AddReviewHandler = RequestHandler<
    unknown,
    TTResponse<Review> | TTError,
    AddReviewBody,
    AddReviewQuery
>;

/**
 * @openapi
 *  /api/reviews:
 *   post:
 *    summary: Add a Review
 *    tags:
 *     - Review
 *    description:
 *     This endpoint allow you to add a review for a certain business
 *    parameters:
 *     - name: businessId
 *       in: query
 *       required: false
 *       description: Use this param to add a review for a business
 *       schema:
 *        type: string
 *    requestBody:
 *     required: true
 *     content:
 *      application/x-www-form-urlencoded:
 *       schema:
 *        type: object
 *        properties:
 *         score:
 *          type: number
 *          nullable: false
 *          example: 3
 *         description:
 *          type: string
 *          nullable: true
 *    security:
 *     - BearerAuth: []
 *    responses:
 *     200:
 *      description: Return  the added review.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          data:
 *           type: array
 */
