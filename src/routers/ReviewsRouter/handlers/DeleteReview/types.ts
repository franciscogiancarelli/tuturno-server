//INCLUDE DELETE REVIEW DECLARATION TYPES BELOW THIS COMMENT//

import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateDeleteReviewRequest = [
    authenticate,
    requireVerifiedEmail,
    query("id").notEmpty().withMessage(__("general.missingId")),
    query("id").isUUID().withMessage(__("general.invalidId")),
    validationHandler,
];

export type DeleteReviewQuery = {
    id: string;
};

export type DeleteReviewHandler = RequestHandler<
    unknown,
    TTResponse<string> | TTError,
    unknown,
    DeleteReviewQuery
>;

/**
 * @openapi
 *  /api/reviews:
 *   delete:
 *    summary: Delete Review
 *    tags:
 *     - Review
 *    description: >
 *     Use this endpoint to delete a review you made.
 *    parameters:
 *     - name: id
 *       in: query
 *       required: true
 *       schema:
 *        type: string
 *    security:
 *     - BearerAuth: []
 *    responses:
 *     200:
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          message:
 *           type: array
 *           example: Mensaje de éxito
 */
