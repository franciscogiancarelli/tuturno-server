import initTestEntities, {
    testApp,
    testEntities,
    testOtherToken,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Can delete reviews", () => {
    test("can delete a review", async () => {
        return request(testApp)
            .delete(`/api/reviews?id=${testEntities.review.id}`)
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.code).toEqual("200");
                expect(res.body.message).toEqual(__("reviews.reviewDeleted"));
            });
    });

    test("can't delete a review that's not yours", async () => {
        return request(testApp)
            .delete(`/api/reviews?id=${testEntities.review.id}`)
            .set("Authorization", `Bearer ${testOtherToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(500);
                expect(res.body.code).toEqual("500");
                expect(res.body.message).toEqual(
                    __("reviews.cantDeleteReview")
                );
            });
    });

    test("can't delete a review if  id is not given", async () => {
        return request(testApp)
            .delete(`/api/reviews`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.missingId"));
            });
    });

    test("can't delete a review if id is not UUID type", async () => {
        return request(testApp)
            .delete(`/api/reviews?id=asdf`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testOtherToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
