import Review from "entities/Review";
import { __ } from "locales";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { DeleteReviewHandler } from "./types";

const ReviewRepository = getRepository(Review);

const DeleteReview: DeleteReviewHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);
        const id = req.query.id;
        const { affected } = await ReviewRepository.createQueryBuilder("review")
            .delete()
            .where("review.id = :id", { id })
            .andWhere(`review."ownerUid" = :uid `, auth)
            .execute();
        if (affected === 1) {
            res.send(new TTResponse(__("reviews.reviewDeleted")));
        } else {
            throw __("reviews.cantDeleteReview");
        }
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default DeleteReview;
