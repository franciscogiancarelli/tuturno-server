import Review from "entities/Review";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { GetReviewHandler } from "./types";

const ReviewRepository = getRepository(Review);
const PAGE_SIZE = 20;

const GetReviews: GetReviewHandler = async (req, res) => {
    try {
        let query = ReviewRepository.createQueryBuilder("review");

        /** Filter by business */
        const businessId = req.query.businessId;
        if (businessId)
            query = query.andWhere("review.businessId = :businessId", {
                businessId,
            });

        /** Paginate */
        const page = Number(req.query.page ?? 0);
        query = query.skip(page * (req.query.pageSize ?? PAGE_SIZE));
        query = query.take(req.query.pageSize ?? PAGE_SIZE);

        const reviews = await query.getMany();
        res.status(200).send(new TTResponse<Review[]>(reviews));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(500).send(tterror);
    }
};

export default GetReviews;
