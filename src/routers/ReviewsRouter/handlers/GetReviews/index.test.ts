import initTestEntities, {
    testApp,
    testEntities,
    testServer,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Can get reviews", () => {
    test("can get the review of a business", async () => {
        return request(testApp)
            .get(`/api/reviews?businessId=${testEntities.business.id}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.code).toEqual("200");
                expect(res.body.data[0].id).toEqual(testEntities.review.id);
            });
    });

    test("can't get the review of a business if business is not UUID type ", async () => {
        return request(testApp)
            .get(`/api/reviews?businessId=asdf`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
            });
    });

    test("can't get the reviews of a business if page or pageSize have invalid values ", async () => {
        return request(testApp)
            .get(`/api/reviews?businessId=asdf&page=-1&pageSize=51`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[1].msg).toMatch(
                    __("general.invalidPageNumber")
                );
                expect(res.body.errors[2].msg).toMatch(
                    __("general.pageSizeTooBig")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
