//INCLUDE GET REVIEWS DECLARATION TYPES BELOW THIS COMMENT//

import Review from "entities/Review";
import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateGetReviewRequest = [
    query("businessId")
        .optional()
        .isUUID()
        .withMessage(__("general.invalidId")),
    query("page")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("general.invalidPageNumber")),
    query("pageSize")
        .optional()
        .isInt({ lt: 51 })
        .withMessage(__("general.pageSizeTooBig")),
    validationHandler,
];

export type GetReviewQuery = {
    businessId?: string;
    page?: number;
    pageSize?: number;
};

export type GetReviewHandler = RequestHandler<
    unknown,
    TTResponse<Review[]> | TTError,
    unknown,
    GetReviewQuery
>;

/**
 * @openapi
 *  /api/reviews:
 *   get:
 *    summary: List Reviews from a Business
 *    tags:
 *     - Review
 *    description: >
 *     this endpoints allow you the list all the reviews. If specificated, from a certain business
 *    parameters:
 *     - name: businessId
 *       in: query
 *       required: false
 *       description: Use this param to get reviews by business
 *       schema:
 *        type: string
 *     - name: page
 *       in: query
 *       required: false
 *       description: Use for pagination
 *     - name: pageSize
 *       in: query
 *       required: false
 *       description: Use for pagination
 *       schema:
 *        type: integer
 *    responses:
 *     200:
 *      description: Returns a paginated list of reviews.
 *      content:
 *       application/json:
 *        schema:
 *         type: object
 *         properties:
 *          code:
 *           type: string
 *           example: 200
 *          data:
 *           type: array
 */
