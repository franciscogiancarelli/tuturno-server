import express from "express";
import GetReviews from "./handlers/GetReviews";
import DeleteReview from "./handlers/DeleteReview";
import AddReview from "./handlers/AddReview";
import { validateGetReviewRequest } from "./handlers/GetReviews/types";
import { validateAddReviewRequest } from "./handlers/AddReview/types";
import { validateDeleteReviewRequest } from "./handlers/DeleteReview/types";

const ReviewsRouter = express.Router({ mergeParams: true });

ReviewsRouter.get("/", validateGetReviewRequest, GetReviews);

ReviewsRouter.post("/", validateAddReviewRequest, AddReview);

ReviewsRouter.delete("/", validateDeleteReviewRequest, DeleteReview);

export default ReviewsRouter;
