import express from "express";
import GetBusinessUsers from "./handlers/GetBusinessUsers/index";
import { validateGetBusinessUsersRequest } from "./handlers/GetBusinessUsers/types";

const UsersRouter = express.Router();

UsersRouter.get("/", validateGetBusinessUsersRequest, GetBusinessUsers);

export default UsersRouter;
