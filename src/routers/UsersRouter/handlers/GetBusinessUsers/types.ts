import User from "entities/User";
import { RequestHandler } from "express";
import { query } from "express-validator";
import { __ } from "locales";
import authenticate from "middlewares/authenticate";
import requireVerifiedEmail from "middlewares/requireVerifiedEmail";
import validationHandler from "middlewares/validationHandler";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";

export const validateGetBusinessUsersRequest = [
    authenticate,
    requireVerifiedEmail,
    query("businessId").notEmpty().withMessage(__("general.missingId")),
    query("businessId").isUUID().withMessage(__("general.invalidId")),
    query("search").optional(),
    query("uid").optional().isUUID().withMessage(__("general.invalidId")),
    query("page")
        .optional()
        .isInt({ gt: -1 })
        .withMessage(__("general.invalidPageNumber")),
    query("pageSize")
        .optional()
        .isInt({ lt: 51 })
        .withMessage(__("general.pageSizeTooBig")),
    validationHandler,
];

export type GetBusinessUsersQuery = {
    businessId: string;
    search?: string;
    page?: number;
    pageSize?: number;
    uid?: string;
};

export type GetBusinessUsersHandler = RequestHandler<
    unknown,
    TTResponse<User[]> | TTError,
    null,
    GetBusinessUsersQuery
>;

/**
 * @openapi
 *  /api/users/:
 *   get:
 *    summary: List Users From a Business
 *    tags:
 *     - Users
 *    description: Endpoint to list users from a business
 *    security:
 *     - BearerAuth: []
 *    parameters:
 *    - in: query
 *      name: businessId
 *      description: The business you want to list the users from. You must be the owner.
 *      required: true
 *    - in: query
 *      name: search
 *      required: false
 *    - in: query
 *      name: uid
 *      required: false
 *    - in: query
 *      name: page
 *      type: number
 *      required: false
 *    - in: query
 *      name: pageSize
 *      type: number
 *      required: false
 *    responses:
 *     200:
 *      description: List of the users of your business.
 *      content:
 *       application/json:
 *        schema:
 *         type: array
 *         items:
 *          $ref: '#/components/schemas/User'
 */
