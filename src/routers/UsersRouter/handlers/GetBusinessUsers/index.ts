import User from "entities/User";
import Auth from "typedef/Auth";
import TTError from "typedef/TTError";
import TTResponse from "typedef/TTResponse";
import { getRepository } from "typeorm";
import { GetBusinessUsersHandler } from "./types";

const UserRepository = getRepository(User);
const PAGE_SIZE = 10;

const GetBusinessUsers: GetBusinessUsersHandler = async (req, res) => {
    try {
        const auth: Auth = JSON.parse(req.headers.authorization as string);

        let query = UserRepository.createQueryBuilder("user")
            .leftJoin("user.appointments", "appointment")
            .leftJoin("appointment.business", "business")
            .andWhere("business.id = :businessId", {
                businessId: req.query.businessId,
            })
            .andWhere(`business."ownerUid" = :uid`, auth);

        const search = req.query.search;
        if (search)
            query = query.andWhere(
                "(user.name ~* :search or user.email ~* :search)",
                { search }
            );

        const uid = req.query.uid;
        if (uid)
            query = query.andWhere(`user.uid = :customerUid`, {
                customerUid: uid,
            });
        /** Paginate */
        const page = Number(req.query.page ?? 0);
        query = query.skip(page * (req.query.pageSize ?? PAGE_SIZE));
        query = query.take(req.query.pageSize ?? PAGE_SIZE);

        const users = await query.getMany();

        res.send(new TTResponse<User[]>(users));
    } catch (error) {
        const tterror = new TTError(error);
        res.status(tterror.status).send(tterror);
    }
};

export default GetBusinessUsers;
