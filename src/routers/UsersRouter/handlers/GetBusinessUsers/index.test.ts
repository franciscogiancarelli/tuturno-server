import initTestEntities, {
    testApp,
    testEntities,
    testServer,
    testToken,
} from "initTestEntities";
import { __ } from "locales";
import request from "supertest";
import { getConnection } from "typeorm";

beforeAll(() => {
    return initTestEntities();
});

describe("Users Router works well", () => {
    test("can get business users", async () => {
        return request(testApp)
            .get(`/api/users?businessId=${testEntities.business.id}`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data[0].email).toEqual(testEntities.user.email);
            });
    });

    test("can get users by page", async () => {
        return request(testApp)
            .get(`/api/users?businessId=${testEntities.business.id}&page=1`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data.length).toEqual(0);
            });
    });

    test("can get users by uid", async () => {
        return request(testApp)
            .get(
                `/api/users?businessId=${testEntities.business.id}&uid=${testEntities.user.uid}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.data[0].uid).toEqual(testEntities.user.uid);
            });
    });

    test("can search for customers", async () => {
        await request(testApp)
            .get(
                `/api/users?businessId=${testEntities.business.id}&search=${testEntities.user.email}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(200);
                expect(res.body).toHaveProperty("code");
                expect(res.body.data).toBeInstanceOf(Array);
                expect(res.body.data.length).toEqual(1);
                expect(res.body.data[0].uid).toEqual(testEntities.user.uid);
            });
        await request(testApp)
            .get(
                `/api/users?businessId=${testEntities.business.id}&search=${testEntities.user.name}`
            )
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.body.data.length).toEqual(1);
                expect(res.body.data[0].uid).toEqual(testEntities.user.uid);
            });
    });

    test("cannot get users if parameters have wrong type", async () => {
        return request(testApp)
            .get(`/api/users?businessId=asdf&uid=asdf&page=-1&pageSize=51`)
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Authorization", `Bearer ${testToken}`)
            .then((res) => {
                expect(res.statusCode).toEqual(400);
                expect(res.body).toHaveProperty("code");
                expect(res.body.code).toEqual("400");
                expect(res.body).toHaveProperty("message");
                expect(res.body).toHaveProperty("errors");
                expect(res.body.errors[0].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[1].msg).toMatch(__("general.invalidId"));
                expect(res.body.errors[2].msg).toMatch(
                    __("general.invalidPageNumber")
                );
                expect(res.body.errors[3].msg).toMatch(
                    __("general.pageSizeTooBig")
                );
            });
    });
});

afterAll(() => {
    return getConnection()
        .close()
        .then(async () => {
            testServer.close();
        });
});
