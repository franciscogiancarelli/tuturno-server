import LocaleTranslation from "typedef/LocaleTranslation";

const translation: LocaleTranslation = {
    businesses: {
        wrongDeletedState: "Deleted debe ser 'include' o 'only'.",
        missingAddress: "Falta address.",
        missingPhone: "Falta phone",
        missingLatitude: "Falta latitude.",
        missingType: "Falta el tipo.",
        invalidImage: "Imagen inválida.",
        missingImageUrl: "Falta la url de la imagen.",
        missingLongitude: "Falta longitude.",
        cantCreateWithImages: "No podes crear un local con imagenes.",
        cantCreateWithCover: "No podes crear un local con una imagen.",
        missingName: "Falta name.",
        businessNotFound: "No encontramos el lugar.",
        businessSafelyRemoved: "Lugar eliminado de forma segura",
        cantUpdateId: "No se puede cambiar el id.",
        cantUpdateCreatedAt: "No se puede cambiar la fecha de creación.",
        cantUpdateDeletedAt: "No se puede cambiar la fecha de eliminación.",
        businessRestored: "Lugar restaurado.",
        businessUpdated: "Lugar actualizado.",
    },
    general: {
        missingId: "Falta ID.",
        missingParam: "Falta Param.",
        mustBeArray: "Param debe ser un array.",
        emailFailed: "Tuvimos un problema enviando el correo.",
        unsuportedImageType:
            "El tipo de imagen no es soportado. Debe ser de tipo jpg, jpeg or png.",
        invalidId: "ID inválido",
        unauthorized: "No autorizado.",
        emailNotConfirmed:
            "Necesitas verificar el email para acceder a esta página.",
        pageSizeTooBig: "La el tamaño de la página es demasiado grande.",
        wrongState: "Debe ser booleano true o false.",
        wrongPhone:
            "El número de celular registrado no tiene formato correcto. Chequea que tenga código de país y de area.",
        cantChangeId: "Cambiar el id no está permitido.",
        invalidDateTimeFormat:
            "El formato de fecha y hora debe ser compatible con ISO 8601.",
        invalidDateFormat: "El formato de fecha debe ser YYYY-MM-DD",
        invalidToken: "Invalid Token.",
        validationError:
            "Algunos de los parámetros enviados son incorrectos o están faltando.",
        userNotFound: "Usuario no encontrado.",
        unknownError: "Tuvimos un problema desconocido.",
        invalidPageNumber: "El número de páginas debe ser un entero mayor a 0.",
    },
    me: {
        emailVerificationSuccessDescription:
            "Ya verificamos tu correo. Ahora vas a poder pedir turnos en cualquier local de la plataforma.",
        emailVerificationSuccessTitle: "🎉 ¡Felicidades!",
        verificationEmailSent:
            "Ya te enviamos un email para que puedas verificar tu correo.",
        emailVerificationSuccessButton: "Buscar my primer turno",
        emailVerificationSuccessHelpDescription: "¿Necesitas ayuda?",
        emailVerificationSuccessHelpTitle: "Comunicate con nostros a",
        emailNotVerified:
            "Tu correó aún no fue verificado. Por favor, te hemos vuelto a enviar un correo de verificación de cuenta para que puedas ingresar.",
        userNotFound:
            "Usuario no encontrado. Chequea que el id enviado sea correcto",
        missingEmail: "Falta email.",
        wrongEmailType: "Email no tiene formato de email.",
        accountNotFullyRegistered:
            "Parece que la cuenta con la que estas intentando ingresar no terminó de ser registrado. Por favor, revisá tu correo para registrar una nueva contraseña y terminar el registro.",
        missingPassword: "Falta password.",
        emailNotFound:
            "Email no encontrado. Chequea que el email enviado sea correcto.",
        incorrectCredentials: "Datos de acceso incorrectos.",
        wrongPassword: "La contraseña es incorrecta.",
        notStrongPassword:
            "La contraseña debe tener al menos 8 caracteres y debe contener al menos un número.",
        missingName: "Falta name",
        missingPhone: "Falta phone.",
        userRegistered:
            "Usuario registrado correctamente. Por favor chequea tu email para verificar tu cuenta.",
        registerFail: "El usuario no pudo ser registrado",
        emailTaken: "Parece que el email ya fue tomado",
        recoveryEmailSent: "Email de recuperación enviado.",
        missingToken: "Falta token.",
        wrongToken: "Token incorrecto.",
        wrongTokenType: "El token enviado no es de tipo JWT",
        passwordUpdated: "Contraseña del usuario modificada",
        cantChangeId: "No puedes cambiar tu id de usuario.",
        cantChangePhone: "No puedes cambiar tu número",
        cantChangeEmail: "No puedes cambiar tu email",
        cantChangePasswordWithoutCurrentPassword:
            "Necesitamos la contraseña actual para cambiarla.",
        dataUpdated: "Datos de usuario actualizados exitosamente",
    },
    resources: {
        missingName: "Falta name.",
        missingInterval: "Falta interval",
        wrongIntervalType: "Debe ser un entero positivo divisible por cinco",
        resourceDeleted: "Recurso eliminado correctamente.",
        resourceNotFound:
            "No pudimos encontrar el recurso. Chequea que el id pertenece a un recurso, y que eres el dueño.",
        cantUpdateResource:
            "El recurso no pudo ser actualizado. Chequea que eres el dueño. Y que el id enviado es válido.",
        mustProvideData: "Al menos name o description debe ser enviado.",
        resourceUpdated: "Recurso actualizado.",
        appointmentTypeRelated: "El tipo de turno fue añadido al recurso",
        cantRelateResource: "El recurso no existe, o no eres el dueño.",
        appointmentTypeDoesntExist: "El tipo de turno no existe.",
    },
    appointments: {
        missingFrom: "Falta turno from.",
        appointmentTypeNotFound: "No se encontró appointmentType",
        appointmentSent: "Turno enviado.",
        timeframeNotFound:
            "El timeframe enviado no existe. O no es compatible. O el fin del turno es mas largo que el timeframe.",
        timeframeIsFull:
            "Parece que el timeframe está lleno en ese horario. Por favor elija un horario distinto",
        appointmentCancelled: "Turno cancelado/s correctamente.",
        cantCancel:
            "No pudimos cancelar el/los turnos. Chequea que el/los ids enviados pertenezcan a un turno del cual seas dueño.",
        wrongDayType: "Formato de día incorrecto.",
        wrongRequestedByValue: "Los valores posibles son customer o business.",
        wrongCancelledByValue:
            "Los valores posibles son customer, business, notCancelled.",
        cantReportAbsentee:
            "Turno no encontrado. Chequea que la fecha del turno haya pasado, y que eres el dueño del negocio",
        absenteeReported: "Ausencia reportada.",
        cantReportPayment:
            "Turno no encontrado. Chequea que eres el dueño del negocio.",
        paymentStatusReported: "Estado del pago reportado.",
        cantConfirmAppointment:
            "Turno no encontrado. Chequeá que seas el dueño del negocio y que el turno todavía no esté confirmado.",
        appointmentConfirmed: "Turno confirmado exitosamente.",
        excedeedMaxUserDailyRequest:
            "Excediste el límite diario de máximos turnos para este tipo de turno.",
        excedeedMaxUserWeeklyRequest:
            "Excediste el límite semanal de máximos turnos para este tipo de turno.",
        excedeedMaxUserMonthlyRequest:
            "Excediste el límite mensual de máximos turnos para este tipo de turno.",
        excedeedMinimumTimeInAdvance:
            "Es muy tarde para pedir este turno. Tiene que solicitar con anticipación",
    },
    appointmentTypes: {
        missingName: "Falta name en tipo de turno.",
        missingDescription: "Falta description en tipo de turno.",
        missingDuration: "Falta duration en tipo de turno.",
        missingCurrency: "Falta currency en tipo de turno.",
        missingNeedConfirmation: "Falta needConfirmation en tipo de turno.",
        wrongCurrencyType: "Este no es un valor de currency permitido",
        wrongDurationType: "Debe ser un entero positivo divisible por cinco",
        missingPrice: "Falta precio en tipo de turno .",
        appointmentTypeNotFound:
            "No pudimos eliminar el tipo de turno. Chequea que el id se corresponda a un tipo de turno.",
        cantUpdateAppointmentType:
            "El tipo de turno no existe, o no le pertenece a tu negocio",
        mustProvideData:
            "Al menos name, description, duration, price, currency o needConfirmation debe ser enviado.",
        appointmentTypeUpdated: "Tipo de turno actualizado.",
        wrongPriceType: "Esto no es un valor válido de price",
        wrongMaxUserRequestType: "Debe ser un entero mayor a 0.",
        wrongMinimumTimeInAdvance:
            "Debe ser cero o un entero positivo. Recuerde que el tiempo está en minutos.",
    },
    reviews: {
        businessNotFound:
            "No pudimos encontrar el business. Chequea el businessId. O el usuario no tiene turnos para el negocio",
        wrongScoreType: "Debe ser un entero entre uno y cinco",
        missingScore: "Falta score en la reseña",
        reviewDeleted: "Reseña eliminada correctamente",
        cantDeleteReview:
            "No pudimos encontrar el business. Chequea el businessId.  Y que eres el dueño.",
    },
    timeframes: {
        missingFrom: "Falta from",
        missingTo: "Falta to",
        missingWidth: "Falta width",
        wrongWidthType: "Debe ser un entero mayor a 0",
        timeframeNotFound:
            "No pudimos encontrar el timeframe. Chequea que el id enviado pertenezca a un timeframe. Y que el timeframe sea tuyo.",
        timeframesDeleted: "Timeframe eliminado exitosamente.",
        mustProvideData: "Al menos from, to o width debe ser enviado.",
        massiveMustProvideData:
            "Al menos dateShift,hsFrom, hsTo, minFrom,minTo o width deben ser enviados.",
        timeframesUpdated: "Timeframe actualizado exitosamente.",
        haveActiveAppointments:
            "Al menos uno de los horarios que quieres actualizar tiene turnos activos. Si la actualización afecta el/los turnos, los cancelaremos. Estás seguro de actualizar?",
        haveActiveAppointmentsOnDelete:
            "Al menos uno de los horarios que quieres eliminar tiene turnos activos. Si procede, cancelaremos los turnos. Está seguro de eliminar?",
        mustSendEndRecurrency:
            "Estás añadiendo timeframes recurrentes, por lo que debes enviar fecha endRecurrency",
    },
};

export default { translation };
