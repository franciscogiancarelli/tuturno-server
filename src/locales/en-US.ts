import LocaleTranslation from "typedef/LocaleTranslation";

const translation: LocaleTranslation = {
    businesses: {
        wrongDeletedState: "Deleted must be 'include' or 'only'.",
        missingAddress: "Missing Address.",
        missingPhone: "Missing Phone",
        missingLatitude: "Missing Latitude.",
        missingLongitude: "Missing Longitude.",
        invalidImage: "Invalid image.",
        missingName: "Missing Name.",
        missingImageUrl: "Missing image url.",
        cantCreateWithImages: "Can't create a business with images.",
        missingType: "Missing Type.",
        cantCreateWithCover: "Can't create a business with a cover.",
        businessNotFound: "The business could not be found.",
        businessSafelyRemoved: "Business Safely Removed",
        cantUpdateId: "Can't update the business's id.",
        cantUpdateCreatedAt: "Can't set the createdAt.",
        cantUpdateDeletedAt: "Can't set the deletedAt.",
        businessRestored: "Business restored.",
        businessUpdated: "Business updated.",
    },
    general: {
        missingId: "Invalid ID.",
        missingParam: "Missing Param.",
        mustBeArray: "Param must be an array.",
        invalidId: "Invalid Id",
        unauthorized: "Unauthorized.",
        emailNotConfirmed: "You must verify your email to access this page.",
        wrongState: "Must be true or false.",
        wrongPhone: "The phone number has not the correct phone number format",
        cantChangeId: "Change the id is not allowed.",
        pageSizeTooBig: "The page size is too big.",
        emailFailed: "We had a problem sending the email.",
        unsuportedImageType:
            "The image type is not supported. It must be of type jpg, jpeg or png.",
        invalidDateTimeFormat:
            "DateTime format must be compliant with ISO 8601.",
        invalidDateFormat: "Date format must be YYYY-MM-DD",
        invalidToken: "Invalid Token.",
        validationError: "Some parameters are incorrect or missing.",
        userNotFound: "User not found.",
        unknownError: "We had an unknown problem.",
        invalidPageNumber: "Page number must be an integer greater than 0.",
    },
    me: {
        emailVerificationSuccessDescription:
            "Your email has been verified. You will now be able to request an appointment for any place in the platform.",
        emailVerificationSuccessTitle: "🎉 Congrats!",
        emailNotVerified: "Your email is not verified.",
        accountNotFullyRegistered:
            "It seems the account you are trying to use has not a password registered. Please check your email to register a new password for your account.",
        wrongPassword: "Wrong password",
        emailVerificationSuccessButton: "Book my first appointment",
        emailVerificationSuccessHelpDescription: "Need help?",
        emailVerificationSuccessHelpTitle: "Contact us at",
        userNotFound: "User not found. Check that user id is correct.",
        missingEmail: "Missing email.",
        wrongEmailType: "Email has not email format.",
        missingPassword: "Missing password.",
        notStrongPassword:
            "Password must be at least 8 characters long, and must contain at least one number.",
        emailNotFound: "Email not found. Check that email is correct.",
        incorrectCredentials: "Incorrect credentials.",
        missingName: "Missing name",
        missingPhone: "Missing phone.",
        verificationEmailSent: "The verification email was sent to your email.",
        userRegistered:
            "User registered successfuly. Please check your email to verify your account.",
        registerFail: "User could not be registered.",
        emailTaken: "It seems the email is already taken.",
        recoveryEmailSent: "Recovery email sent.",
        missingToken: "Missing token.",
        wrongToken: "Wrong token.",
        wrongTokenType: "Token sent is not JWT type",
        passwordUpdated: "User password updated.",
        cantChangeId: "You can't modify your user id.",
        cantChangePhone: "You can't change your phone",
        cantChangeEmail: "You can't change your email",
        cantChangePasswordWithoutCurrentPassword:
            "Must send current password to change it.",
        dataUpdated: "User's data updated succesfully.",
    },
    resources: {
        missingName: "Missing resource name",
        missingInterval: "Missing resource interval",
        wrongIntervalType: "Must be a positive integer divisible by five",
        resourceDeleted: "Resource deleted successfully",
        resourceNotFound:
            "We couldn't find the resource. Check if the given id belongs to a resource. And that you are the owner.",
        cantUpdateResource:
            "The resource could not be updated. Check that your the resource's owner. And that the provided id is valid.",
        mustProvideData:
            "At least one of name, description or interval must be provided.",
        resourceUpdated: "Resource updated.",
        appointmentTypeRelated:
            "The appointmentType was added to the resource.",
        cantRelateResource:
            "The resource doesn't exists, or you are not the owner.",
        appointmentTypeDoesntExist: "The appointment type doesn't exists",
    },
    appointments: {
        missingFrom: "Missing appointment from.",
        appointmentTypeNotFound: "AppointmentType not found.",
        timeframeNotFound:
            "The given timeframe doesn't exists. Or is not compatible. Or the appointment end date is larger than the timeframe.",
        timeframeIsFull:
            "It seems the timeframe is full on that time. Please choose a different one.",
        appointmentCancelled: "Appointment/s cancelled successfully.",
        cantCancel:
            "We couldn't cancel the Appointment. Check if the given id/s belongs to appointment/s you own.",
        appointmentSent: "Appointment sent.",
        wrongDayType: "Wrong day type",
        cantReportAbsentee:
            "Appointment not found. Check that the appointment from date has passed, you are the owner of the business.",
        absenteeReported: "Absentee reported.",
        cantReportPayment:
            "Appointment not found. Check that you are the owner of the business.",
        paymentStatusReported: "Payment status reported.",
        cantConfirmAppointment:
            "Appointment not found. Check you are the owner of the business and that the appointment is not already confirmed.",
        appointmentConfirmed: "Appointment succesfully confirmed",
        excedeedMaxUserDailyRequest:
            "You have exceeded the daily limit of maximum appointments for this appointment type.",
        excedeedMaxUserWeeklyRequest:
            "You have exceeded the weekly limit of maximum appointments for this week.",
        excedeedMaxUserMonthlyRequest:
            "You have exceeded the monthly limit of maximum appointments for this month.",
        wrongRequestedByValue: "Possible values are customer or business.",
        wrongCancelledByValue:
            "Possible values are customer, business, notCancelled.",
        excedeedMinimumTimeInAdvance:
            "It's too late to request this appointment. You have to request in advance",
    },
    appointmentTypes: {
        missingName: "Missing appointmentType name",
        missingDescription: "Missing appointmentType description.",
        missingDuration: "Missing appointmentType duration",
        missingCurrency: "Missing appointmentType currency",
        missingNeedConfirmation: "Missing appointment type needConfirmation",
        wrongCurrencyType: "This is not an allowed currency value",
        wrongDurationType: "Must be a positive integer divisible by five",
        missingPrice: "Missing appointmentType price.",
        appointmentTypeNotFound:
            "We couldn't delete the Appointment Type. Check if the given id belongs to an Appointment Type.",
        cantUpdateAppointmentType:
            "The given appointmentType doesn't exists, or is not yours.",
        mustProvideData:
            "At least one of name, description, duration, price, currency or needConfirmation must be provided.",
        appointmentTypeUpdated: "Appointment Type updated.",
        wrongPriceType: "This is not a valid price value",
        wrongMaxUserRequestType: "Must be an integer greater than 0.",
        wrongMinimumTimeInAdvance:
            "Must be cero or a positive integer. Remember that time is in minutes.",
    },
    reviews: {
        businessNotFound:
            "We couldn't find the business. Check businessId. Or user dont have appointments for the business.",
        wrongScoreType: "Must be an integer between one and five",
        missingScore: "Missing review score",
        reviewDeleted: "Review deleted successfully",
        cantDeleteReview:
            "We couldn't delete the review. Check if the given id belongs to a review. And that you are the owner.",
    },
    timeframes: {
        missingFrom: "Missing from",
        missingTo: "Missing to",
        missingWidth: "Missing width",
        wrongWidthType: "Must be integer greater than 0",
        timeframeNotFound:
            "We couldn't find the timeframe. Check if the given id belongs to a timeframe. And that timeframe is yours.",
        timeframesDeleted: "Timeframe deleted successfully.",
        mustProvideData: "At least, from,to or width must be provided.",
        massiveMustProvideData:
            "At least dateShift,hsFrom, hsTo, minFrom,minTo or width must be provided.",
        timeframesUpdated: "Timeframe updated successfully.",
        haveActiveAppointments:
            "At least one of the timeframes you want to update has active appointments. If the update affect the appointment/s, we will cancel them. Are you sure you want to update?",
        haveActiveAppointmentsOnDelete:
            "At least one of the timeframes you want to delete has active appointments. If you proceed, we will cancel the appointments. Are you sure you want to delete?",
        mustSendEndRecurrency:
            "You are creating recurrent timeframes, so you must send endRecurrency date",
    },
};

export default { translation };
