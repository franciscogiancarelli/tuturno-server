import LocaleTranslation from "typedef/LocaleTranslation";

const translation: LocaleTranslation = {
    businesses: {
        wrongDeletedState: "shaize",
        missingAddress: "shaize",
        missingPhone: "shaize",
        missingLatitude: "shaize",
        cantCreateWithImages: "shaize",
        missingImageUrl: "shaize",
        missingLongitude: "shaize",
        invalidImage: "shaize",
        missingType: "shaize",
        cantCreateWithCover: "shaize",
        missingName: "shaize",
        businessNotFound: "shaize",
        businessSafelyRemoved: "shaize",
        cantUpdateId: "shaize",
        cantUpdateCreatedAt: "shaize",
        cantUpdateDeletedAt: "shaize",
        businessRestored: "Shaise shaize.",
        businessUpdated: "Shaize",
    },
    general: {
        missingId: "shaize",
        missingParam: "shaize",
        mustBeArray: "shaize",
        invalidId: "shaize",
        emailFailed: "shaize",
        cantChangeId: "shaize",
        unsuportedImageType: "shaize",
        invalidToken: "shaize",
        emailNotConfirmed: "shaize",
        pageSizeTooBig: "shaize",
        wrongState: "shaize",
        wrongPhone: "shaize",
        invalidDateTimeFormat: "shaize",
        invalidDateFormat: "shaize",
        unauthorized: "shaize",
        validationError: "Shaize shaize.",
        userNotFound: "shaize",
        unknownError: "Shaize",
        invalidPageNumber: "shaize",
    },
    me: {
        emailVerificationSuccessDescription: "shaize",
        emailVerificationSuccessTitle: "shaize",
        emailVerificationSuccessButton: "shaize",
        emailVerificationSuccessHelpDescription: "shaize",
        emailVerificationSuccessHelpTitle: "shaize",
        accountNotFullyRegistered: "shaize",
        emailNotVerified: "shaize",
        userNotFound: "shaize",
        missingEmail: "shaize",
        wrongPassword: "shaize",
        notStrongPassword: "shaize",
        wrongEmailType: "shaize",
        missingPassword: "shaize",
        emailNotFound: "shaize",
        incorrectCredentials: "shaize",
        missingName: "shaize",
        missingPhone: "shaize",
        verificationEmailSent: "shaize",
        userRegistered: "shaize",
        registerFail: "shaize",
        emailTaken: "shaize",
        recoveryEmailSent: "shaize",
        missingToken: "shaize",
        wrongToken: "shaize",
        wrongTokenType: "shaize",
        passwordUpdated: "shaize",
        cantChangeId: "shaize",
        cantChangePhone: "shaize",
        cantChangeEmail: "shaize",
        cantChangePasswordWithoutCurrentPassword: "shaize",
        dataUpdated: "shaize",
    },
    resources: {
        missingName: "shaize",
        missingInterval: "shaize",
        wrongIntervalType: "shaize",
        resourceDeleted: "shaize",
        resourceNotFound: "shaize",
        cantUpdateResource: "shaize",
        mustProvideData: "shaize",
        resourceUpdated: "shaize",
        appointmentTypeRelated: "shaize",
        cantRelateResource: "shaize",
        appointmentTypeDoesntExist: "shaize",
    },
    appointments: {
        missingFrom: "shaize",
        appointmentTypeNotFound: "shaize",
        timeframeNotFound: "shaize",
        appointmentSent: "shaize",
        timeframeIsFull: "shaize",
        appointmentCancelled: "shaize",
        cantCancel: "shaize",
        wrongDayType: "shaize",
        wrongRequestedByValue: "shaize",
        wrongCancelledByValue: "shaize",
        cantReportAbsentee: "shaize",
        absenteeReported: "shaize",
        cantReportPayment: "shaize",
        paymentStatusReported: "shaize",
        cantConfirmAppointment: "shaize",
        appointmentConfirmed: "shaize",
        excedeedMaxUserDailyRequest: "shaize",
        excedeedMaxUserWeeklyRequest: "shaize.",
        excedeedMaxUserMonthlyRequest: "shaize",
        excedeedMinimumTimeInAdvance: "shaize",
    },
    appointmentTypes: {
        missingName: "shaize",
        missingDescription: "shaize.",
        missingDuration: "shaize",
        missingCurrency: "shaize",
        missingNeedConfirmation: "shaize",
        wrongCurrencyType: "shaize",
        wrongDurationType: "shaize",
        missingPrice: "shaize",
        appointmentTypeNotFound: "shaize",
        cantUpdateAppointmentType: "shaize",
        mustProvideData: "shaize",
        appointmentTypeUpdated: "shaize",
        wrongPriceType: "shaize",
        wrongMaxUserRequestType: "shaize",
        wrongMinimumTimeInAdvance: "shaize",
    },
    reviews: {
        businessNotFound: "shaize",
        wrongScoreType: "shaize",
        missingScore: "shaize",
        reviewDeleted: "shaize",
        cantDeleteReview: "shaize",
    },
    timeframes: {
        missingFrom: "shaize",
        missingTo: "shaize",
        missingWidth: "shaize",
        wrongWidthType: "shaize",
        timeframeNotFound: "shaize",
        timeframesDeleted: "shaize",
        mustProvideData: "shaize",
        massiveMustProvideData: "shaize",
        timeframesUpdated: "shaize",
        haveActiveAppointments: "shaize",
        haveActiveAppointmentsOnDelete: "shaize",
        mustSendEndRecurrency: "shaize",
    },
};

export default { translation };
