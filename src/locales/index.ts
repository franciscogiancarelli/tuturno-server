import i18next, { StringMap, TOptions } from "i18next";
import LocaleTranslation from "typedef/LocaleTranslation";
import de from "./de";
import enUS from "./en-US";
import esAR from "./es-AR";
import esES from "./es-ES";

const locales = {
    de: de,
    "en-US": enUS,
    "es-AR": esAR,
    "es-ES": esES,
};

type LocaleKey =
    | `businesses.${keyof LocaleTranslation["businesses"]}`
    | `general.${keyof LocaleTranslation["general"]}`
    | `me.${keyof LocaleTranslation["me"]}`
    | `resources.${keyof LocaleTranslation["resources"]}`
    | `appointments.${keyof LocaleTranslation["appointments"]}`
    | `appointmentTypes.${keyof LocaleTranslation["appointmentTypes"]}`
    | `reviews.${keyof LocaleTranslation["reviews"]}`
    | `timeframes.${keyof LocaleTranslation["timeframes"]}`;

/**
 * Get translated text according to LOCAL env variable
 * @param key The key to get the translated text
 * @param options Parameters and/or options for translation
 * @returns Translated text
 *  */
export function __(
    key: LocaleKey | LocaleKey[],
    options?: TOptions<StringMap> | string
): string {
    return i18next.t(key, options);
}

export default locales;
