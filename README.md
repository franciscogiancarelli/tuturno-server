# tuturno-server

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

<div style="display: flex;">

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=tuturno_tuturno-server&metric=coverage&token=9ded8bee8cf7a0fd4a2fdf2169cc99aeb1e97273)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=tuturno_tuturno-server&metric=code_smells&token=9ded8bee8cf7a0fd4a2fdf2169cc99aeb1e97273)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=tuturno_tuturno-server&metric=duplicated_lines_density&token=9ded8bee8cf7a0fd4a2fdf2169cc99aeb1e97273)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=tuturno_tuturno-server&metric=ncloc&token=9ded8bee8cf7a0fd4a2fdf2169cc99aeb1e97273)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=tuturno_tuturno-server&metric=sqale_rating&token=9ded8bee8cf7a0fd4a2fdf2169cc99aeb1e97273)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=tuturno_tuturno-server&metric=security_rating&token=9ded8bee8cf7a0fd4a2fdf2169cc99aeb1e97273)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=tuturno_tuturno-server&metric=vulnerabilities&token=9ded8bee8cf7a0fd4a2fdf2169cc99aeb1e97273)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=tuturno_tuturno-server&metric=bugs&token=9ded8bee8cf7a0fd4a2fdf2169cc99aeb1e97273)](https://sonarcloud.io/summary/new_code?id=tuturno_tuturno-server)

<span>
<a href="https://cloud.digitalocean.com/kubernetes/clusters?i=457a77">
![](https://shields.io/badge/simple-DigitalOcean-black?logo=digitalocean&color=grey&label&message)
</a>
</span>

</div>

Hola! Estás en el repositorio del servidor principal de tuturno.
Está desarrollando en nodejs con typescript utilizando expressjs y typeorm.

Para poder levantarlo y desarrollar vas a necesitar tener Docker instalado y el un archivo .env que podes pedirmelo a [mi](franciscogiancarelli@gmail.com). (Y debería venir con docker-compose, si no viene por defecto instalado por separado).
Una vez que tengas docker instalado, parate en la raíz del proyecto y corre el commando:

```
docker-compose up -d
```

El flag `-d` le dice a docker que deje libre la terminal una vez que arranque los servicios.

Una vez que corras eso vas a ver algo parecido a esto:

```
Creating adminer       ... done
Creating test_database ... done
Creating database      ... done
```

Esto va a levantar la base de datos de desarrollo y la de pruebas (a la cual se conecta jest cuando levanta la aplicación para correr las test automatizadas).

Una vez que tenes eso corriendo, solo tenes que hacer un npm start para que arranque la app

## Documentación

La [documentación de los endpoints](https://tuturno.com.ar/api/docs/) se hace con [swagger](http://swagger.io/). Un módulo instalado en el proyecto nos permite definir en un comentario de tipo jsdoc la estructura según el estandar OpenAPI en cada endpoint definido. Cuando corre el servidor, otro módulo llamado swagger ui, levanta esa documentación OpenAPI construida antes a partir de comentarios, y corre una página estática en /api/docs con la documentación parseada para que sea linda de ver, y se pueda probar.

## Testing

La aplicación está testeada usando [jest](http://jestjs.io/) y [supertest](https://github.com/visionmedia/supertest#readme). Si querés correr los test, tenés que usar el commando `npm run test`.

Al principio de las test, se vuela la base de datos de prueba que estaba antes y se corren todas las migraciones desde cero. Además usamos la librería faker, para en cada prueba (cada archivo .test.ts) crear entidades de prueba nuevas. Eso se hace en el archivo initTestEntities, el cual vas a ver que se llama en al principio de todas las pruebas, para tener algo con que probar.

### Estructura de pruebas

Vas a ver que todos los handlers tienen un archivo identico pero que termina en test.ts. Esta es la manera de manejarnos con la estructura de pruebas en el proyecto. Cada vez que hagas un Handler, agrega un archivo al lado de pruebas que testee todas las funcionalidades de ese handler. Si agregas un handler no te va a quedar otra que agregar las tests para ese handler, porque jest está configurado para validar que el coverage no cae por debajo del 80%.

## Migraciones

Manejamos las migraciones con el [cli de typeorm](https://github.com/typeorm/typeorm/blob/master/docs/using-cli.md). No hay nada loco en esto, para correr migraciones en tu base de datos local desde tu computadora directamente podes usar el comando `npm run migrate`, que es lo mismo que hacer npm run typeorm migration:run. Solo es una facilidad para que el comando sea más corto.
