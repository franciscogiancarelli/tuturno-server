/* eslint-disable no-undef */
module.exports = {
    type: "postgres",
    host: process.env.POSTGRES_HOST,
    port: parseInt(process.env.POSTGRES_PORT),
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    ssl:
        process.env.NODE_ENV === "dev"
            ? undefined
            : {
                  rejectUnauthorized: false,
              },
    entities: [
        process.env.NODE_ENV === "dev"
            ? "./src/entities/*.ts"
            : "./build/entities/*.js",
    ],
    migrations:
        process.env.NODE_ENV === "dev"
            ? ["./src/migrations/*.ts"]
            : ["./build/migrations/*.js"],
    cli: {
        migrationsDir:
            process.env.NODE_ENV === "dev"
                ? "./src/migrations"
                : "./build/migrations",
    },
};
